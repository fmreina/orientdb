## OrientDB [![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

This repository was originally cloned from [OrientDb Technologies](https://github.com/orientechnologies/orientdb) repository on github.

Our purpose was to include the support for **Integrity Constraints** on a **Graph Database**. We chose the OrientDB database because its implementation already offered some support for the definition of simple contraints as value range, attribute type, unique value, among few others. With that our work extended the support to allow the declaration of new integrity constraints.

The experiments executed to test the impact of this new constraints can be found on the following repositories:

- [Orient-Driver](https://bitbucket.org/fmreina/orient-driver/src/master/) : Initiates a server of the OrientDB and a Client to run the tests
- [Constraint-Tests-Client](https://bitbucket.org/fmreina/constraint-tests-client/src/master/): Contains the tests used to evaluate the impact on the time consuption for using the new constraints

The current branch of this project ("master") contains the *Modified* version of the OrientDB, while the *Original* version used on the experiments is on the branch "original" of this project.

- [Modified version](https://bitbucket.org/fmreina/orientdb/src/master/)
- [Original version](https://bitbucket.org/fmreina/orientdb/src/original/)

The environment variable ``$ORIENTDB_HOME`` need to be setted to refer to the folder server of this project.

# Constraints Orient Project
On this project we implemented a **Constraint Manager** and added four new constraints that are listed bellow:

- Conditional
- In/Out
- Required Edge
- Cardinality

The goal of the **Conditional**  constraint is to compare properties of a node and to validate values assigned to them according to a previously defined condition. Its syntax is shown below.

```
<CREATE> <CONSTRAINT> name
<ON> class <(> attribute <)>
<CONDITIONAL> <(>
		<IF> property (>|<|>=|<=|=|!=) expression
		<THEN> property (>|<|>=|<=|=|!=) expression
		[ <ELSE> property (>|<|>=|<=|=|!=) expression ]
<)>
```

The **In / Out** constraint restricts the classes of nodes that participate as origin or target of an edge class. It also enforces the direction of the represented relationship. The syntax is as follow.

```
<CREATE> <CONSTRAINT> name
<ON> edge_type <IN_OUT_EDGE> (
		<FROM> origin_class <TO> target_class
		| <FROM> origin_class
		| <TO> target_class
)
```


The **Required Edge** constraint defines that a node class has a mandatory outgoing edge type, that will point to an instance of a target node class. This constraint follows the syntax below.

```
<CREATE> <CONSTRAINT> name
<ON> origin_class <REQUIRED_EDGE>
		[edge_type] <TO> target_class
```

The **Cardinality** constraint restricts the number of edges of a given class that connects an origin node to target nodes. The specification uses two integer numbers, with *N* serving as a placeholder for *"unspecified"*, any number is allowed. Its syntax is presented below.

```
<CREATE> <CONSTRAINT> name
<ON> origin_class <CARDINALITY>
		[edge_type] <(> <INT | N> <..> <INT | N> <)>    
		[<TO> target_class ]
```

---


### More About OrientDB:

More information about OrientDB can be found on the original repository from [OrientDb Technologies](https://github.com/orientechnologies/orientdb).

##### Interesting links:

- [Documentation](http://orientdb.com/docs/last/)
- [Get started with OrientDB](http://orientdb.com/getting-started/)
- [OrientDB Community Group](http://orientdb.com/active-user-community/)

##### OrientDB Licensing:
OrientDB is licensed by OrientDB LTD under the Apache 2 license. OrientDB relies on the following 3rd party libraries, which are compatible with the Apache license:

- Javamail: CDDL license (http://www.oracle.com/technetwork/java/faq-135477.html)
- java persistence 2.0: CDDL license
- JNA: Apache 2 (https://github.com/twall/jna/blob/master/LICENSE)
- Hibernate JPA 2.0 API: Eclipse Distribution License 1.0
- ASM: OW2

References:

- Apache 2 license (Apache2):
  http://www.apache.org/licenses/LICENSE-2.0.html

- Common Development and Distribution License (CDDL-1.0):
  http://opensource.org/licenses/CDDL-1.0

- Eclipse Distribution License (EDL-1.0):
  http://www.eclipse.org/org/documents/edl-v10.php (http://www.eclipse.org/org/documents/edl-v10.php)
