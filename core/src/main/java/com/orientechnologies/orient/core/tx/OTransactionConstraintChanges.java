package com.orientechnologies.orient.core.tx;

import java.util.NavigableMap;
import java.util.TreeMap;

import com.orientechnologies.common.comparator.ODefaultComparator;
import com.orientechnologies.orient.core.constraint.OConstraint;
import com.orientechnologies.orient.core.constraint.OConstraintInternal;
import com.orientechnologies.orient.core.constraint.OConstraintManager;

/**
 * Collects the changes to a constraint for a certain key
 *
 * @author fabio
 */
public class OTransactionConstraintChanges {

  public enum OPERATION {
    PUT, REMOVE, CLEAR
  }

  public NavigableMap<Object, OTransactionConstraintChangesPerKey> changesPerKey = new TreeMap<Object, OTransactionConstraintChangesPerKey>(
      ODefaultComparator.INSTANCE);

  public OTransactionConstraintChangesPerKey nullKeyChanges = new OTransactionConstraintChangesPerKey(null);

  public boolean cleared = false;

  private OConstraintInternal<?> resolvedConstraint = null;

  public OTransactionConstraintChangesPerKey getChangesPerKey(final Object key) {
    if (key == null)
      return nullKeyChanges;

    return changesPerKey.computeIfAbsent(key, OTransactionConstraintChangesPerKey::new);
  }

  public void setCleared() {
    changesPerKey.clear();
    nullKeyChanges.clear();

    cleared = true;
  }

  public Object getFirstKey() {
    return changesPerKey.firstKey();
  }

  public Object getLastKey() {
    return changesPerKey.lastKey();
  }

  public Object getLowerKey(Object key) {
    return changesPerKey.lowerKey(key);
  }

  public Object getHigherKey(Object key) {
    return changesPerKey.higherKey(key);
  }

  public Object getCeilingKey(Object key) {
    return changesPerKey.ceilingKey(key);
  }

  public Object[] firstAndLastKeys(Object from, boolean fromInclusive, Object to, boolean toInclusive) {
    final NavigableMap<Object, OTransactionConstraintChangesPerKey> interval = changesPerKey
        .subMap(from, fromInclusive, to, toInclusive);

    if (interval.isEmpty()) {
      return new Object[0];
    } else {
      return new Object[] {interval.firstKey(), interval.lastKey()};
    }
  }

  public Object getFloorKey(Object key) {
    return changesPerKey.floorKey(key);
  }

  public OConstraintInternal<?> resolveAssociatedConstraint(String constraintName, OConstraintManager constraintManager) {
    if (resolvedConstraint == null) {
      final OConstraint<?> constraint = constraintManager.getConstraint(constraintName);
      if (constraint != null)
        resolvedConstraint = constraint.getInternal();
    }

    return resolvedConstraint;
  }

  public OConstraintInternal<?> getAssociatedConstraint() {
    return resolvedConstraint;
  }
}
