package com.orientechnologies.orient.core.constraint;

import java.io.Serializable;

import com.orientechnologies.orient.core.sql.parser.OIdentifier;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintInOutEdgeParameters implements OConstraintParameters, Serializable {

  private static final long serialVersionUID = -2402571119067660545L;

  public OIdentifier        inClass;
  public OIdentifier        outClass;

  public OConstraintInOutEdgeParameters() {
  }

  public OConstraintInOutEdgeParameters(OIdentifier outClass, OIdentifier inClass) {
    this.outClass = outClass;
    this.inClass = inClass;
  }

  public OConstraintInOutEdgeParameters(String params) {
    strToParam(params);
  }

  @Override
  public OConstraintParameters strToParam(String str) {
    // FIXME
    // TODO - FABIO: fazer a conversão dos valores de string para os artibutos desse objeto para retornar um objeto desse tipo
    String[] props = str.split(" ");
    outClass = new OIdentifier(props[1]);
    inClass = new OIdentifier(props[3]);

    return this;
  }

  @Override
  public String toString() {
    return "[ " + outClass + " : " + inClass + " ]";
  }

  public OIdentifier getOutClass() {
    return outClass;
  }

  public OIdentifier getInClass() {
    return inClass;
  }

}
