package com.orientechnologies.orient.core.constraint;

import java.util.List;
import java.util.Map;

import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.exception.OInvalidConstraintEngineIdException;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ODocumentEntry;
import com.orientechnologies.orient.core.record.impl.OVertexDocument;
import com.orientechnologies.orient.core.sql.parser.OIdentifier;
import com.orientechnologies.orient.core.storage.OConstraintValidationException;
import com.orientechnologies.orient.core.storage.impl.local.OAbstractPaginatedStorage;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintInOutEdge extends OConstraintOneValue {

  private final OConstraintEngine.Validator<Object, OIdentifiable> CONSTRAINT_VALIDATOR = new OConstraintEngine.Validator<Object, OIdentifiable>() {
    @Override
    public Object validate(Object key, OIdentifiable oldValue, OIdentifiable newValue,
        Map<String, List<OConstraintDto>> fields, String params) {

//      System.out.println("\nValidation of the IN/OUT constraint.");
//      System.out.println("Params: " + params);
//      System.out.println("Entered Values:");
//      System.out.println("oldValue: " + oldValue);
//      System.out.println("newValue: "+newValue);
//      fields.keySet().forEach(k -> {
//        String cName = k;
//        System.out.println(cName);
//        List<OConstraintDto> listDto = fields.get(k);
//        listDto.forEach(dto -> {
//          Map<String, ODocumentEntry> m = dto.getAttributes();
//          m.keySet().forEach(s -> {
//            String attribute = s;
//            System.out.print("\t");
//            System.out.print(attribute);
//            System.out.print(" = ");
//            ODocumentEntry doc = m.get(s);
//            System.out.println(doc.value);
//          });
//          System.out.println();
//        });
//      });

      fields.values().forEach(list -> {
        list.forEach(dto -> {
          if(OConstraintDto.OConstraintObjectType.EDGE.equals(dto.getType()))
            doValidate(dto.getAttributes(), params);
          });
      });

      if (!newValue.getIdentity().isPersistent())
        newValue = newValue.getRecord();
      return newValue.getIdentity();
    }

    private void doValidate(Map<String, ODocumentEntry> fields, String params) {
      if (params != null && !params.isEmpty()) {
        OConstraintInOutEdgeParameters constraintParams = new OConstraintInOutEdgeParameters(params);

        OIdentifier outParam = constraintParams.getOutClass();
        OIdentifier inParam = constraintParams.getInClass();

        ODocumentEntry outNode = fields.get("out");
        OVertexDocument outNodeValue = null;
        if (outNode.value instanceof OVertexDocument)
          outNodeValue = (OVertexDocument) outNode.value;
        ODocumentEntry inNode = fields.get("in");
        OVertexDocument inNodeValue = null;
        if (inNode.value instanceof OVertexDocument)
          inNodeValue = (OVertexDocument) inNode.value;

        if (outNode == null || inNode == null)
          throw new OConstraintValidationException("It was not possible to recover the boundery nodes' information", getName());

        boolean outNodeIsWrong = false;
        boolean inNodeIsWrong = false;
        if (outNodeValue != null && !outParam.getStringValue().equalsIgnoreCase(outNodeValue.getClassName())) {
          outNodeIsWrong = true;
        }
        if (inNodeValue != null && !inParam.getStringValue().equalsIgnoreCase(inNodeValue.getClassName())) {
          inNodeIsWrong = true;
        }

        if (outNodeIsWrong || inNodeIsWrong) {
          throw new OConstraintValidationException(String.format(
              "Wrong boundery nodes for the Edge. It was expected outNode = %s and inNode = %s but received outNode = %s and inNode = %s.",
              outParam, inParam, outNodeValue.getClassName(), inNodeValue.getClassName()), getName());
        }

      } else {
        throw new OConstraintValidationException("It was not possible to recover the params' information", getName());
      }
    }
  };

  public OConstraintInOutEdge(String name, final String type, String algorithm, int version, OAbstractPaginatedStorage storage,
      String valueContainerAlgorithm, ODocument metadata) {
    super(name, type, algorithm, valueContainerAlgorithm, metadata, version, storage);
  }

  @Override
  public OConstraint<OIdentifiable> put(Object key, final OIdentifiable value, Map<String, List<OConstraintDto>> fields) {
    key = getCollatingValue(key);

    acquireSharedLock();
    try {
      while (true) {
        try {
          storage.validatedPutConstraintValue(constraintId, key, value, CONSTRAINT_VALIDATOR, fields,
              getConstraintDefinition().getConstraintParams());
          return this;
        } catch (OInvalidConstraintEngineIdException ignore) {
          doReloadConstraintEngine();
        }
      }

    } finally {
      releaseSharedLock();
    }
  }

  @Override
  public boolean supportsOrderedIterations() {
    return false;
  }

}
