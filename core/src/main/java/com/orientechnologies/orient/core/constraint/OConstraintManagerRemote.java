package com.orientechnologies.orient.core.constraint;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import com.orientechnologies.common.listener.OProgressListener;
import com.orientechnologies.common.log.OLogManager;
import com.orientechnologies.orient.core.db.ODatabaseDocumentInternal;
import com.orientechnologies.orient.core.exception.OCommandExecutionException;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.record.ORecordInternal;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandExecutorSQLCreateConstraint;
import com.orientechnologies.orient.core.storage.OStorage;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintManagerRemote extends OConstraintManagerAbstract {

  private static final long   serialVersionUID = 8526460233546244546L;
  private AtomicBoolean       skipPush         = new AtomicBoolean(false);
  private static final String QUERY_DROP       = "drop constraint `%s` if exists";
  private OStorage            storage;

  public OConstraintManagerRemote(OStorage storage) {
    super();
    this.storage = storage;
  }

  @Override
  public OConstraint<?> createConstraint(String iName, String iType, OConstraintDefinition constraintDefinition,
      int[] clusterIdsToConstraint, OProgressListener progressListener, ODocument metadata) {
    return createConstraint(iName, iType, constraintDefinition, clusterIdsToConstraint, progressListener, metadata, null);
  }

  @Override
  public OConstraint<?> createConstraint(String iName, String iType, OConstraintDefinition iConstraintDefinition,
      int[] clusterIdsToConstraint, OProgressListener progressListener, ODocument metadata, String algorithm) {
    //TODO - FABIO : recebe params

    String createConstraintDDL;
    if (iConstraintDefinition != null)
      createConstraintDDL = iConstraintDefinition.toCreateConstraintDDL(iName, iType, algorithm);
    else
      createConstraintDDL = new OSimpleKeyConstraintDefinition().toCreateConstraintDDL(iName, iType, algorithm);

    if (metadata != null)
      createConstraintDDL += " " + OCommandExecutorSQLCreateConstraint.KEYWORD_METADATA + " " + metadata.toJSON();

    acquireExclusiveLock();
    try {
      if (progressListener != null)
        progressListener.onBegin(this, 0, false);

      getDatabase().command(createConstraintDDL).close();

      ORecordInternal.setIdentity(document,
          new ORecordId(getDatabase().getStorage().getConfiguration().getConstraintMgrRecordId()));

      if (progressListener != null)
        progressListener.onCompletition(this, true);

      reload();

      return preProcessBeforeReturn(getDatabase(), constraints.get(iName));
    } catch (OCommandExecutionException e) {
      throw new OConstraintException(e.getMessage());
    } finally {
      releaseExclusiveLock();
    }
  }

  @Override
  protected OStorage getStorage() {
    return storage;
  }

  public OConstraintManager dropConstraint(final String iConstraintName) {
    acquireExclusiveLock();
    try {
      final String text = String.format(QUERY_DROP, iConstraintName);
      getDatabase().command(text).close();

      // REMOVE THE CONSTRAINT LOCALLY
      constraints.remove(iConstraintName);
      reload();

      return this;
    } finally {
      releaseExclusiveLock();
    }
  }

  @Override
  public ODocument toStream() {
    throw new UnsupportedOperationException("Remote constraint cannot be streamed");
  }

  @Override
  public OConstraint<?> preProcessBeforeReturn(ODatabaseDocumentInternal database, OConstraint<?> constraint) {

    return constraint;
  }

  @Override
  public void removeClassPropertyConstraint(OConstraint<?> constr) {
  }

  protected OConstraint<?> getRemoteConstraintInstace(boolean isMultiValueConstraint, String type, String name, String algorithm,
      Set<String> clusterToConstraint, OConstraintDefinition constraintDefinition, ORID identity, ODocument configuration) {
    if (isMultiValueConstraint)
      return new OConstraintRemoteMultiValue(name, type, algorithm, identity, constraintDefinition, configuration,
          clusterToConstraint, getStorage().getName());

    return new OConstraintRemoteOneValue(name, type, algorithm, identity, constraintDefinition, configuration, clusterToConstraint,
        getStorage().getName());
  }

  @Override
  protected void fromStream() {
    acquireExclusiveLock();
    try {
      clearMetadata();

      final Collection<ODocument> constrs = document.field(CONFIG_CONSTRAINTS);

      if (constrs != null) {
        for (ODocument d : constrs) {
          d.setLazyLoad(false);

          try {
            final boolean isMultiValue = ODefaultConstraintFactory
                .isMultiValueConstraint((String) d.field(OConstraintInternal.CONFIG_TYPE));

            final OConstraintMetadata newConstraintMetadata = OConstraintAbstract.loadMetadataInternal(d,
                (String) d.field(OConstraintInternal.CONFIG_TYPE), d.<String> field(OConstraintInternal.ALGORITHM),
                d.<String> field(OConstraintInternal.VALUE_CONTAINER_ALGORITHM));

            addConstraintInternal(
                getRemoteConstraintInstace(isMultiValue, newConstraintMetadata.getType(), newConstraintMetadata.getName(),
                    newConstraintMetadata.getAlgorithm(), newConstraintMetadata.getClustersToConstraint(),
                    newConstraintMetadata.getConstraintDefinition(), (ORID) d.field(OConstraintAbstract.CONFIG_MAP_RID), d));
          } catch (Exception e) {
            OLogManager.instance().error(this, "Error on loading of constraint by configuration: %s", e, d);
          }
        }
      }
    } finally {
      releaseExclusiveLock();
    }
  }

  @Override
  public void fromStream(ODocument iDocument) {
    super.acquireExclusiveLock();
    try {
      super.fromStream(iDocument);
    } finally {
      super.releaseExclusiveLock();
    }
  }

  public void update(ODocument constraintManager) {
    if (!skipPush.get()) {
      super.fromStream(constraintManager);
    }
  }

}
