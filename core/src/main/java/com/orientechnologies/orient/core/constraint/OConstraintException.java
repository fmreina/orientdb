package com.orientechnologies.orient.core.constraint;

import com.orientechnologies.orient.core.exception.OCoreException;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintException extends OCoreException{

	private static final long serialVersionUID = 5532374948458044666L;

	public OConstraintException(OConstraintException exception) {
		super(exception);
	}
	
	public OConstraintException(final String string){
		super(string);
	}
}
