package com.orientechnologies.orient.core.constraint;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.orientechnologies.common.exception.OException;
import com.orientechnologies.orient.core.collate.OCollate;
import com.orientechnologies.orient.core.db.record.OMultiValueChangeEvent;
import com.orientechnologies.orient.core.db.record.ORecordElement;
import com.orientechnologies.orient.core.index.OCompositeKey;
import com.orientechnologies.orient.core.index.OIndexException;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandExecutorSQLCreateIndex;

/**
 * checar OCompositeKey usado nesta classe. OCompositeKey é do pacote index e esta voltado para o index (Fabio)
 * 
 * @author fabio
 *
 */
public class OCompositeConstraintDefinition extends OAbstractConstraintDefinition {

  private static final long                 serialVersionUID               = 7604520568634164954L;
  private final List<OConstraintDefinition> constraintDefinitions;
  private String                            className;
  private int                               multiValueDefinitionConstraint = -1;
  private OConstraintCompositeCollate       collate                        = new OConstraintCompositeCollate(this);

  public OCompositeConstraintDefinition() {
    constraintDefinitions = new ArrayList<OConstraintDefinition>(5);
  }

  /**
   * Constructor for new constraint creation.
   *
   * @param iClassName
   *          - name of class which is owner of this constraint
   */
  public OCompositeConstraintDefinition(final String iClassName) {
    super();

    constraintDefinitions = new ArrayList<OConstraintDefinition>(5);
    className = iClassName;
  }

  /**
   * Constructor for new constraint creation.
   *
   * @param iClassName
   *          - name of class which is owner of this constraint
   * @param iConstraints
   *          List of constraintDefinitions to add in given constraint.
   */
  public OCompositeConstraintDefinition(final String iClassName, final List<? extends OConstraintDefinition> iConstraints,
      int version) {
    super();

    constraintDefinitions = new ArrayList<OConstraintDefinition>(5);
    for (OConstraintDefinition constraintDefinition : iConstraints) {
      constraintDefinitions.add(constraintDefinition);
      collate.addCollate(constraintDefinition.getCollate());

      if (constraintDefinition instanceof OConstraintDefinitionMultiValue)
        if (multiValueDefinitionConstraint == -1)
          multiValueDefinitionConstraint = constraintDefinitions.size() - 1;
        else
          throw new OConstraintException("Composite key cannot contain more than one collection item");
    }

    className = iClassName;
  }

  /**
   * {@inheritDoc}
   */
  public String getClassName() {
    return className;
  }

  /**
   * Add new constraintDefinition in current composite.
   *
   * @param constraintDefinition
   *          Constraint to add.
   */
  public void addConstraint(final OConstraintDefinition constraintDefinition) {
    constraintDefinitions.add(constraintDefinition);
    if (constraintDefinition instanceof OConstraintDefinitionMultiValue) {
      if (multiValueDefinitionConstraint == -1)
        multiValueDefinitionConstraint = constraintDefinitions.size() - 1;
      else
        throw new OIndexException("Composite key cannot contain more than one collection item");
    }

    collate.addCollate(constraintDefinition.getCollate());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<String> getFields() {
    final List<String> fields = new LinkedList<String>();
    for (final OConstraintDefinition constraintDefinition : constraintDefinitions) {
      fields.addAll(constraintDefinition.getFields());
    }
    return Collections.unmodifiableList(fields);
  }

  @Override
  public List<String> getFieldsToConstraint() {
    final List<String> fields = new LinkedList<String>();
    for (final OConstraintDefinition constraintDefinition : constraintDefinitions) {
      fields.addAll(constraintDefinition.getFieldsToConstraint());
    }
    return Collections.unmodifiableList(fields);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object getDocumentValueToConstraint(ODocument iDocument) {
    final List<OCompositeKey> compositeKeys = new ArrayList<OCompositeKey>(10);
    final OCompositeKey firstKey = new OCompositeKey();
    boolean containsCollection = false;

    compositeKeys.add(firstKey);

    for (final OConstraintDefinition constraintDefinition : constraintDefinitions) {
      final Object result = constraintDefinition.getDocumentValueToConstraint(iDocument);

      if (result == null && isNullValuesIgnored())
        return null;

      // for empty collections we add null key in index
      if (result instanceof Collection && ((Collection) result).isEmpty() && isNullValuesIgnored())
        return null;

      containsCollection = addKey(firstKey, compositeKeys, containsCollection, result);
    }

    if (!containsCollection)
      return firstKey;

    return compositeKeys;
  }

  public int getMultiValueDefinitionConstraint() {
    return multiValueDefinitionConstraint;
  }

  public String getMultiValueField() {
    if (multiValueDefinitionConstraint >= 0)
      return constraintDefinitions.get(multiValueDefinitionConstraint).getFields().get(0);

    return null;
  }

  /**
   * {@inheritDoc}
   */
  public Object createValue(final List<?> params) {
    int currentParamConstraint = 0;
    final OCompositeKey firstKey = new OCompositeKey();

    final List<OCompositeKey> compositeKeys = new ArrayList<OCompositeKey>(10);
    compositeKeys.add(firstKey);

    boolean containsCollection = false;

    for (final OConstraintDefinition constraintDefinition : constraintDefinitions) {
      if (currentParamConstraint + 1 > params.size())
        break;

      final int endConstraint;
      if (currentParamConstraint + constraintDefinition.getParamCount() > params.size())
        endConstraint = params.size();
      else
        endConstraint = currentParamConstraint + constraintDefinition.getParamCount();

      final List<?> constraintParams = params.subList(currentParamConstraint, endConstraint);
      currentParamConstraint += constraintDefinition.getParamCount();

      final Object keyValue = constraintDefinition.createValue(constraintParams);

      if (keyValue == null && isNullValuesIgnored())
        return null;

      // for empty collections we add null key in constraint
      if (keyValue instanceof Collection && ((Collection) keyValue).isEmpty() && isNullValuesIgnored())
        return null;

      containsCollection = addKey(firstKey, compositeKeys, containsCollection, keyValue);
    }

    if (!containsCollection)
      return firstKey;

    return compositeKeys;
  }

  public OConstraintDefinitionMultiValue getMultiValueDefinition() {
    if (multiValueDefinitionConstraint > -1)
      return (OConstraintDefinitionMultiValue) constraintDefinitions.get(multiValueDefinitionConstraint);

    return null;
  }

  public OCompositeKey createSingleValue(final List<?> params) {
    final OCompositeKey compositeKey = new OCompositeKey();
    int currentParamConstraint = 0;

    for (final OConstraintDefinition constraintDefinition : constraintDefinitions) {
      if (currentParamConstraint + 1 > params.size())
        break;

      final int endConstraint;
      if (currentParamConstraint + constraintDefinition.getParamCount() > params.size())
        endConstraint = params.size();
      else
        endConstraint = currentParamConstraint + constraintDefinition.getParamCount();

      final List<?> constraintParams = params.subList(currentParamConstraint, endConstraint);
      currentParamConstraint += constraintDefinition.getParamCount();

      final Object keyValue;

      if (constraintDefinition instanceof OConstraintDefinitionMultiValue)
        keyValue = ((OConstraintDefinitionMultiValue) constraintDefinition).createSingleValue(constraintParams.toArray());
      else
        keyValue = constraintDefinition.createValue(constraintParams);

      if (keyValue == null && isNullValuesIgnored())
        return null;

      compositeKey.addKey(keyValue);
    }

    return compositeKey;
  }

  private static boolean addKey(OCompositeKey firstKey, List<OCompositeKey> compositeKeys, boolean containsCollection,
      Object keyValue) {
    // in case of collection we split single composite key on several composite keys
    // each of those composite keys contain single collection item.
    // we can not contain more than single collection item in constraint
    if (keyValue instanceof Collection) {
      final Collection<?> collectionKey = (Collection<?>) keyValue;
      final int collectionSize;

      // we insert null if collection is empty
      if (collectionKey.isEmpty())
        collectionSize = 1;
      else
        collectionSize = collectionKey.size();

      // if that is first collection we split single composite key on several keys, each of those
      // composite keys contain single item from collection
      if (!containsCollection)
        // sure we need to expand collection only if collection size more than one, otherwise
        // collection of composite keys already contains original composite key
        for (int i = 1; i < collectionSize; i++) {
          final OCompositeKey compositeKey = new OCompositeKey(firstKey.getKeys());
          compositeKeys.add(compositeKey);
        }
      else
        throw new OConstraintException("Composite key cannot contain more than one collection item");

      int compositeConstraint = 0;
      if (!collectionKey.isEmpty()) {
        for (final Object keyItem : collectionKey) {
          final OCompositeKey compositeKey = compositeKeys.get(compositeConstraint);
          compositeKey.addKey(keyItem);

          compositeConstraint++;
        }
      } else {
        firstKey.addKey(null);
      }

      containsCollection = true;
    } else if (containsCollection)
      for (final OCompositeKey compositeKey : compositeKeys)
        compositeKey.addKey(keyValue);
    else
      firstKey.addKey(keyValue);

    return containsCollection;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object createValue(final Object... params) {
    if (params.length == 1 && params[0] instanceof Collection)
      return params[0];

    return createValue(Arrays.asList(params));
  }

  public void processChangeEvent(OMultiValueChangeEvent<?, ?> changeEvent, Map<OCompositeKey, Integer> keysToAdd,
      Map<OCompositeKey, Integer> keysToRemove, Object... params) {

    final OConstraintDefinitionMultiValue constraintDefinitionMultiValue = (OConstraintDefinitionMultiValue) constraintDefinitions
        .get(multiValueDefinitionConstraint);

    final CompositeWrapperMap compositeWrapperKeysToAdd = new CompositeWrapperMap(keysToAdd, constraintDefinitions, params,
        multiValueDefinitionConstraint);

    final CompositeWrapperMap compositeWrapperKeysToRemove = new CompositeWrapperMap(keysToRemove, constraintDefinitions, params,
        multiValueDefinitionConstraint);

    constraintDefinitionMultiValue.processChangeEvent(changeEvent, compositeWrapperKeysToAdd, compositeWrapperKeysToRemove);
  }

  /**
   * {@inheritDoc}
   */
  public int getParamCount() {
    int total = 0;
    for (final OConstraintDefinition constraintDefinition : constraintDefinitions)
      total += constraintDefinition.getParamCount();
    return total;
  }

  /**
   * {@inheritDoc}
   */
  public OType[] getTypes() {
    final List<OType> types = new LinkedList<OType>();
    for (final OConstraintDefinition constraintDefinition : constraintDefinitions)
      Collections.addAll(types, constraintDefinition.getTypes());

    return types.toArray(new OType[types.size()]);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    final OCompositeConstraintDefinition that = (OCompositeConstraintDefinition) o;

    if (!className.equals(that.className))
      return false;
    if (!constraintDefinitions.equals(that.constraintDefinitions))
      return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = constraintDefinitions.hashCode();
    result = 31 * result + className.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "OCompositeConstraitnDefinition{" + "constraintDefinitions=" + constraintDefinitions + ", className='" + className + '\''
        + '}';
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ODocument toStream() {
    document.setInternalStatus(ORecordElement.STATUS.UNMARSHALLING);
    try {
      serializeToStream();
    } finally {
      document.setInternalStatus(ORecordElement.STATUS.LOADED);
    }

    return document;
  }

  @Override
  protected void serializeToStream() {
    super.serializeToStream();

    final List<ODocument> constraints = new ArrayList<ODocument>(constraintDefinitions.size());
    final List<String> constraintClasses = new ArrayList<String>(constraintDefinitions.size());

    document.field("className", className);
    for (final OConstraintDefinition constraintDefinition : constraintDefinitions) {
      final ODocument constraintDocument = constraintDefinition.toStream();
      constraints.add(constraintDocument);

      constraintClasses.add(constraintDefinition.getClass().getName());
    }
    document.field("constraintDefinitions", constraints, OType.EMBEDDEDLIST);
    document.field("constraintClasses", constraintClasses, OType.EMBEDDEDLIST);
    document.field("nullValuesIgnored", isNullValuesIgnored());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toCreateConstraintDDL(String constraintName, String constraintType, String engine) {
    final StringBuilder ddl = new StringBuilder("create constraint ");
    ddl.append(constraintName).append(" on ").append(className).append(" ( ");

    final Iterator<String> fieldIterator = getFieldsToConstraint().iterator();
    if (fieldIterator.hasNext()) {
      ddl.append(fieldIterator.next());
      while (fieldIterator.hasNext()) {
        ddl.append(", ").append(fieldIterator.next());
      }
    }
    ddl.append(" ) ").append(constraintType).append(' ');

    if (engine != null)
      ddl.append(OCommandExecutorSQLCreateIndex.KEYWORD_ENGINE + " " + engine).append(' ');

    if (multiValueDefinitionConstraint == -1) {
      boolean first = true;
      for (OType oType : getTypes()) {
        if (first)
          first = false;
        else
          ddl.append(", ");

        ddl.append(oType.name());
      }
    }

    return ddl.toString();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void fromStream() {
    serializeFromStream();
  }

  @Override
  protected void serializeFromStream() {
    super.serializeFromStream();

    try {
      className = document.field("className");

      final List<ODocument> constraints = document.field("constraintDefinitions");
      final List<String> constraintClasses = document.field("constraintClasses");

      constraintDefinitions.clear();

      collate = new OConstraintCompositeCollate(this);

      for (int i = 0; i < constraintClasses.size(); i++) {
        final Class<?> clazz = Class.forName(constraintClasses.get(i));
        final ODocument constraintDoc = constraints.get(i);

        final OConstraintDefinition constraintDefinition = (OConstraintDefinition) clazz.getDeclaredConstructor().newInstance();
        constraintDefinition.fromStream(constraintDoc);

        constraintDefinitions.add(constraintDefinition);
        collate.addCollate(constraintDefinition.getCollate());

        if (constraintDefinition instanceof OConstraintDefinitionMultiValue)
          multiValueDefinitionConstraint = constraintDefinitions.size() - 1;
      }

      setNullValuesIgnored(!Boolean.FALSE.equals(document.<Boolean> field("nullValuesIgnored")));
    } catch (final ClassNotFoundException e) {
      throw OException.wrapException(new OIndexException("Error during composite constraint deserialization"), e);
    } catch (final NoSuchMethodException e) {
      throw OException.wrapException(new OIndexException("Error during composite constraint deserialization"), e);
    } catch (final InvocationTargetException e) {
      throw OException.wrapException(new OIndexException("Error during composite constraint deserialization"), e);
    } catch (final InstantiationException e) {
      throw OException.wrapException(new OIndexException("Error during composite constraint deserialization"), e);
    } catch (final IllegalAccessException e) {
      throw OException.wrapException(new OIndexException("Error during composite constraint deserialization"), e);
    }
  }

  @Override
  public OCollate getCollate() {
    return collate;
  }

  @Override
  public void setCollate(OCollate collate) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean isAutomatic() {
    return constraintDefinitions.get(0).isAutomatic();
  }

  private static final class CompositeWrapperMap implements Map<Object, Integer> {
    private final Map<OCompositeKey, Integer> underlying;
    private final Object[]                    params;
    private final List<OConstraintDefinition> constraintDefinitions;
    private final int                         multiValueConstraint;

    private CompositeWrapperMap(Map<OCompositeKey, Integer> underlying, List<OConstraintDefinition> constraintDefinitions,
        Object[] params, int multiValueConstraint) {
      this.underlying = underlying;
      this.params = params;
      this.multiValueConstraint = multiValueConstraint;
      this.constraintDefinitions = constraintDefinitions;
    }

    public int size() {
      return underlying.size();
    }

    public boolean isEmpty() {
      return underlying.isEmpty();
    }

    public boolean containsKey(Object key) {
      final OCompositeKey compositeKey = convertToCompositeKey(key);

      return underlying.containsKey(compositeKey);
    }

    public boolean containsValue(Object value) {
      return underlying.containsValue(value);
    }

    public Integer get(Object key) {
      return underlying.get(convertToCompositeKey(key));
    }

    public Integer put(Object key, Integer value) {
      final OCompositeKey compositeKey = convertToCompositeKey(key);
      return underlying.put(compositeKey, value);
    }

    public Integer remove(Object key) {
      return underlying.remove(convertToCompositeKey(key));
    }

    public void putAll(Map<? extends Object, ? extends Integer> m) {
      throw new UnsupportedOperationException("Unsupported because of performance reasons");
    }

    public void clear() {
      underlying.clear();
    }

    public Set<Object> keySet() {
      throw new UnsupportedOperationException("Unsupported because of performance reasons");
    }

    public Collection<Integer> values() {
      return underlying.values();
    }

    public Set<Entry<Object, Integer>> entrySet() {
      throw new UnsupportedOperationException();
    }

    private OCompositeKey convertToCompositeKey(Object key) {
      final OCompositeKey compositeKey = new OCompositeKey();

      int paramsConstraint = 0;
      for (int i = 0; i < constraintDefinitions.size(); i++) {
        final OConstraintDefinition constraintDefinition = constraintDefinitions.get(i);
        if (i != multiValueConstraint) {
          compositeKey.addKey(constraintDefinition.createValue(params[paramsConstraint]));
          paramsConstraint++;
        } else
          compositeKey.addKey(((OConstraintDefinitionMultiValue) constraintDefinition).createSingleValue(key));
      }
      return compositeKey;
    }
  }
}
