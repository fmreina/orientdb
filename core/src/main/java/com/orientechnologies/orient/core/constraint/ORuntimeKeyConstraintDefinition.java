package com.orientechnologies.orient.core.constraint;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.orientechnologies.common.serialization.types.OBinarySerializer;
import com.orientechnologies.orient.core.collate.ODefaultCollate;
import com.orientechnologies.orient.core.db.record.ORecordElement;
import com.orientechnologies.orient.core.exception.OConfigurationException;
import com.orientechnologies.orient.core.index.OIndexException;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.serialization.serializer.binary.OBinarySerializerFactory;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * 
 * @author fabio
 *
 * @param <T>
 */
@SuppressFBWarnings(value = "SE_TRANSIENT_FIELD_NOT_RESTORED")
public class ORuntimeKeyConstraintDefinition<T> extends OAbstractConstraintDefinition {

  private static final long              serialVersionUID = 199393547569702302L;
  private transient OBinarySerializer<T> serializer;

  @SuppressWarnings("unchecked")
  public ORuntimeKeyConstraintDefinition(final byte iId, int version) {
    super();

    serializer = (OBinarySerializer<T>) OBinarySerializerFactory.getInstance().getObjectSerializer(iId);
    if (serializer == null)
      throw new OConfigurationException("Runtime constraint definition cannot find binary serializer with id=" + iId
          + ". Assure to plug custom serializer into the server.");
  }

  public ORuntimeKeyConstraintDefinition() {
  }

  @Override
  public List<String> getFields() {
    return Collections.emptyList();
  }

  @Override
  public List<String> getFieldsToConstraint() {
    return Collections.emptyList();
  }

  @Override
  public String getClassName() {
    return null;
  }

  @Override
  public Object createValue(Object... params) {
    return createValue(Arrays.asList(params));
  }

  @Override
  public int getParamCount() {
    return -1;
  }

  @Override
  public OType[] getTypes() {
    return new OType[0];
  }

  @Override
  public ODocument toStream() {
    document.setInternalStatus(ORecordElement.STATUS.UNMARSHALLING);
    try {
      serializeToStream();
      return document;
    } finally {
      document.setInternalStatus(ORecordElement.STATUS.LOADED);
    }
  }

  @Override
  protected void serializeToStream() {
    super.serializeToStream();

    document.field("keySerializerId", serializer.getId());
    document.field("collate", collate.getName());
    document.field("nullValuesIgnored", isNullValuesIgnored());
  }

  @Override
  protected void fromStream() {
    serializeFromStream();
  }

  @Override
  protected void serializeFromStream() {
    super.serializeFromStream();

    final byte keySerializerId = ((Number) document.field("keySerializerId")).byteValue();
    serializer = (OBinarySerializer<T>) OBinarySerializerFactory.getInstance().getObjectSerializer(keySerializerId);
    if (serializer == null)
      throw new OConfigurationException("Runtime constraint definition cannot find binary serializer with id=" + keySerializerId
          + ". Assure to plug custom serializer into the server.");

    String collateField = document.field("collate");
    if (collateField == null)
      collateField = ODefaultCollate.NAME;
    setNullValuesIgnored(!Boolean.FALSE.equals(document.<Boolean> field("nullValuesIgnored")));
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    final ORuntimeKeyConstraintDefinition<?> that = (ORuntimeKeyConstraintDefinition<?>) o;
    return serializer.equals(that.serializer);
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + serializer.getId();
    return result;
  }

  @Override
  public String toString() {
    return "ORuntimeKeyConstraintDefinition{" + "serializer=" + serializer.getId() + '}';
  }

  /**
   * {@inheritDoc}
   *
   * @param indexName
   * @param indexType
   */
  @Override
  public String toCreateConstraintDDL(String constraintName, String constraintType, String engine) {
    final StringBuilder ddl = new StringBuilder("create constraint `");
    ddl.append(constraintName).append("` ").append(constraintType).append(' ');
    ddl.append("runtime ").append(serializer.getId());
    return ddl.toString();
  }

  public OBinarySerializer<T> getSerializer() {
    return serializer;
  }

  @Override
  public boolean isAutomatic() {
    return getClassName() != null;
  }

  @Override
  public Object getDocumentValueToConstraint(ODocument iDocument) {
    throw new OIndexException("This method is not supported in given constraint definition.");
  }

}
