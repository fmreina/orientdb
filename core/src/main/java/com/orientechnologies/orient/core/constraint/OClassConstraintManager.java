package com.orientechnologies.orient.core.constraint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.orientechnologies.common.exception.OException;
import com.orientechnologies.orient.core.db.ODatabaseDocumentInternal;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.db.record.OMultiValueChangeEvent;
import com.orientechnologies.orient.core.db.record.OMultiValueChangeTimeLine;
import com.orientechnologies.orient.core.db.record.ORecordElement;
import com.orientechnologies.orient.core.db.record.OTrackedMultiValue;
import com.orientechnologies.orient.core.exception.ORecordNotFoundException;
import com.orientechnologies.orient.core.index.OCompositeKey;
import com.orientechnologies.orient.core.metadata.schema.OImmutableClass;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ODocumentEntry;
import com.orientechnologies.orient.core.record.impl.ODocumentInternal;
import com.orientechnologies.orient.core.tx.OTransactionConstraintChanges;

/**
 * Handles constraints when records change.
 * 
 * @author fabio
 *
 */
public class OClassConstraintManager {

  public static class ConstraintChange {
    public final OConstraint                             constraint;
    public final OTransactionConstraintChanges.OPERATION operation;
    public final Object                                  key;
    public final OIdentifiable                           value;
    private Map<String, ODocumentEntry>                  fields;

    public ConstraintChange(OConstraint constraintName, OTransactionConstraintChanges.OPERATION operation, Object key,
        OIdentifiable value, Map<String, ODocumentEntry> fields) {
      this.constraint = constraintName;
      this.operation = operation;
      this.key = key;
      this.value = value;
      this.fields = fields;
    }
  }

  private static ODocument checkForLoading(final ODocument iRecord) {
    if (iRecord.getInternalStatus() == ORecordElement.STATUS.NOT_LOADED) {
      try {
        return (ODocument) iRecord.load();
      } catch (final ORecordNotFoundException e) {
        throw OException.wrapException(new OConstraintException("Error during loading of record with id " + iRecord.getIdentity()),
            e);
      }
    }
    return iRecord;
  }

  public static void checkConstraintsAfterCreate(ODocument document, ODatabaseDocumentInternal database) {
    document = checkForLoading(document);
    List<ConstraintChange> ops = new ArrayList<>();

    processConstraintOnCreate(database, document, ops);
    applyChanges(ops);
  }

  private static void applyChanges(List<ConstraintChange> changes) {
    for (ConstraintChange op : changes) {
      if (op.operation == OTransactionConstraintChanges.OPERATION.PUT) {
        Map<String, List<OConstraintDto>> fieldsMap = new HashMap<>();
        List<OConstraintDto> list = new ArrayList<>();
        list.add(new OConstraintDto(null, op.constraint.getName(), op.fields));
        fieldsMap.put(op.constraint.getName(), list);

        putInConstraint(op.constraint, op.key, op.value, fieldsMap);
      } else {
        removeFromConstraint(op.constraint, op.key, op.value);
      }
    }
  }

  protected static void putInConstraint(OConstraint<?> constraint, Object key, OIdentifiable value,
      Map<String, List<OConstraintDto>> fields) {
    constraint.put(key, value, fields);
  }

  protected static void removeFromConstraint(OConstraint<?> constraint, Object key, OIdentifiable value) {
    constraint.remove(key, value);
  }

  public static void processConstraintOnCreate(ODatabaseDocumentInternal database, ODocument document, List<ConstraintChange> ops) {
    final OImmutableClass cls = ODocumentInternal.getImmutableSchemaClass(database, document);
    if (cls != null) {
      final Collection<OConstraint<?>> constraintes = cls.getRawConstraints();
      addConstraintsEntries(database, document, constraintes, ops);
    }
  }

  private static void addConstraintsEntries(ODatabaseDocumentInternal database, ODocument document,
      final Collection<OConstraint<?>> constraintes, List<ConstraintChange> changes) {
    // STORE THE RECORD IF NEW, OTHERWISE ITS RID
    final OIdentifiable rid = document.getIdentity();

    for (final OConstraint<?> constraint : constraintes) {
      addConstraintEntry(document, rid, getTransactionalConstraint(database, constraint), changes);
    }
  }

  private static void addConstraintEntry(ODocument document, OIdentifiable rid, OConstraint<?> constraint,
      List<ConstraintChange> changes) {
    Map<String, ODocumentEntry> fields = document.getFieldsMap();

    final OConstraintDefinition constraintDefinition = constraint.getDefinition();
    final Object key = constraintDefinition.getDocumentValueToConstraint(document);
    if (key instanceof Collection) {
      for (final Object keyItem : (Collection<?>) key)
        if (!constraintDefinition.isNullValuesIgnored() || keyItem != null)
          addPut(changes, constraint, keyItem, rid, fields);
    } else if (!constraintDefinition.isNullValuesIgnored() || key != null)
      addPut(changes, constraint, key, rid, fields);
  }

  private static OConstraint getTransactionalConstraint(ODatabaseDocumentInternal database, OConstraint<?> constraint) {
    return ((OConstraintManagerProxy) database.getMetadata().getConstraintManager()).preProcessBeforeReturn(database, constraint);
  }

  private static void addPut(List<ConstraintChange> changes, OConstraint<?> constraint, Object key, OIdentifiable value,
      Map<String, ODocumentEntry> fields) {
    changes.add(new ConstraintChange(constraint, OTransactionConstraintChanges.OPERATION.PUT, key, value, fields));
  }

  private static void deleteConstraintKey(final OConstraint<?> constraint, final ODocument iRecord, final Object origValue,
      List<ConstraintChange> changes, Map<String, ODocumentEntry> fields) {
    final OConstraintDefinition constraintDefinition = constraint.getDefinition();
    if (origValue instanceof Collection) {
      for (final Object valueItem : (Collection<?>) origValue) {
        if (!constraintDefinition.isNullValuesIgnored() || valueItem != null)
          addRemove(changes, constraint, valueItem, iRecord, fields);
      }
    } else if (!constraintDefinition.isNullValuesIgnored() || origValue != null) {
      addRemove(changes, constraint, origValue, iRecord, fields);
    }
  }

  private static void addRemove(List<ConstraintChange> changes, OConstraint<?> constraint, Object key, OIdentifiable value, Map<String, ODocumentEntry> fields) {
    changes.add(new ConstraintChange(constraint, OTransactionConstraintChanges.OPERATION.REMOVE, key, value, fields));
  }

  public static void processConstraintOnDelete(ODatabaseDocumentInternal database, ODocument iDocument,
      List<ConstraintChange> changes, Map<String, ODocumentEntry> fields) {
    final OImmutableClass cls = ODocumentInternal.getImmutableSchemaClass(database, iDocument);
    if (cls == null)
      return;

    final Collection<OConstraint<?>> constraintes = new ArrayList<>();
    for (OConstraint constraint : cls.getRawConstraints()) {
      constraintes.add(getTransactionalConstraint(database, constraint));
    }

    if (!constraintes.isEmpty()) {
      final Set<String> dirtyFields = new HashSet<>(Arrays.asList(iDocument.getDirtyFields()));

      if (!dirtyFields.isEmpty()) {
        // REMOVE CONSTRAINT OF ENTRIES FOR THE OLD VALUES
        final Iterator<OConstraint<?>> constraintIterator = constraintes.iterator();

        while (constraintIterator.hasNext()) {
          final OConstraint<?> constraint = constraintIterator.next();

          final boolean result;
          if (constraint.getDefinition() instanceof OCompositeConstraintDefinition)
            result = processCompositeConstraintDelete(constraint, dirtyFields, iDocument, changes, fields);
          else
            result = processSingleConstraintDelete(constraint, dirtyFields, iDocument, changes, fields);

          if (result)
            constraintIterator.remove();
        }
      }
    }

    // REMOVE CONSTRAINT OF ENTRIES FOR THE NON CHANGED ONLY VALUES
    for (final OConstraint<?> constraint : constraintes) {
      final Object key = constraint.getDefinition().getDocumentValueToConstraint(iDocument);
      deleteConstraintKey(constraint, iDocument, key, changes, fields);
    }
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  private static boolean processSingleConstraintDelete(final OConstraint<?> constraint, final Set<String> dirtyFields,
      final ODocument iRecord, List<ConstraintChange> changes, Map<String, ODocumentEntry> fields) {
    final OConstraintDefinition constraintDefinition = constraint.getDefinition();

    final List<String> constraintFields = constraintDefinition.getFields();
    if (constraintFields.isEmpty()) {
      return false;
    }

    final String constraintField = constraintFields.iterator().next();
    if (dirtyFields.contains(constraintField)) {
      final OMultiValueChangeTimeLine<?, ?> multiValueChangeTimeLine = iRecord.getCollectionTimeLine(constraintField);

      final Object origValue;
      if (multiValueChangeTimeLine != null) {
        final OTrackedMultiValue fieldValue = iRecord.field(constraintField);
        final Object restoredMultiValue = fieldValue.returnOriginalState(multiValueChangeTimeLine.getMultiValueChangeEvents());
        origValue = constraintDefinition.createValue(restoredMultiValue);
      } else
        origValue = constraintDefinition.createValue(iRecord.getOriginalValue(constraintField));
      deleteConstraintKey(constraint, iRecord, origValue, changes, fields);
      return true;
    }
    return false;
  }

  private static boolean processCompositeConstraintDelete(final OConstraint<?> constraint, final Set<String> dirtyFields,
      final ODocument iRecord, List<ConstraintChange> changes, Map<String, ODocumentEntry> fields) {
    final OCompositeConstraintDefinition constraintDefinition = (OCompositeConstraintDefinition) constraint.getDefinition();

    final String multiValueField = constraintDefinition.getMultiValueField();

    final List<String> constraintFields = constraintDefinition.getFields();
    for (final String constraintField : constraintFields) {
      // REMOVE IT
      if (dirtyFields.contains(constraintField)) {
        final List<Object> origValues = new ArrayList<>(constraintFields.size());

        for (final String field : constraintFields) {
          if (!field.equals(multiValueField))
            if (dirtyFields.contains(field))
              origValues.add(iRecord.getOriginalValue(field));
            else
              origValues.add(iRecord.field(field));
        }

        if (multiValueField != null) {
          final OMultiValueChangeTimeLine<?, ?> multiValueChangeTimeLine = iRecord.getCollectionTimeLine(multiValueField);
          if (multiValueChangeTimeLine != null) {
            final OTrackedMultiValue fieldValue = iRecord.field(multiValueField);
            @SuppressWarnings("unchecked")
            final Object restoredMultiValue = fieldValue.returnOriginalState(multiValueChangeTimeLine.getMultiValueChangeEvents());
            origValues.add(constraintDefinition.getMultiValueDefinitionConstraint(), restoredMultiValue);
          } else if (dirtyFields.contains(multiValueField))
            origValues.add(constraintDefinition.getMultiValueDefinitionConstraint(), iRecord.getOriginalValue(multiValueField));
          else
            origValues.add(constraintDefinition.getMultiValueDefinitionConstraint(), iRecord.field(multiValueField));
        }

        final Object origValue = constraintDefinition.createValue(origValues);
        deleteConstraintKey(constraint, iRecord, origValue, changes, fields);
        return true;
      }
    }
    return false;
  }

  public static void checkConstraintsAfterDelete(ODocument iDocument, ODatabaseDocumentInternal database, Map<String, ODocumentEntry> fields) {
    List<ConstraintChange> changes = new ArrayList<>();
    processConstraintOnDelete(database, iDocument, changes, fields);
    applyChanges(changes);
  }

  public static void processConstraintUpdate(ODocument iDocument, Set<String> dirtyFields, OConstraint<?> constraint,
      List<ConstraintChange> changes, Map<String, ODocumentEntry> fields) {
    if (constraint.getDefinition() instanceof OCompositeConstraintDefinition)
      processCompositeConstraintUpdate(constraint, dirtyFields, iDocument, changes, fields);
    else
      processSingleConstraintUpdate(constraint, dirtyFields, iDocument, changes, fields);
  }

  private static void processSingleConstraintUpdate(final OConstraint<?> constraint, final Set<String> dirtyFields,
      final ODocument iRecord, List<ConstraintChange> changes, Map<String, ODocumentEntry> fields) {
    final OConstraintDefinition constraintDefinition = constraint.getDefinition();
    final List<String> constraintFields = constraintDefinition.getFields();

    if (constraintFields.isEmpty()) 
      return;

    final String constraintField = constraintFields.get(0);
    // it will check everytime even if the node of the field of the constraint is not dirty 
//    if (!dirtyFields.contains(constraintField))
//      return;

    final OMultiValueChangeTimeLine<?, ?> multiValueChangeTimeLine = iRecord.getCollectionTimeLine(constraintField);
    if (multiValueChangeTimeLine != null) {
      final OConstraintDefinitionMultiValue constraintDefinitionMultiValue = (OConstraintDefinitionMultiValue) constraintDefinition;
      final Map<Object, Integer> keysToAdd = new HashMap<>();
      final Map<Object, Integer> keysToRemove = new HashMap<>();

      for (OMultiValueChangeEvent<?, ?> changeEvent : multiValueChangeTimeLine.getMultiValueChangeEvents()) {
        constraintDefinitionMultiValue.processChangeEvent(changeEvent, keysToAdd, keysToRemove);
      }

      for (final Object keyToRemove : keysToRemove.keySet())
        addRemove(changes, constraint, keyToRemove, iRecord, fields);

      for (final Object keyToAdd : keysToAdd.keySet())
        addPut(changes, constraint, keyToAdd, iRecord.getIdentity(), fields);

    } else {
      final Object origValue = constraintDefinition.createValue(iRecord.getOriginalValue(constraintField));
      final Object newValue = constraintDefinition.getDocumentValueToConstraint(iRecord);
//      final Object origValue = constraintDefinition.createValue(iRecord.getOriginalValue(dirtyFields.iterator().next()));
//      final Object newValue = fields.get(dirtyFields.iterator().next()).value;// constraintDefinition.getDocumentValueToConstraint(iRecord);

      processConstraintUpdateFieldAssignment(constraint, iRecord, origValue, newValue, changes, fields);
    }
  }

  private static void processCompositeConstraintUpdate(final OConstraint<?> constraint, final Set<String> dirtyFields,
      final ODocument iRecord, List<ConstraintChange> changes, Map<String, ODocumentEntry> fields) {
    final OCompositeConstraintDefinition constraintDefinition = (OCompositeConstraintDefinition) constraint.getDefinition();

    final List<String> constraintFields = constraintDefinition.getFields();
    final String multiValueField = constraintDefinition.getMultiValueField();

    for (final String constraintField : constraintFields) {
      if (dirtyFields.contains(constraintField)) {
        final List<Object> origValues = new ArrayList<>(constraintFields.size());

        for (final String field : constraintFields) {
          if (!field.equals(multiValueField))
            if (dirtyFields.contains(field)) {
              origValues.add(iRecord.getOriginalValue(field));
            } else {
              origValues.add(iRecord.field(field));
            }
        }

        if (multiValueField == null) {
          final Object origValue = constraintDefinition.createValue(origValues);
          final Object newValue = constraintDefinition.getDocumentValueToConstraint(iRecord);

          if (!constraintDefinition.isNullValuesIgnored() || origValue != null)
            addRemove(changes, constraint, origValue, iRecord, fields);

          if (!constraintDefinition.isNullValuesIgnored() || newValue != null)
            addPut(changes, constraint, newValue, iRecord.getIdentity(), fields);
        } else {
          final OMultiValueChangeTimeLine<?, ?> multiValueChangeTimeLine = iRecord.getCollectionTimeLine(multiValueField);
          if (multiValueChangeTimeLine == null) {
            if (dirtyFields.contains(multiValueField))
              origValues.add(constraintDefinition.getMultiValueDefinitionConstraint(), iRecord.getOriginalValue(multiValueField));
            else
              origValues.add(constraintDefinition.getMultiValueDefinitionConstraint(), iRecord.field(multiValueField));

            final Object origValue = constraintDefinition.createValue(origValues);
            final Object newValue = constraintDefinition.getDocumentValueToConstraint(iRecord);

            processConstraintUpdateFieldAssignment(constraint, iRecord, origValue, newValue, changes, fields);
          } else {
            // in case of null values support and empty collection field we put null placeholder in
            // place where collection item should be located so we can not use "fast path" to
            // update constraint values
            if (dirtyFields.size() == 1 && constraintDefinition.isNullValuesIgnored()) {
              final Map<OCompositeKey, Integer> keysToAdd = new HashMap<>();
              final Map<OCompositeKey, Integer> keysToRemove = new HashMap<>();

              for (OMultiValueChangeEvent<?, ?> changeEvent : multiValueChangeTimeLine.getMultiValueChangeEvents()) {
                constraintDefinition.processChangeEvent(changeEvent, keysToAdd, keysToRemove, origValues.toArray());
              }

              for (final Object keyToRemove : keysToRemove.keySet())
                addRemove(changes, constraint, keyToRemove, iRecord, fields);

              for (final Object keyToAdd : keysToAdd.keySet())
                addPut(changes, constraint, keyToAdd, iRecord.getIdentity(), fields);
            } else {
              final OTrackedMultiValue fieldValue = iRecord.field(multiValueField);
              @SuppressWarnings("unchecked")
              final Object restoredMultiValue = fieldValue
                  .returnOriginalState(multiValueChangeTimeLine.getMultiValueChangeEvents());

              origValues.add(constraintDefinition.getMultiValueDefinitionConstraint(), restoredMultiValue);

              final Object origValue = constraintDefinition.createValue(origValues);
              final Object newValue = constraintDefinition.getDocumentValueToConstraint(iRecord);

              processConstraintUpdateFieldAssignment(constraint, iRecord, origValue, newValue, changes, fields);
            }
          }
        }
        return;
      }
    }
    return;
  }

  private static void processConstraintUpdateFieldAssignment(OConstraint<?> constraint, ODocument iRecord, final Object origValue,
      final Object newValue, List<ConstraintChange> changes, Map<String, ODocumentEntry> fields) {
    final OConstraintDefinition constraintDefinition = constraint.getDefinition();
    if ((origValue instanceof Collection) && (newValue instanceof Collection)) {
      final Set<Object> valuesToRemove = new HashSet<>((Collection<?>) origValue);
      final Set<Object> valuesToAdd = new HashSet<>((Collection<?>) newValue);

      valuesToRemove.removeAll((Collection<?>) newValue);
      valuesToAdd.removeAll((Collection<?>) origValue);

      for (final Object valueToRemove : valuesToRemove) {
        if (!constraintDefinition.isNullValuesIgnored() || valueToRemove != null) {
          addRemove(changes, constraint, valueToRemove, iRecord, fields);
        }
      }

      for (final Object valueToAdd : valuesToAdd) {
        if (!constraintDefinition.isNullValuesIgnored() || valueToAdd != null) {
          addPut(changes, constraint, valueToAdd, iRecord, fields);
        }
      }
    } else {
      deleteConstraintKey(constraint, iRecord, origValue, changes, fields);

      if (newValue instanceof Collection) {
        for (final Object newValueItem : (Collection<?>) newValue) {
          addPut(changes, constraint, newValueItem, iRecord.getIdentity(), fields);
        }
      } else if (!constraintDefinition.isNullValuesIgnored() || newValue != null) {
        addPut(changes, constraint, newValue, iRecord.getIdentity(), fields);
      }
    }
  }

  public static void checkConstraintsAfterUpdate(ODocument iDocument, ODatabaseDocumentInternal database, Map<String, ODocumentEntry> fields) {
    iDocument = checkForLoading(iDocument);
    List<ConstraintChange> changes = new ArrayList<>();
    processConstraintOnUpdate(database, iDocument, changes, fields);
    applyChanges(changes);
  }

  public static void processConstraintOnUpdate(ODatabaseDocumentInternal database, ODocument iDocument,
      List<ConstraintChange> changes, Map<String, ODocumentEntry> fields) {
    final OImmutableClass cls = ODocumentInternal.getImmutableSchemaClass(database, iDocument);
    if (cls == null) {
      return;
    }

    final Collection<OConstraint<?>> constraints = cls.getRawConstraints();
    if (!constraints.isEmpty()) {
//      final Set<String> dirtyFields = new HashSet<>(Arrays.asList(iDocument.getDirtyFields()));
      final Set<String> dirtyFields = iDocument.getFieldsMap().keySet();
      if (!dirtyFields.isEmpty())
        for (final OConstraint<?> constraint : constraints) {
          processConstraintUpdate(iDocument, dirtyFields, getTransactionalConstraint(database, constraint), changes, fields);
        }
    }
  }
}
