package com.orientechnologies.orient.core.constraint;

import java.util.Map;

import com.orientechnologies.orient.core.db.record.ORecordOperation;
import com.orientechnologies.orient.core.db.record.ridbag.ORidBag;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ODocumentEntry;
import com.orientechnologies.orient.core.record.impl.OVertexDocument;

/**
 * 
 * @author fabio
 * 
 */
public class OConstraintDto {

  public enum OConstraintObjectType {
    NODE, EDGE, DEFAULT
  }
  
  public enum OOperation{
    LOADED(0), UPDATED(1), DELETED(2), CREATED(3);
    
    int code;
    
    private OOperation(int code) {
      this.code = code;
    }
    
    public static OOperation getByid(int id){
      for(OOperation op : values())
        if(op.code == id)
          return op;
      return null;
    }
  }

  private ORID                        id;
  private String                      nameType;  // person or car or has ...
  private OConstraintObjectType       type;
  private Map<String, ODocumentEntry> attributes;
  private OOperation operation;

  public OConstraintDto(ORecordOperation recordOperation) {
    this.id = recordOperation.getRID();
    this.nameType = extractClassName(recordOperation);
    this.type = extractType(recordOperation);
    this.attributes = extractAttributes(recordOperation);
    this.operation = OOperation.getByid(recordOperation.getType());
  }

  public OConstraintDto(ORID id, String name, Map<String, ODocumentEntry> fields) {
    this.id = id;
    this.nameType = name;
    this.type = OConstraintObjectType.DEFAULT;
    this.attributes = fields;
  }

  /**
   * @return the name type of the node (ex.: Person, Car, Has,...)
   */
  public String getName() {
    return nameType;
  }

  public ORID getId() {
    return id;
  }

  public Map<String, ODocumentEntry> getAttributes() {
    return attributes;
  }
  
  public OConstraintObjectType getType() {
    return type;
  }
  
  public String getNameType() {
    return nameType;
  }
  
  public OOperation getOperation() {
    return operation;
  }

  private String extractClassName(ORecordOperation recordOperation) {
    ODocument record = ((ODocument) recordOperation.getRecord());
    String name = record != null ? record.getClassName() : "className";
    name = name != null ? record.getClassName().toUpperCase() : "className";
    return name;
  }

  private Map<String, ODocumentEntry> extractAttributes(ORecordOperation recordOperation) {
    return ((ODocument) recordOperation.getRecord()).getFieldsMap();
  }

  private OConstraintObjectType extractType(ORecordOperation recordOperation) {
    if (recordOperation.getRecord() instanceof OVertexDocument)
      return OConstraintObjectType.NODE;
    else
      return OConstraintObjectType.EDGE;
  }

  public ORidBag extractOutEdges() {
    String out = attributes.keySet().stream().filter(k -> k.startsWith("out_")).findFirst().orElse("");

    if (out.isEmpty())
      return new ORidBag();

    ORidBag edgesIds = ((ORidBag) attributes.get(out).value);

    return edgesIds;
  }
  
  public ORidBag extractInEdges() {
    String in = attributes.keySet().stream().filter(k -> k.startsWith("in_")).findFirst().orElse("");

    if (in.isEmpty())
      return new ORidBag();

    ORidBag edgesIds = ((ORidBag) attributes.get(in).value);

    return edgesIds;
  }

  public OVertexDocument extractInNode() {
    ODocumentEntry entry = attributes.get("in");

    if (entry == null)
      return null;
    
    OVertexDocument inNode = (OVertexDocument) entry.value;
    return inNode;
  }

  public OVertexDocument extractOutNode() {
    ODocumentEntry entry = attributes.get("out");

    if (entry == null)
      return null;
    
    OVertexDocument outNode = (OVertexDocument) entry.value;
    return outNode;
  }

  // private String extractOutEdgeName(Map<String, ODocumentEntry> attributes) {
  // String out = attributes.keySet().stream().filter(k -> k.startsWith("out_")).collect(Collectors.toList()).get(0);
  // return out.substring(out.indexOf('_') + 1);
  // }
  //
  // private String extractInEdgeName(Map<String, ODocumentEntry> attributes) {
  // String in = attributes.keySet().stream().filter(k -> k.startsWith("in_")).collect(Collectors.toList()).get(0);
  // return in.substring(in.indexOf('_') + 1);
  // }
  
 public String toString() {
   String att = "";
   for(String k : attributes.keySet())
     att += k + ":"+ attributes.get(k).value + " ";
   return type + " " + nameType  + " " + id + " ( " + att + ")";
 };

}
