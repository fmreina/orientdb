package com.orientechnologies.orient.core.constraint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.orientechnologies.orient.core.collate.OCollate;
import com.orientechnologies.orient.core.collate.ODefaultCollate;
import com.orientechnologies.orient.core.db.record.ORecordElement;
import com.orientechnologies.orient.core.index.OCompositeKey;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OSQLEngine;

/**
 * 
 * @author fabio
 *
 */
public class OSimpleKeyConstraintDefinition extends OAbstractConstraintDefinition {

  private static final long serialVersionUID = 6655074693306801899L;
  private OType[]           keyTypes;

  public OSimpleKeyConstraintDefinition() {
  }

  public OSimpleKeyConstraintDefinition(int version, OType... keyTypes) {
    super();

    this.keyTypes = keyTypes;
  }

  public OSimpleKeyConstraintDefinition(OType[] keyTypes2, List<OCollate> collatesList, int version) {
    super();

    this.keyTypes = Arrays.copyOf(keyTypes2, keyTypes2.length);

    if (keyTypes.length > 1) {
      OConstraintCompositeCollate collate = new OConstraintCompositeCollate(this);
      if (collatesList != null) {
        for (OCollate oCollate : collatesList) {
          collate.addCollate(oCollate);
        }
      } else {
        final int typesSize = keyTypes.length;
        final OCollate defCollate = OSQLEngine.getCollate(ODefaultCollate.NAME);
        for (int i = 0; i < typesSize; i++) {
          collate.addCollate(defCollate);
        }
      }
      this.collate = collate;
    }
  }

  @Override
  public List<String> getFields() {
    return Collections.emptyList();
  }

  @Override
  public List<String> getFieldsToConstraint() {
    return Collections.emptyList();
  }

  @Override
  public String getClassName() {
    return null;
  }

  public Object createValue(final Object... params) {
    if (params == null || params.length == 0)
      return null;

    if (keyTypes.length == 1)
      return OType.convert(params[0], keyTypes[0].getDefaultJavaType());

    final OCompositeKey compositeKey = new OCompositeKey();

    for (int i = 0; i < params.length; ++i) {
      final Comparable<?> paramValue = (Comparable<?>) OType.convert(params[i], keyTypes[i].getDefaultJavaType());

      if (paramValue == null)
        return null;
      compositeKey.addKey(paramValue);
    }

    return compositeKey;
  }

  @Override
  public Object getDocumentValueToConstraint(ODocument iDocument) {
    throw new OConstraintException("This method is not supported in given constraint definition.");
  }

  @Override
  public int getParamCount() {
    return keyTypes.length;
  }

  public OType[] getTypes() {
    return Arrays.copyOf(keyTypes, keyTypes.length);
  }

  @Override
  public ODocument toStream() {
    document.setInternalStatus(ORecordElement.STATUS.UNMARSHALLING);
    try {
      serializeToStream();
      return document;
    } finally {
      document.setInternalStatus(ORecordElement.STATUS.LOADED);
    }
  }

  @Override
  protected void serializeToStream() {
    super.serializeToStream();

    final List<String> keyTypeNames = new ArrayList<String>(keyTypes.length);

    for (final OType keyType : keyTypes)
      keyTypeNames.add(keyType.toString());

    document.field("keyTypes", keyTypeNames, OType.EMBEDDEDLIST);
    if (collate instanceof OConstraintCompositeCollate) {
      List<String> collatesNames = new ArrayList<String>();
      for (OCollate curCollate : ((OConstraintCompositeCollate) this.collate).getCollates())
        collatesNames.add(curCollate.getName());
      document.field("collates", collatesNames, OType.EMBEDDEDLIST);
    } else
      document.field("collate", collate.getName());

    document.field("nullValuesIgnored", isNullValuesIgnored());
  }

  @Override
  protected void fromStream() {
    serializeFromStream();
  }

  @Override
  protected void serializeFromStream() {
    super.serializeFromStream();

    final List<String> keyTypeNames = document.field("keyTypes");
    keyTypes = new OType[keyTypeNames.size()];

    int i = 0;
    for (final String keyTypeName : keyTypeNames) {
      keyTypes[i] = OType.valueOf(keyTypeName);
      i++;
    }
    String collate = document.field("collate");
    if (collate != null) {
      setCollate(collate);
    } else {
      final List<String> collatesNames = document.field("collates");
      if (collatesNames != null) {
        OConstraintCompositeCollate collates = new OConstraintCompositeCollate(this);
        for (String collateName : collatesNames)
          collates.addCollate(OSQLEngine.getCollate(collateName));
        this.collate = collates;
      }
    }

    setNullValuesIgnored(!Boolean.FALSE.equals(document.<Boolean> field("nullValuesIgnored")));
  }

  @Override
  public String toString() {
    return "OSimpleKeyConstraintDefinition{" + "keyTypes=" + (keyTypes == null ? null : Arrays.asList(keyTypes)) + '}';
  }

  @Override
  public String toCreateConstraintDDL(String constraintName, String constraintType, String engine) {
    final StringBuilder ddl = new StringBuilder("create constraint `");
    ddl.append(constraintName).append("` ").append(constraintType).append(' ');

    if (keyTypes != null && keyTypes.length > 0) {
      ddl.append(keyTypes[0].toString());
      for (int i = 1; i < keyTypes.length; i++) {
        ddl.append(", ").append(keyTypes[i].toString());
      }
    }
    return ddl.toString();
  }

  @Override
  public boolean isAutomatic() {
    return false;
  }
}
