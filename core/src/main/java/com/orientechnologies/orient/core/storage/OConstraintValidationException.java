/*
 *
 *  *  Copyright 2010-2016 OrientDB LTD (http://orientdb.com)
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *
 *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *  *
 *  * For more information: http://orientdb.com
 *
 */

package com.orientechnologies.orient.core.storage;

import com.orientechnologies.common.exception.OHighLevelException;
import com.orientechnologies.orient.core.exception.OCoreException;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintValidationException extends OCoreException implements OHighLevelException {
  private final String constraintName;

  public OConstraintValidationException(final OConstraintValidationException exception) {
    super(exception);
    this.constraintName = exception.constraintName;
  }

  public OConstraintValidationException(final String message, final String constraintName) {
    super(message);
    this.constraintName = constraintName;
  }

  public String getConstraintName() {
    return constraintName;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null || !obj.getClass().equals(getClass()))
      return false;

    return constraintName.equals(((OConstraintValidationException) obj).constraintName);
  }

  @Override
  public int hashCode() {
    return constraintName.hashCode();
  }

  @Override
  public String toString() {
    return super.toString() + " CONSTRAINT=" + constraintName;
  }
}
