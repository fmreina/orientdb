package com.orientechnologies.orient.core.constraint;

import com.orientechnologies.orient.core.record.impl.ODocument;

/**
 * 
 * @author fabio
 *
 */
public interface OConstraintCallback {

  Object getDocumentValueToConstraint(ODocument iDocument);
}
