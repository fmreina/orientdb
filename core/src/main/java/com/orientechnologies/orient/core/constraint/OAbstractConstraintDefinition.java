package com.orientechnologies.orient.core.constraint;

import com.orientechnologies.orient.core.collate.OCollate;
import com.orientechnologies.orient.core.collate.ODefaultCollate;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OSQLEngine;
import com.orientechnologies.orient.core.type.ODocumentWrapperNoClass;

/**
 * Abstract constraint definition implementation
 * 
 * @author fabio
 *
 */
public abstract class OAbstractConstraintDefinition extends ODocumentWrapperNoClass implements OConstraintDefinition {

  protected OCollate collate           = new ODefaultCollate();
  private boolean    nullValuesIgnored = true;

  public OAbstractConstraintDefinition() {
    super(new ODocument().setTrackingChanges(false));
  }

  public OCollate getCollate() {
    return collate;
  }

  public void setCollate(final OCollate collate) {
    if (collate == null)
      throw new IllegalArgumentException("COLLATE cannot be null");
    this.collate = collate;
  }

  public void setCollate(String iCollate) {
    if (iCollate == null)
      iCollate = ODefaultCollate.NAME;

    setCollate(OSQLEngine.getCollate(iCollate));
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    OAbstractConstraintDefinition that = (OAbstractConstraintDefinition) o;

    if (!collate.equals(that.collate))
      return false;

    if (nullValuesIgnored != that.nullValuesIgnored)
      return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = collate.hashCode();
    result = 31 * result + (nullValuesIgnored ? 1 : 0);
    return result;
  }

  @Override
  public boolean isNullValuesIgnored() {
    return nullValuesIgnored;
  }

  @Override
  public void setNullValuesIgnored(boolean value) {
    nullValuesIgnored = value;
  }

  protected void serializeToStream() {
  }

  protected void serializeFromStream() {
  }
  
  @Override
  public String getConstraintParams() {
    return "";
  }
}
