package com.orientechnologies.orient.core.constraint;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.db.record.OMultiValueChangeEvent;
import com.orientechnologies.orient.core.db.record.ridbag.ORidBag;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;

public class OPropertyRidBagConstraintDefinition extends OAbstractConstraintDefinitionMultiValue {

  private static final long serialVersionUID = 3181968539994768854L;

  public OPropertyRidBagConstraintDefinition() {
  }

  public OPropertyRidBagConstraintDefinition(String className, String field) {
    super(className, field, OType.LINK);
  }

  @Override
  public Object createSingleValue(Object... param) {
    return OType.convert(param[0], keyType.getDefaultJavaType());
  }

  public void processChangeEvent(final OMultiValueChangeEvent<?, ?> changeEvent, final Map<Object, Integer> keysToAdd,
      final Map<Object, Integer> keysToRemove) {
    switch (changeEvent.getChangeType()) {
    case ADD: {
      processAdd(createSingleValue(changeEvent.getValue()), keysToAdd, keysToRemove);
      break;
    }
    case REMOVE: {
      processRemoval(createSingleValue(changeEvent.getOldValue()), keysToAdd, keysToRemove);
      break;
    }
    default:
      throw new IllegalArgumentException("Invalid change type : " + changeEvent.getChangeType());
    }
  }

  @Override
  public Object getDocumentValueToConstraint(ODocument iDocument) {
    return createValue(iDocument.<Object> field(field));
  }

  @Override
  public Object createValue(final List<?> params) {
    if (!(params.get(0) instanceof ORidBag))
      return null;

    final ORidBag ridBag = (ORidBag) params.get(0);
    final List<Object> values = new ArrayList<Object>();
    for (final OIdentifiable item : ridBag) {
      values.add(createSingleValue(item));
    }

    return values;
  }

  @Override
  public Object createValue(final Object... params) {
    if (!(params[0] instanceof ORidBag))
      return null;

    final ORidBag ridBag = (ORidBag) params[0];
    final List<Object> values = new ArrayList<Object>();
    for (final OIdentifiable item : ridBag) {
      values.add(createSingleValue(item));
    }

    return values;
  }

  @Override
  public String toCreateConstraintDDL(String constraintName, String constraintType, String engine) {
    return createConstraintDDLWithoutFieldType(constraintName, constraintType, engine).toString();
  }

}
