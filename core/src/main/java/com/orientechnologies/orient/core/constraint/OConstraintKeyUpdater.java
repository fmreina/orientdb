package com.orientechnologies.orient.core.constraint;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 
 * @author fabio
 *
 */
public interface OConstraintKeyUpdater<V> {

  OConstraintUpdateAction<V> update(V o, AtomicLong bonsayFileId);

}
