package com.orientechnologies.orient.core.constraint;

import static com.orientechnologies.common.util.OClassLoaderHelper.lookupProviderWithOrientClassLoader;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.orientechnologies.common.util.OCollections;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.storage.OStorage;

/**
 * Utility class to create constraint. New OConstraintFactory can be registered
 * <p/>
 * <p>
 * In order to be detected, factories must implement the {@link OConstraintFactory} interface.
 * </p>
 * <p/>
 * <p>
 * In addition to implementing this interface datasources should have a services file:<br>
 * <code>META-INF/services/com.orientechnologies.orient.core.constraint.OConstraintFactory</code>
 * </p>
 * <p/>
 * <p>
 * The file should contain a single line which gives the full name of the implementing class.
 * </p>
 * <p/>
 * <p>
 * Example:<br>
 * <code>org.mycompany.constraint.MyConstraintFactory</code>
 * </p>
 * 
 * @author fabio
 *
 */
public class OConstraints {

  private static Set<OConstraintFactory>       FACTORIES         = null;
  private static final Set<OConstraintFactory> DYNAMIC_FACTORIES = Collections.synchronizedSet(new HashSet<OConstraintFactory>());
  private static ClassLoader                   orientClassLoader = OConstraints.class.getClassLoader();

  /**
   * Cache a set of all factories.
   * 
   * @return Set<OConstraintFactory>
   */
  private static synchronized Set<OConstraintFactory> getFactories() {
    if (FACTORIES == null) {
      final Iterator<OConstraintFactory> ite = lookupProviderWithOrientClassLoader(OConstraintFactory.class, orientClassLoader);

      final Set<OConstraintFactory> factories = new HashSet<>();
      while (ite.hasNext()) {
        factories.add(ite.next());
      }
      factories.addAll(DYNAMIC_FACTORIES);
      FACTORIES = Collections.unmodifiableSet(factories);
    }
    return FACTORIES;
  }

  /**
   * @return Iterator of all constraint factories
   */
  public static Iterator<OConstraintFactory> getAllFactories() {
    return getFactories().iterator();
  }

  public static OConstraintFactory getFactory(String constraintType, String algorithm) {
    if (algorithm == null)
      algorithm = chooseDefaultConstraintAlgorithm(constraintType);

    final Iterator<OConstraintFactory> ite = getAllFactories();

    while (ite.hasNext()) {
      final OConstraintFactory factory = ite.next();
      if (factory.getTypes().contains(constraintType.toUpperCase()) && factory.getAlgorithms().contains(algorithm))
        return factory;
    }

    throw new OConstraintException("Constraint with type " + constraintType + " and algorithm " + algorithm + " does not exist.");
  }

  public static String chooseDefaultConstraintAlgorithm(String type) {
    String algorithm = null;

    if (OClass.CONSTRAINT_TYPE.CONDITIONAL.name().equalsIgnoreCase(type) || OClass.CONSTRAINT_TYPE.UNIQUE.name().equalsIgnoreCase(type)
        || OClass.CONSTRAINT_TYPE.REQUIRED_EDGE.name().equalsIgnoreCase(type) || OClass.CONSTRAINT_TYPE.IN_OUT_EDGE.name().equalsIgnoreCase(type)
        || OClass.CONSTRAINT_TYPE.CARDINALITY.name().equalsIgnoreCase(type) ) {
      algorithm = ODefaultConstraintFactory.SBTREE_ALGORITHM;
    }

    return algorithm;
  }

  /**
   * Iterates on all factries and appens all constraint types.
   * 
   * @return Set of all constraint types
   */
  public static Set<String> getConstraintTypes() {
    final Set<String> types = new HashSet<>();
    final Iterator<OConstraintFactory> ite = getAllFactories();
    while (ite.hasNext()) {
      types.addAll(ite.next().getTypes());
    }
    return types;
  }

  public static OConstraintInternal<?> createConstraint(OStorage storage, String name, String constraintType, String algorithm,
      String valueContainerAlgorithm, ODocument metadata, int version) throws OConstraintException, OConstraintException {
    return findFactoryByAlgorithmAndType(algorithm, constraintType).createConstraint(name, storage, constraintType, algorithm,
        valueContainerAlgorithm, metadata, version);
  }

  public static OConstraintFactory findFactoryByAlgorithmAndType(String algorithm, String constraintType) {
    for (OConstraintFactory factory : getFactories()) {
      if (constraintType == null || constraintType.isEmpty()
          || (factory.getTypes().contains(constraintType)) && factory.getAlgorithms().contains(algorithm)) {
        return factory;
      }
    }
    throw new OConstraintException("Constraint type " + constraintType + " with engine " + algorithm
        + "is not supported. Types are " + OCollections.toString(getConstraintTypes()));
  }

  public static OConstraintEngine createConstraintEngine(final String name, final String algorithm, final String type,
      final Boolean durableInNonTxMode, final OStorage storage, final int version, final Map<String, String> constraintProperties,
      ODocument metadata) {

    final OConstraintFactory factory = findFactoryByAlgorithmAndType(algorithm, type);

    return factory.createConstraintEngine(algorithm, name, durableInNonTxMode, storage, version, constraintProperties);
  }

}
