package com.orientechnologies.orient.core.constraint;

import java.util.List;
import java.util.Map;
import java.util.jar.Attributes.Name;

import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.db.record.ridbag.ORidBag;
import com.orientechnologies.orient.core.exception.OInvalidConstraintEngineIdException;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ODocumentEntry;
import com.orientechnologies.orient.core.record.impl.OEdgeDocument;
import com.orientechnologies.orient.core.record.impl.OVertexDocument;
import com.orientechnologies.orient.core.sql.parser.OExpression;
import com.orientechnologies.orient.core.sql.parser.OIdentifier;
import com.orientechnologies.orient.core.storage.OConstraintValidationException;
import com.orientechnologies.orient.core.storage.impl.local.OAbstractPaginatedStorage;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintCardinality extends OConstraintOneValue {

  private enum Direction {
    IN, // looks from the target node
    OUT // looks from the origin node
  }

  private final OConstraintEngine.Validator<Object, OIdentifiable> CONSTRAINT_VALIDATOR = new OConstraintEngine.Validator<Object, OIdentifiable>() {
    @Override
    public Object validate(Object key, OIdentifiable oldValue, OIdentifiable newValue, Map<String, List<OConstraintDto>> fields,
        String params) {

//      System.out.println("\nValidation of the CARDINALITY constraint.");
//      System.out.println("Params: " + params);
//      System.out.println("Entered Values:");
//      System.out.println("oldValue: " + oldValue);
//      System.out.println("newValue: "+newValue);
//      fields.keySet().forEach(k -> {
//        String cName = k;
//        System.out.println(cName);
//        List<OConstraintDto> listDto = fields.get(k);
//        listDto.forEach(dto -> {
//          Map<String, ODocumentEntry> m = dto.getAttributes();
//          m.keySet().forEach(s -> {
//            String attribute = s;
//            System.out.print("\t");
//            System.out.print(attribute);
//            System.out.print(" = ");
//            ODocumentEntry doc = m.get(s);
//            System.out.println(doc.value);
//          });
//          System.out.println();
//        });
//      });

      doValidate(params, fields);

       if (!newValue.getIdentity().isPersistent())
       newValue = newValue.getRecord();
       return newValue.getIdentity();
    }

    private void doValidate(String params, Map<String, List<OConstraintDto>> fields) {
      if (params != null && !params.isEmpty()) {
        OConstraintCardinalityParameters constraintParams = new OConstraintCardinalityParameters(params);

        OIdentifier originClassParam = constraintParams.getOriginClass();
        OIdentifier edgeTypeParam = constraintParams.getEdgeType();
        OIdentifier targetClassParam = constraintParams.getTargetClass();
        OExpression lowerLimitParam = constraintParams.getLowerLimit();
        OExpression upperLimitParam = constraintParams.getUpperLimit();

        List<OConstraintDto> originDtoListEntered = fields.get(originClassParam.getValue().toUpperCase());
        List<OConstraintDto> targetDtoListEntered = fields.get(targetClassParam.getValue().toUpperCase());
        List<OConstraintDto> edgeDtoListEntered = fields.get(edgeTypeParam.getValue().toUpperCase());
        if (originDtoListEntered == null)
          throw new OConstraintValidationException(
              String.format("Origin class entered does not match the origin class on constraint parameters. "
                  + "It was expected a node type '%s' but it was not found on the entered data.", originClassParam),
              getName());

        if (targetDtoListEntered != null && edgeDtoListEntered != null) { // TODO: check if it is necessary
          // checks one way of the restriction, that is, the upper limit (person -has(1:3)-> Car = person has maximum of 3 cars)
          doValidateDirection(Direction.OUT, originDtoListEntered, upperLimitParam, edgeDtoListEntered, edgeTypeParam,
              originClassParam, targetClassParam);
  
          // checks way back, that is, the lower limit (person -has(1:3)-> Car = car belongs to a maximum of 1 person)
          doValidateDirection(Direction.IN, targetDtoListEntered, lowerLimitParam, edgeDtoListEntered, edgeTypeParam,
              targetClassParam, originClassParam);
        }
      }
    }

    /**
     * @param direction
     *          - Direction.IN or Direction.OUT of the node
     * @param dtoListEntered
     *          - originDtoList (if outEdge) or tagertDtoList (if inEdge)
     * @param limitParam
     *          - upperLimit (if outEdge) or lowerLimit (if inEdge)
     * @param edgeListEntered
     * @param edgeTypeParam
     * @param thisSideNodeClassParam
     *          - origin (if outEdge) or target (if inEdge)
     * @param otherSideNodeParam
     *          - origin (if inEdge) or target (if outEdge)
     */
    private void doValidateDirection(Direction direction, List<OConstraintDto> dtoListEntered, OExpression limitParam,
        List<OConstraintDto> edgeListEntered, OIdentifier edgeTypeParam, OIdentifier thisSideNodeClassParam,
        OIdentifier otherSideNodeParam) {
      for (OConstraintDto dto : dtoListEntered) {
        ORidBag edgesOfTheNode = extractEdges(direction, dto);
        try {
          int limit = Integer.valueOf(limitParam.toString());

          for (OIdentifiable extractedEdge : edgesOfTheNode) {
            int edgeCounter = 0;
            for (OConstraintDto enteredEdge : edgeListEntered) {

              ODocument enteredEdgeDoc = (ODocument)extractedEdge;
              if (enteredEdge.getNameType().equalsIgnoreCase(enteredEdgeDoc.getClassName())) {
                // check the edge type
                if (enteredEdge.getName().equalsIgnoreCase(edgeTypeParam.getValue())) {
                  edgeCounter++;

                  // check the otherSideNode node
                  OVertexDocument otherSideNodeExtracted = extractOtherSideNode(enteredEdge, direction);

                  // if otherSideNode != otherSideNodeParam -> error!
                  if (!otherSideNodeParam.getValue().equalsIgnoreCase(otherSideNodeExtracted.getClassName())) {
                    String observedNode = Direction.IN.equals(direction) ? "In node" : "Out node";
                    throw new OConstraintValidationException(String.format(
                        "'%s' entered does not match the one on constraint parameters. It was expected %s but recieved %s.",
                        observedNode, otherSideNodeParam.getValue(), otherSideNodeExtracted.getClassName()), getName());
                  }
                }
              }
            }
            
            checkLimit(thisSideNodeClassParam, edgeTypeParam, edgeCounter, limit, direction);
          }

        } catch (NumberFormatException e) {
          if ("N".equalsIgnoreCase(limitParam.toString())) {
  //            // Internal message
  //            System.out.println(String.format("The %s limit is defined as 'N'. No need for validations",
  //                Direction.IN.equals(direction) ? "lower" : "upper"));
            break;
          }
        }
      }
    }

    private void checkLimit(OIdentifier thisSideNodeClassParam, OIdentifier edgeTypeParam, int edgeCounter, int limit,
        Direction direction) {
      String limitName = Direction.IN.equals(direction) ? "Lower" : "Upper";
      String edgeDirection = Direction.IN.equals(direction) ? "in edges" : "out edges";

      if (edgeCounter > limit)
        throw new OConstraintValidationException(String.format(
            "%s limit exceeded! "
                + "The operation is trying to create a node %s with more '%s', of the type %s, than it is allowed by the declared constraint. "
                + "You are trying to create %d edges but the maximun allowed are %s.",
            limitName, thisSideNodeClassParam, edgeDirection, edgeTypeParam, edgeCounter, limit), getName());
    }

    private OVertexDocument extractOtherSideNode(OConstraintDto dto, Direction direction) {
      OVertexDocument otherSideNode = Direction.IN.equals(direction) ? dto.extractOutNode() : dto.extractInNode();
      String nodeName = Direction.IN.equals(direction) ? "origin" : "target";

      if (otherSideNode == null)
        throw new OConstraintValidationException(
            String.format("Failed to retreive the entered %s node of the edge %s.", nodeName, dto.getName()), getName());

      return otherSideNode;
    }

    private ORidBag extractEdges(Direction direction, OConstraintDto dto) {
      switch (direction) {
      case IN:
        return dto.extractInEdges();
      case OUT:
        return dto.extractOutEdges();
      default:
        throw new OConstraintValidationException("Failed to retrieve the associated edges of the node. Direction not identified.",
            getName());
      }
    }

  };

  public OConstraintCardinality(String name, final String type, String algorithm, int version, OAbstractPaginatedStorage storage,
      String valueContainerAlgorithm, ODocument metadata) {
    super(name, type, algorithm, valueContainerAlgorithm, metadata, version, storage);
  }

  @Override
  public OConstraint<OIdentifiable> put(Object key, final OIdentifiable value, Map<String, List<OConstraintDto>> fields) {
    key = getCollatingValue(key);

    acquireSharedLock();
    try {
      while (true) {
        try {
          storage.validatedPutConstraintValue(constraintId, key, value, CONSTRAINT_VALIDATOR, fields,
              getConstraintDefinition().getConstraintParams());
          return this;
        } catch (OInvalidConstraintEngineIdException ignore) {
          doReloadConstraintEngine();
        }
      }

    } finally {
      releaseSharedLock();
    }
  }

  @Override
  public boolean supportsOrderedIterations() {
    return false;
  }

}
