package com.orientechnologies.orient.core.constraint;

import java.io.Serializable;

import com.orientechnologies.orient.core.sql.parser.OExpression;
import com.orientechnologies.orient.core.sql.parser.OIdentifier;
import com.orientechnologies.orient.core.sql.parser.OrientSqlConstants;
import com.orientechnologies.orient.core.sql.parser.Token;

/**
 * 
 * @author fabio
 *
 */
public class OConditionalParameters implements OConstraintParameters, Serializable{
  
  private static final long serialVersionUID = 7566661740245570585L;

  private OIdentifier          ifPropertyName;
  private OExpression          ifPropertyValue;
  private Token                ifTokenCondition;
  private OIdentifier          thenPropertyName;
  private OExpression          thenPropertyValue;
  private Token                thenTokenCondition;
  private OIdentifier          elsePropertyName;
  private OExpression          elsePropertyValue;
  private Token                elseTokenCondition;
  
  public OConditionalParameters(OIdentifier ifPropertyName, OExpression ifPropertyValue, Token ifTokenCondition,
      OIdentifier thenPropertyName, OExpression thenPropertyValue, Token thenTokenCondition, OIdentifier elsePropertyName,
      OExpression elsePropertyValue, Token elseTokenCondition) {
    super();
    this.ifPropertyName = ifPropertyName;
    this.ifPropertyValue = ifPropertyValue;
    this.ifTokenCondition = ifTokenCondition;
    this.thenPropertyName = thenPropertyName;
    this.thenPropertyValue = thenPropertyValue;
    this.thenTokenCondition = thenTokenCondition;
    this.elsePropertyName = elsePropertyName;
    this.elsePropertyValue = elsePropertyValue;
    this.elseTokenCondition = elseTokenCondition;
  }
  
  public OConditionalParameters(String params){
    strToParam(params);
  }
      
  @Override
  public OConstraintParameters strToParam(String str) {
    //FIXME
    String[] props = str.split(" ");
    //TODO - FABIO: fazer a conversão dos valores de string para os artibutos desse objeto para retornar um objeto desse tipo
    ifPropertyName = new OIdentifier(props[1]);
    ifTokenCondition = new Token(getTokenKind(props[2]), props[2]);
    ifPropertyValue = new OExpression(new OIdentifier(props[3]));
    
    thenPropertyName = new OIdentifier(props[5]);
    thenTokenCondition = new Token(getTokenKind(props[6]), props[6]);
    thenPropertyValue = new OExpression(new OIdentifier(props[7]));
    
    elsePropertyName = new OIdentifier(props[9]);
    elseTokenCondition = new Token(getTokenKind(props[10]), props[10]);
    elsePropertyValue = new OExpression(new OIdentifier(props[11]));
    
    return this;
  }
  
  private int getTokenKind(String str){
    switch (str) {
    case "==":
      return OrientSqlConstants.EQ;
    case "!=":
      return OrientSqlConstants.NE;
    case ">":
      return OrientSqlConstants.GT;
    case "<":
      return OrientSqlConstants.LT;
    case ">=":
      return OrientSqlConstants.GE;
    case "<=":
      return OrientSqlConstants.LE;
    default:
      return -1;
    }
  }
  
  @Override
  public String toString() {
    String msg = "IF " + ifPropertyName + " " + ifTokenCondition + " " + ifPropertyValue + 
        " THEN " + thenPropertyName + " " + thenTokenCondition + " " + thenPropertyValue;
    if (elsePropertyName != null && elseTokenCondition != null && elsePropertyValue != null)
      msg += " " + "ELSE " + elsePropertyName + " " + elseTokenCondition + " " + elsePropertyValue;
    msg += " ";
    return msg;
  }

  public OIdentifier getIfPropertyName() {
    return ifPropertyName;
  }

  public OExpression getIfPropertyValue() {
    return ifPropertyValue;
  }

  public Token getIfTokenCondition() {
    return ifTokenCondition;
  }

  public OIdentifier getThenPropertyName() {
    return thenPropertyName;
  }

  public OExpression getThenPropertyValue() {
    return thenPropertyValue;
  }

  public Token getThenTokenCondition() {
    return thenTokenCondition;
  }

  public OIdentifier getElsePropertyName() {
    return elsePropertyName;
  }

  public OExpression getElsePropertyValue() {
    return elsePropertyValue;
  }

  public Token getElseTokenCondition() {
    return elseTokenCondition;
  }

}
