package com.orientechnologies.orient.core.storage.ridbag.sbtree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import com.orientechnologies.common.serialization.types.OBooleanSerializer;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.serialization.serializer.binary.impl.OLinkSerializer;
import com.orientechnologies.orient.core.storage.impl.local.OAbstractPaginatedStorage;
import com.orientechnologies.orient.core.storage.impl.local.paginated.atomicoperations.OAtomicOperation;
import com.orientechnologies.orient.core.storage.index.sbtree.OSBTreeMapEntryIterator;
import com.orientechnologies.orient.core.storage.index.sbtree.OTreeInternal;
import com.orientechnologies.orient.core.storage.index.sbtreebonsai.local.OBonsaiBucketPointer;
import com.orientechnologies.orient.core.storage.index.sbtreebonsai.local.OSBTreeBonsaiLocal;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintRIDContainerSBTree implements Set<OIdentifiable> {
  public static final String CONSTRAINT_FILE_EXTENSION = ".ctr";

  /**
   * Generates a lock name for the given constraint name.
   *
   * @param constraintName
   *          the constraint name to generate the lock name for.
   *
   * @return the generated lock name.
   */
  public static String generateLockName(String constraintName) {
    return constraintName + CONSTRAINT_FILE_EXTENSION;
  }

  private final OSBTreeBonsaiLocal<OIdentifiable, Boolean> tree;

  public OConstraintRIDContainerSBTree(long fileId, OAbstractPaginatedStorage storage) {
    String fileName;

    OAtomicOperation atomicOperation = storage.getAtomicOperationsManager().getCurrentOperation();
    if (atomicOperation == null)
      fileName = storage.getWriteCache().fileNameById(fileId);
    else
      fileName = atomicOperation.fileNameById(fileId);

    tree = new OSBTreeBonsaiLocal<>(fileName.substring(0, fileName.length() - CONSTRAINT_FILE_EXTENSION.length()),
        CONSTRAINT_FILE_EXTENSION, storage);

    tree.create(OLinkSerializer.INSTANCE, OBooleanSerializer.INSTANCE);
  }

  public OConstraintRIDContainerSBTree(long fileId, OBonsaiBucketPointer rootPointer, boolean durableMode,
      OAbstractPaginatedStorage storage) {
    String fileName;

    OAtomicOperation atomicOperation = storage.getAtomicOperationsManager().getCurrentOperation();
    if (atomicOperation == null)
      fileName = storage.getWriteCache().fileNameById(fileId);
    else
      fileName = atomicOperation.fileNameById(fileId);

    tree = new OSBTreeBonsaiLocal<>(fileName.substring(0, fileName.length() - CONSTRAINT_FILE_EXTENSION.length()),
        CONSTRAINT_FILE_EXTENSION, storage);
    tree.load(rootPointer);
  }

  public OConstraintRIDContainerSBTree(String file, OBonsaiBucketPointer rootPointer, boolean durableMode,
      OAbstractPaginatedStorage storage) {
    tree = new OSBTreeBonsaiLocal<>(file, CONSTRAINT_FILE_EXTENSION, storage);
    tree.load(rootPointer);
  }

  public OBonsaiBucketPointer getRootPointer() {
    return tree.getRootBucketPointer();
  }

  @Override
  public int size() {
    return (int) tree.size();
  }

  @Override
  public boolean isEmpty() {
    return tree.size() == 0L;
  }

  @Override
  public boolean contains(Object o) {
    return o instanceof OIdentifiable && contains((OIdentifiable) o);
  }

  public boolean contains(OIdentifiable o) {
    return tree.get(o) != null;
  }

  @Override
  public Iterator<OIdentifiable> iterator() {
    return new TreeKeyIterator(tree, false);
  }

  @Override
  public Object[] toArray() {
    // TODO replace with more efficient implementation

    final ArrayList<OIdentifiable> list = new ArrayList<>(size());

    list.addAll(this);

    return list.toArray();
  }

  @SuppressWarnings("SuspiciousToArrayCall")
  @Override
  public <T> T[] toArray(T[] a) {
    // TODO replace with more efficient implementation.

    final ArrayList<OIdentifiable> list = new ArrayList<>(size());

    list.addAll(this);

    return list.toArray(a);
  }

  @Override
  public boolean add(OIdentifiable oIdentifiable) {
    return this.tree.put(oIdentifiable, Boolean.TRUE);
  }

  @Override
  public boolean remove(Object o) {
    return o instanceof OIdentifiable && remove((OIdentifiable) o);
  }

  public boolean remove(OIdentifiable o) {
    return tree.remove(o) != null;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    for (Object e : c)
      if (!contains(e))
        return false;
    return true;
  }

  @Override
  public boolean addAll(Collection<? extends OIdentifiable> c) {
    boolean modified = false;
    for (OIdentifiable e : c)
      if (add(e))
        modified = true;
    return modified;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    boolean modified = false;
    Iterator<OIdentifiable> it = iterator();
    while (it.hasNext()) {
      if (!c.contains(it.next())) {
        it.remove();
        modified = true;
      }
    }
    return modified;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    boolean modified = false;
    for (Object o : c) {
      modified |= remove(o);
    }

    return modified;
  }

  @Override
  public void clear() {
    tree.clear();
  }

  public void delete() {
    tree.delete();
  }

  public String getName() {
    return tree.getName();
  }

  private static class TreeKeyIterator implements Iterator<OIdentifiable> {
    private final boolean                                         autoConvertToRecord;
    private final OSBTreeMapEntryIterator<OIdentifiable, Boolean> entryIterator;

    public TreeKeyIterator(OTreeInternal<OIdentifiable, Boolean> tree, boolean autoConvertToRecord) {
      entryIterator = new OSBTreeMapEntryIterator<>(tree);
      this.autoConvertToRecord = autoConvertToRecord;
    }

    @Override
    public boolean hasNext() {
      return entryIterator.hasNext();
    }

    @Override
    public OIdentifiable next() {
      final OIdentifiable identifiable = entryIterator.next().getKey();
      if (autoConvertToRecord)
        return identifiable.getRecord();
      else
        return identifiable;
    }

    @Override
    public void remove() {
      entryIterator.remove();
    }
  }
}
