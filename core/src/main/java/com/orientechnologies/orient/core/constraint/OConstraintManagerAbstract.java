package com.orientechnologies.orient.core.constraint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.orientechnologies.common.concur.resource.OCloseable;
import com.orientechnologies.common.log.OLogManager;
import com.orientechnologies.common.util.OMultiKey;
import com.orientechnologies.orient.core.config.OStorageConfiguration;
import com.orientechnologies.orient.core.db.ODatabaseDocumentInternal;
import com.orientechnologies.orient.core.db.ODatabaseRecordThreadLocal;
import com.orientechnologies.orient.core.db.OMetadataUpdateListener;
import com.orientechnologies.orient.core.db.OScenarioThreadLocal;
import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.exception.OConcurrentModificationException;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.metadata.OMetadata;
import com.orientechnologies.orient.core.metadata.OMetadataDefault;
import com.orientechnologies.orient.core.metadata.OMetadataInternal;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.storage.OStorage;
import com.orientechnologies.orient.core.type.ODocumentWrapper;
import com.orientechnologies.orient.core.type.ODocumentWrapperNoClass;

/**
 * Abstract class to manage constraints
 * 
 * @author fabio
 */

@SuppressWarnings({ "unchecked", "serial" })
public abstract class OConstraintManagerAbstract extends ODocumentWrapperNoClass implements OConstraintManager, OCloseable {
  public static final String                                       CONFIG_CONSTRAINTS      = "constraints";
  public static final String                                       CONDITIONAL_NAME             = "conditional";

  protected final Map<String, Map<OMultiKey, Set<OConstraint<?>>>> classPropertyConstraint = new ConcurrentHashMap<String, Map<OMultiKey, Set<OConstraint<?>>>>();
  protected Map<String, OConstraint<?>>                            constraints             = new ConcurrentHashMap<String, OConstraint<?>>();
  protected String                                                 defaultClusterName      = OMetadataDefault.CLUSTER_CONSTRAINT_NAME;
  protected String                                                 manualClusterName       = OMetadataDefault.CLUSTER_MANUAL_CONSTRAINT_NAME;

  private AtomicInteger                                            writeLockNesting        = new AtomicInteger();
  protected ReadWriteLock                                          lock                    = new ReentrantReadWriteLock();

  public OConstraintManagerAbstract() {
    super(new ODocument().setTrackingChanges(false));
  }

  @Override
  public OConstraintManagerAbstract load() {
    throw new UnsupportedOperationException();
  }

  public OConstraintManagerAbstract load(ODatabaseDocumentInternal database) {
    // if(!autoRecreateConstraintsAfterCrash(database)){
    acquireExclusiveLock();
    try {
      if (database.getStorage().getConfiguration().getConstraintMgrRecordId() == null)
        create(database); // @COMPATIBILITY: CREATE THE CONSTRAINT MGR

      // RELOAD IT
      ((ORecordId) document.getIdentity()).fromString(database.getStorage().getConfiguration().getConstraintMgrRecordId());
      super.reload("*:-1 constraint:0");
    } finally {
      releaseExclusiveLock();
    }
    // }
    return this;
  }

  @Override
  public OConstraintManagerAbstract reload() {
    acquireExclusiveLock();
    try {
      return super.reload();
    } finally {
      releaseExclusiveLock();
    }
  }

  public Collection<? extends OConstraint<?>> getConstraints(ODatabaseDocumentInternal database) {
    final Collection<OConstraint<?>> rawResult = constraints.values();
    final List<OConstraint<?>> result = new ArrayList<OConstraint<?>>(rawResult.size());
    for (final OConstraint<?> constraint : rawResult)
      result.add(preProcessBeforeReturn(database, constraint));
    return result;
  }

  @Override
  public Collection<? extends OConstraint<?>> getConstraints() {
    throw new UnsupportedOperationException();
  }

  @Override
  public OConstraint<?> getConstraint(final String iName) {
    final Locale locale = getServerLocale();

    final OConstraint<?> constraint = constraints.get(iName);
    if (constraint == null)
      return null;
    return preProcessBeforeReturn(getDatabase(), constraint);
  }

  @Override
  public <RET extends ODocumentWrapper> RET save() {
    OScenarioThreadLocal.executeAsDistributed(new Callable<Object>() {

      @Override
      public Object call() throws Exception {
        acquireExclusiveLock();
        try {
          boolean saved = false;
          for (int retry = 0; retry < 10; retry++)
            try {
              toStream();
              document.save();
              saved = true;
              break;
            } catch (OConcurrentModificationException e) {
              OLogManager.instance().debug(this, "concurrent modification while saving constraint manager configuration", e);
              reload(null, true);
            }
          if (!saved)
            OLogManager.instance().error(this, "failed to save the constraintmanager configuration after 10 retries", null);

          return null;
        } finally {
          releaseExclusiveLock();
        }
      }
    });

    return (RET) this;
  }

  public void create(ODatabaseDocumentInternal database) {
    acquireExclusiveLock();
    try {
      try {
        save(OMetadataDefault.CLUSTER_INTERNAL_NAME);
      } catch (Exception e) {
        OLogManager.instance().error(this, "Error during storing of consraint manager metadata,"
            + " will try to allocate new document to store constraint manager metadata", e);

        // RESET RID TO ALLOCATE A NEW ONE
        if (ORecordId.isPersistent(document.getIdentity().getClusterPosition())) {
          document.getIdentity().reset();
          save(OMetadataDefault.CLUSTER_INTERNAL_NAME);
        }
      }
      database.getStorage().setConstraintMgrRecordId(document.getIdentity().toString());

      OConstraintFactory factory = OConstraints.getFactory(OClass.CONSTRAINT_TYPE.CONDITIONAL.toString(), null);
      // creates a constraint on initialization if not created yet
      createConstraint(CONDITIONAL_NAME, OClass.CONSTRAINT_TYPE.CONDITIONAL.toString(),
          new OSimpleKeyConstraintDefinition(factory.getLastVersion(), OType.STRING), null, null, null);
    } finally {
      releaseExclusiveLock();
    }
  }

  public OConstraintManager setDirty() {
    acquireExclusiveLock();
    try {
      document.setDirty();
      return this;
    } finally {
      releaseExclusiveLock();
    }
  }

  @Override
  public boolean existsConstraint(final String iName) {
    final Locale locale = getServerLocale();
    return constraints.containsKey(iName);
  }

  @Override
  public void close() {
    constraints.clear();
    classPropertyConstraint.clear();
  }

  protected void acquireSharedLock() {
    lock.readLock().lock();
  }

  protected void releaseSharedLock() {
    lock.readLock().unlock();

  }

  protected void acquireExclusiveLock() {
    internalAcquireExclusiveLock();
    writeLockNesting.incrementAndGet();
  }

  protected void internalAcquireExclusiveLock() {
    final ODatabaseDocument databaseRecord = getDatabaseIfDefined();
    if (databaseRecord != null && !databaseRecord.isClosed()) {
      final OMetadataInternal metadata = (OMetadataInternal) databaseRecord.getMetadata();
      if (metadata != null)
        metadata.makeThreadLocalSchemaSnapshot();
    }

    lock.writeLock().lock();
  }

  protected void releaseExclusiveLock() {
    int val = writeLockNesting.decrementAndGet();
    internalReleaseExclusiveLock();
    if (val == 0) {
      ODatabaseDocumentInternal database = getDatabase();
      for (OMetadataUpdateListener listener : database.getSharedContext().browseListeners()) {
        listener.onConstraintManagerUpdate(database.getName(), this);
      }
    }
  }

  protected void internalReleaseExclusiveLock() {
    lock.writeLock().unlock();

    final ODatabaseDocument databaseRecord = getDatabaseIfDefined();
    if (databaseRecord != null && !databaseRecord.isClosed()) {
      final OMetadata metadata = databaseRecord.getMetadata();
      if (metadata != null)
        ((OMetadataInternal) metadata).clearThreadLocalSchemaSnapshot();
    }
  }

  protected void clearMetadata() {
    acquireExclusiveLock();
    try {
      constraints.clear();
      classPropertyConstraint.clear();
    } finally {
      releaseExclusiveLock();
    }
  }

  protected static ODatabaseDocumentInternal getDatabase() {
    return ODatabaseRecordThreadLocal.instance().get();
  }

  protected abstract OStorage getStorage();

  protected ODatabaseDocumentInternal getDatabaseIfDefined() {
    return ODatabaseRecordThreadLocal.instance().getIfDefined();
  }

  protected void addConstraintInternal(final OConstraint<?> constraint) {
    acquireExclusiveLock();
    try {
      final Locale locale = getServerLocale();
      constraints.put(constraint.getName(), constraint);

      final OConstraintDefinition constraintDefinition = constraint.getDefinition();

      if (constraintDefinition == null || constraintDefinition.getClassName() == null)
        return;

      Map<OMultiKey, Set<OConstraint<?>>> propertyConstraint = getConstraintOnProperty(constraintDefinition.getClassName());

      if (propertyConstraint == null) {
        propertyConstraint = new HashMap<OMultiKey, Set<OConstraint<?>>>();
      } else {
        propertyConstraint = new HashMap<OMultiKey, Set<OConstraint<?>>>(propertyConstraint);
      }

      final int paramCount = constraintDefinition.getParamCount();

      for (int i = 1; i <= paramCount; i++) {
        final List<String> fields = constraintDefinition.getFields().subList(0, i);
        final OMultiKey multiKey = new OMultiKey(normalizeFieldNames(fields));
        Set<OConstraint<?>> constraintSet = propertyConstraint.get(multiKey);

        if (constraintSet == null)
          constraintSet = new HashSet<OConstraint<?>>();
        else
          constraintSet = new HashSet<OConstraint<?>>(constraintSet);

        constraintSet.add(constraint);
        propertyConstraint.put(multiKey, constraintSet);
      }

      classPropertyConstraint.put(constraintDefinition.getClassName().toLowerCase(locale), copyPropertyMap(propertyConstraint));
    } finally {
      releaseExclusiveLock();
    }
  }

  protected static Map<OMultiKey, Set<OConstraint<?>>> copyPropertyMap(Map<OMultiKey, Set<OConstraint<?>>> original) {
    final Map<OMultiKey, Set<OConstraint<?>>> result = new HashMap<OMultiKey, Set<OConstraint<?>>>();

    for (Map.Entry<OMultiKey, Set<OConstraint<?>>> entry : original.entrySet()) {
      Set<OConstraint<?>> constraints = new HashSet<OConstraint<?>>(entry.getValue());
      assert constraints.equals(entry.getValue());

      result.put(entry.getKey(), Collections.unmodifiableSet(constraints));
    }

    assert result.equals(original);

    return Collections.unmodifiableMap(result);
  }

  protected List<String> normalizeFieldNames(final Collection<String> fieldNames) {
    final Locale locale = getServerLocale();
    final ArrayList<String> result = new ArrayList<String>(fieldNames.size());
    for (final String fieldName : fieldNames)
      result.add(fieldName);
    return result;
  }

  public abstract OConstraint<?> preProcessBeforeReturn(ODatabaseDocumentInternal database, final OConstraint<?> constraint);

  protected Locale getServerLocale() {
    OStorage storage = getStorage();
    OStorageConfiguration configuration = storage.getConfiguration();
    return configuration.getLocaleInstance();
  }

  private Map<OMultiKey, Set<OConstraint<?>>> getConstraintOnProperty(final String className) {
    final Locale locale = getServerLocale();

    acquireSharedLock();
    try {
      return classPropertyConstraint.get(className.toLowerCase(locale));
    } finally {
      releaseSharedLock();
    }
  }
  
  @Override
  public Set<OConstraint<?>> getClassConstraints(String className) {
    final HashSet<OConstraint<?>> coll = new HashSet<OConstraint<?>>(4);
    getClassConstraints(className, coll);
    return coll;
  }
  
  @Override
  public void getClassConstraints(final String className, final Collection<OConstraint<?>> constraints) {
    final Map<OMultiKey, Set<OConstraint<?>>> propertyConstraint = getConstraintOnProperty(className);

    if (propertyConstraint == null)
      return;

    for (final Set<OConstraint<?>> propertyConstraints : propertyConstraint.values())
      for (final OConstraint<?> constraint : propertyConstraints)
        constraints.add(preProcessBeforeReturn(getDatabase(), constraint));
  }
  
  
  public void getClassRawConstraints(final String className, final Collection<OConstraint<?>> constraints) {
    final Map<OMultiKey, Set<OConstraint<?>>> propertyConstraint = getConstraintOnProperty(className);

    if (propertyConstraint == null)
      return;

    for (final Set<OConstraint<?>> propertyConstraints : propertyConstraint.values())
      for (final OConstraint<?> constraint : propertyConstraints)
        constraints.add(constraint);
  }
  
}
