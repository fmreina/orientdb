package com.orientechnologies.orient.core.constraint;

import java.util.Map;

import com.orientechnologies.orient.core.db.record.OMultiValueChangeEvent;

/**
 * 
 * @author fabio
 *
 */
public interface OConstraintDefinitionMultiValue extends OConstraintDefinition {

  /**
   * Converts passed in value in the key of single constraint entry.
   * 
   * @param param
   *          Value to convert
   * 
   * @return Constraint key
   */
  public Object createSingleValue(final Object... param);

  /**
   * Process event that contains operation on collection and extract values that should be added removed from constraint to reflect
   * collection changes in the given constraint.
   *
   * @param changeEvent
   *          Event that describes operation that was performed on collection.
   * @param keysToAdd
   *          Values that should be added to related constraint.
   * @param keysToRemove
   *          Values that should be removed to related constraint.
   */
  public void processChangeEvent(final OMultiValueChangeEvent<?, ?> changeEvent, final Map<Object, Integer> keysToAdd,
      final Map<Object, Integer> keysToRemove);
}
