package com.orientechnologies.orient.core.constraint;

import com.orientechnologies.orient.core.constraint.OConstraintUpdateAction;

/**
 * 
 * @author fabio
 *
 * @param <V>
 */
public abstract class OConstraintUpdateAction<V> {

  private static OConstraintUpdateAction nothing = new OConstraintUpdateAction() {
    @Override
    public boolean isNothing() {
      return true;
    }

    @Override
    public boolean isChange() {
      return false;
    }

    @Override
    public boolean isRemove() {
      return false;
    }

    @Override
    public Object getValue() {
      throw new UnsupportedOperationException();
    }
  };

  private static OConstraintUpdateAction remove = new OConstraintUpdateAction() {
    @Override
    public boolean isNothing() {
      return false;
    }

    @Override
    public boolean isChange() {
      return false;
    }

    @Override
    public boolean isRemove() {
      return true;
    }

    @Override
    public Object getValue() {
      throw new UnsupportedOperationException();
    }
  };

  public static OConstraintUpdateAction nothing() {
    return nothing;
  }

  public static OConstraintUpdateAction remove() {
    return remove;
  }

  public static <V> OConstraintUpdateAction<V> changed(V newValue) {
    return new OConstraintUpdateAction() {
      @Override
      public boolean isChange() {
        return true;
      }

      @Override
      public boolean isRemove() {
        return false;
      }

      @Override
      public boolean isNothing() {
        return false;
      }

      @Override
      public Object getValue() {
        return newValue;
      }
    };
  }

  public abstract boolean isChange();

  public abstract boolean isRemove();

  public abstract boolean isNothing();

  public abstract V getValue();
}
