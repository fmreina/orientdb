package com.orientechnologies.orient.core.exception;

import com.orientechnologies.common.exception.OHighLevelException;

/**
 * 
 * @author fabio
 *
 */
public class OTooBigConstraintKeyException extends OCoreException implements OHighLevelException {
  public OTooBigConstraintKeyException(OCoreException exception) {
    super(exception);
  }

  public OTooBigConstraintKeyException(String message, String componentName) {
    super(message, componentName);
  }
}
