package com.orientechnologies.orient.core.storage.constraint.engine;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.orientechnologies.common.serialization.types.OBinarySerializer;
import com.orientechnologies.orient.core.constraint.OConstraintDefinition;
import com.orientechnologies.orient.core.constraint.OConstraintDto;
import com.orientechnologies.orient.core.constraint.OConstraintEngine;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.encryption.OEncryption;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ODocumentEntry;

/**
 * 
 * @author fabio
 *
 */
public class ORemoteConstraintEngine implements OConstraintEngine {

  private String name;

  public ORemoteConstraintEngine(String name) {
    this.name = name;
  }

  @Override
  public long size(ValuesTransformer transformer) {
    return 0;
  }

  public String getName() {
    return name;
  }

  @Override
  public void init(String constraintName, String constraintType, OConstraintDefinition constraintDefinition, boolean isAutomatic,
      ODocument metadata) {
  }

  @Override
  public void create(OBinarySerializer valueSerializer, boolean isAutomatic, OType[] keyTypes, boolean nullPointerSupport,
      OBinarySerializer keySerializer, int keySize, Set<String> clustersToConstraint, Map<String, String> engineProperties,
      ODocument metadata, OEncryption encryption) {
  }

  @Override
  public void delete() {
  }

  @Override
  public void load(String constraintName, OBinarySerializer valueSerializer, boolean isAutomatic, OBinarySerializer keySerializer,
      OType[] keyTypes, boolean nullPointerSupport, int keySize, Map<String, String> engineProperties, OEncryption encryption) {
  }

  @Override
  public int getVersion() {
    return -1;
  }

  @Override
  public boolean contains(Object key) {
    return false;
  }
  
  @Override
  public boolean remove(Object key) {
    return false;
  }

  @Override
  public void clear() {
  }

  @Override
  public Object get(Object key) {
    return null;
  }

  @Override
  public void put(Object key, Object value) {
  }

  @Override
  public boolean validatedPut(Object key, OIdentifiable value, Validator<Object, OIdentifiable> validator, Map<String, List<OConstraintDto>> fields, String params) {
    return false;
  }

  @Override
  public String getConstraintNameByKey(Object key) {
    return name;
  }
  
  @Override
  public boolean acquireAtomicExclusiveLock(Object key) {
    throw new UnsupportedOperationException("atomic locking is not supported by remote constraint engine");
  }
}
