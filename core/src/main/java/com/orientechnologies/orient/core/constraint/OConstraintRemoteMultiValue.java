package com.orientechnologies.orient.core.constraint;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.executor.OResultSet;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintRemoteMultiValue extends OConstraintRemote<Collection<OIdentifiable>> {

  protected final static String QUERY_GET = "select expand( rid ) from constraint:`%s` where key = ?";

  public OConstraintRemoteMultiValue(final String iName, final String iWrappedType, final String algorithm, final ORID iRid,
      final OConstraintDefinition iConstraintDefinition, final ODocument iConfiguration, final Set<String> clustersToIndex,
      String database) {
    super(iName, iWrappedType, algorithm, iRid, iConstraintDefinition, iConfiguration, clustersToIndex, database);
  }

  @Override
  public Collection<OIdentifiable> get(final Object iKey) {
    try (final OResultSet result = getDatabase().indexQuery(getName(), String.format(QUERY_GET, name), iKey)) {
      return result.stream().map((res) -> res.getIdentity().orElse(null)).collect(Collectors.toSet());
    }
  }

  public Iterator<Entry<Object, Collection<OIdentifiable>>> iterator() {
    try (final OResultSet result = getDatabase().constraintQuery(getName(), String.format(QUERY_ENTRIES, name))) {

      final Map<Object, Collection<OIdentifiable>> map = result.stream()
          .collect(Collectors.toMap((r) -> r.getProperty("key"), (r) -> r.getProperty("rid")));
      return map.entrySet().iterator();
    }
  }

  // public Iterator<OIdentifiable> valuesIterator() {
  // throw new UnsupportedOperationException();
  // }
  //
  // public Iterator<OIdentifiable> valuesInverseIterator() {
  // throw new UnsupportedOperationException();
  // }

  @Override
  public boolean supportsOrderedIterations() {
    return false;
  }
}
