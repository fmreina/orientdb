package com.orientechnologies.orient.core.constraint;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.orientechnologies.common.listener.OProgressListener;
import com.orientechnologies.common.util.OApi;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ODocumentEntry;

/**
 * Basic inerface to handle constraint
 * 
 * @author fabio
 *
 * @param <T>
 */
public interface OConstraint<T> extends Comparable<OConstraint<T>> {

  String CONSTRAINT_KEYS = "constraintKeys";
  
  /**
   * Creates the constraint.
   * 
   * @param name
   * @param constraintDefinition
   * @param clusterConstraintName
   * @param clusterToConstraint
   * @param rebuild
   * @param progressListener
   * @return
   */
  OConstraint<T> create(String name, OConstraintDefinition constraintDefinition, String clusterConstraintName,
      Set<String> clusterToConstraint, boolean rebuild, OProgressListener progressListener);

  String getDatabaseName();

  /**
   * Gets the set of records associated with the passed key.
   *
   * @param iKey
   *          The key to search
   *
   * @return The Record set if found, otherwise an empty Set
   */
  T get(Object iKey);

  /**
   * @return number of entries.
   */
  long getSize();

  /**
   * @return Number of keys in constraint
   */
  long getKeySize();
  
  /**
   * Delete the constraint.
   *
   * @return The constraint instance itself to allow in chain calls
   */
  @OApi(enduser = false)
  OConstraint<T> delete();

  /**
   * @return The name of the constraint
   */
  String getName();

  /**
   * Returns the type of the index as string.
   */
  String getType();

  ODocument getMetadata();

  /**
   * Inserts a new entry in the constraint. The behaviour depends by the constraintimplementation.
   *
   * @param iKey
   *          Entry's key
   * @param iValue
   *          Entry's value as OIdentifiable instance
   * @param fields Map of the entered fields and its values for validation
   *
   * @return The constraint instance itself to allow in chain calls
   */
  OConstraint<T> put(Object iKey, OIdentifiable iValue,  Map<String, List<OConstraintDto>> fields);

  /**
   * Removes an entry by its key.
   *
   * @param key The entry's key to remove
   *
   * @return True if the entry has been found and removed, otherwise false
   */
  boolean remove(Object key);

  /**
   * Removes an entry by its key and value.
   *
   * @param iKey The entry's key to remove
   *
   * @return True if the entry has been found and removed, otherwise false
   */
  boolean remove(Object iKey, OIdentifiable iRID);
//  boolean remove(Object iKey, OConditionalDTO iRID);

  /**
   * Clears the constraint removing all the entries in one shot.
   *
   * @return The constraint instance itself to allow in chain calls
   */
  OConstraint<T> clear();
  
  /**
   * Tells if the constraint is automatic. Automatic means it's maintained automatically by OrientDB. This is the case of
   * constraints created against schema properties. Automatic constraints can always been rebuilt.
   *
   * @return True if the constraint is automatic, otherwise false
   */
  boolean isAutomatic();

  /**
   * Returns the constraint configuration
   * 
   * @return an ODocument object containing all the constraint properties
   */
  ODocument getConfiguration();

  /**
   * Returns the internal constraint used.
   */
  OConstraintInternal<T> getInternal();

  OConstraintDefinition getDefinition();

  /**
   * Returns Names of clusters that will be constrained.
   *
   * @return Names of clusters that will be constrained.
   */
  Set<String> getClusters();

  boolean supportsOrderedIterations();

  int getConstraintId();

  boolean isUnique();
}
