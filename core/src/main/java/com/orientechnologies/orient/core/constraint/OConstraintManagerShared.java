package com.orientechnologies.orient.core.constraint;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.orientechnologies.common.exception.OException;
import com.orientechnologies.common.listener.OProgressListener;
import com.orientechnologies.common.log.OLogManager;
import com.orientechnologies.common.util.OMultiKey;
import com.orientechnologies.orient.core.config.OGlobalConfiguration;
import com.orientechnologies.orient.core.db.ODatabase;
import com.orientechnologies.orient.core.db.ODatabaseDocumentInternal;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.db.record.ORecordElement;
import com.orientechnologies.orient.core.db.record.OTrackedSet;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OClassImpl;
import com.orientechnologies.orient.core.metadata.schema.OSchemaShared;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.storage.OStorage;

/**
 * Manages constraints at database level. A single instance is shared among multiple databases. Contentions are managed r/w locks.
 * 
 * @author fabio
 */
public class OConstraintManagerShared extends OConstraintManagerAbstract {

  private static final long serialVersionUID = 1L;

  private OStorage          storage;

  public OConstraintManagerShared(OStorage storage) {
    super();
    this.storage = storage;
  }

  @Override
  public OConstraint<?> createConstraint(String iName, String iType, OConstraintDefinition constraintDefinition,
      int[] clusterIdsToConstraint, OProgressListener progressListener, ODocument metadata) {
    return createConstraint(iName, iType, constraintDefinition, clusterIdsToConstraint, progressListener, metadata, null);
  }

  @Override
  public OConstraint<?> createConstraint(String iName, String iType, OConstraintDefinition constraintDefinition,
      int[] clusterIdsToConstraint, OProgressListener progressListener, ODocument metadata, String algorithm) {
    if (getDatabase().getTransaction().isActive())
      throw new IllegalStateException("Cannot create a new constraint inside a transaction");

    final Character c = OSchemaShared.checkFieldNameIfValid(iName);
    if (c != null)
      throw new IllegalArgumentException("Invalid constraint name '" + iName + "'. Character '" + c + "' is invalid");

    if (constraintDefinition == null)
      throw new IllegalArgumentException("Constraint definition cannot be null");

    ODatabaseDocumentInternal database = getDatabase();
    OStorage storage = database.getStorage();

    final Locale locale = getServerLocale();
    iType = iType.toUpperCase(locale);
    if (algorithm == null)
      algorithm = OConstraints.chooseDefaultConstraintAlgorithm(iType);

    final String valueContainerAlgorithm = chooseContainerAlgorithm(iType);

    final OConstraintInternal<?> constraint;
    acquireExclusiveLock();
    try {

      if (constraints.containsKey(iName))
        throw new OConstraintException("Constraint with name " + iName + " already exists.");

      if (clusterIdsToConstraint == null || clusterIdsToConstraint.length == 0) {
        if (metadata == null)
          metadata = new ODocument().setTrackingChanges(false);

        final Object durable = metadata.field("durableInNonTxMode");
        if (!(durable instanceof Boolean))
          metadata.field("durableInNonTxMode", true);
        if (metadata.field("trackMode") == null)
          metadata.field("trackMode", "FUll");
      }

      constraint = OConstraints.createConstraint(getStorage(), iName, iType, algorithm, valueContainerAlgorithm, metadata, -1);
      if (progressListener == null) {
        // ASSIGN DEFAULT PROGRESS LISTENER
        progressListener = new OConstraintRebuildOutputListener(constraint);
      }

      final Set<String> clustersToConstraint = findClustersByIds(clusterIdsToConstraint, database);
      Object ignoreNullValues = metadata == null ? null : metadata.field("ignoreNullValues");
      if (Boolean.TRUE.equals(ignoreNullValues))
        constraintDefinition.setNullValuesIgnored(true);
      else if (Boolean.FALSE.equals(ignoreNullValues))
        constraintDefinition.setNullValuesIgnored(false);
      else
        constraintDefinition.setNullValuesIgnored(
            database.getConfiguration().getValueAsBoolean(OGlobalConfiguration.CONSTRAINT_IGNORE_NULL_VALUES_DEFAULT));

      // decide which cluster to use
      final String clusterName = constraintDefinition.getClassName() != null ? defaultClusterName : manualClusterName;

      constraint.create(iName, constraintDefinition, clusterName, clustersToConstraint, true, progressListener);

      addConstraintInternal(constraint);

      if (metadata != null) {
        final ODocument config = constraint.getConfiguration();
        config.field("metadata", metadata, OType.EMBEDDED);
      }

      setDirty();
      save();

    } finally {
      releaseExclusiveLock();
    }

    notifyInvolvedClasses(clusterIdsToConstraint);
    return preProcessBeforeReturn(database, constraint);
  }

  protected void notifyInvolvedClasses(int[] clusterIdsToConstraint) {
    if (clusterIdsToConstraint == null || clusterIdsToConstraint.length == 0)
      return;

    final ODatabaseDocumentInternal database = getDatabase();

    // UPDATE INVOLVED CLASSES
    final Set<String> classes = new HashSet<>();
    for (int clusterId : clusterIdsToConstraint) {
      final OClass cls = database.getMetadata().getSchema().getClassByClusterId(clusterId);
      if (cls != null && cls instanceof OClassImpl && !classes.contains(cls.getName())) {
        ((OClassImpl) cls).onPostConstraintManagement();
        classes.add(cls.getName());
      }
    }
  }

  private Set<String> findClustersByIds(int[] clusterIdsToConstraint, ODatabase database) {
    Set<String> clustersToConstraint = new HashSet<>();
    if (clusterIdsToConstraint != null) {
      for (int clusterId : clusterIdsToConstraint) {
        final String clusterNameToConstraint = database.getClusterNameById(clusterId);
        if (clusterNameToConstraint == null)
          throw new OConstraintException("Cluster with id " + clusterId + " does not exist.");
        clustersToConstraint.add(clusterNameToConstraint);
      }
    }
    return clustersToConstraint;
  }

  private String chooseContainerAlgorithm(String type) {
    final String valueConstainerAlgorithm;

    valueConstainerAlgorithm = ODefaultConstraintFactory.NONE_VALUE_CONTAINER;

    return valueConstainerAlgorithm;
  }

  public OConstraintManager dropConstraint(final String iConstraintName) {
    if (getDatabase().getTransaction().isActive())
      throw new IllegalStateException("Cannot drop an constraint inside a transaction");

    int[] clusterIdsToConstraint = null;

    acquireExclusiveLock();

    OConstraint<?> constraint = null;
    try {
      constraint = constraints.remove(iConstraintName);
      if (constraint != null) {
        final Set<String> clusters = constraint.getClusters();
        if (clusters != null && !clusters.isEmpty()) {
          final ODatabaseDocumentInternal db = getDatabase();
          clusterIdsToConstraint = new int[clusters.size()];
          int i = 0;
          for (String cl : clusters) {
            clusterIdsToConstraint[i++] = db.getClusterIdByName(cl);
          }
        }

        removeClassPropertyConstraint(constraint);

        constraint.delete();
        setDirty();
        save();

        notifyInvolvedClasses(clusterIdsToConstraint);
      }
    } catch (OException e) {
      constraints.put(iConstraintName, constraint);
      reload();
      throw e;
    } finally {
      releaseExclusiveLock();
    }

    return this;
  }

  /**
   * Binds POJO to ODocument.
   */
  @Override
  public ODocument toStream() {
    internalAcquireExclusiveLock();
    try {
      document.setInternalStatus(ORecordElement.STATUS.UNMARSHALLING);

      try {
        final OTrackedSet<ODocument> constraints = new OTrackedSet<>(document);

        for (final OConstraint<?> c : this.constraints.values()) {
          constraints.add(((OConstraintInternal<?>) c).updateConfiguration());
        }
        document.field(CONFIG_CONSTRAINTS, constraints, OType.EMBEDDEDSET);
      } finally {
        document.setInternalStatus(ORecordElement.STATUS.LOADED);
      }
      document.setDirty();

      return document;
    } finally {
      internalReleaseExclusiveLock();
    }
  }

  @Override
  protected void fromStream() {

    internalAcquireExclusiveLock();
    try {
      final Map<String, OConstraint<?>> oldConstraints = new HashMap<>(constraints);

      clearMetadata();
      final Collection<ODocument> constraintDocuments = document.field(CONFIG_CONSTRAINTS);

      if (constraintDocuments != null) {
        OConstraintInternal<?> constraint;
        boolean configUpdated = false;
        Iterator<ODocument> constraintConfigurationIterator = constraintDocuments.iterator();
        while (constraintConfigurationIterator.hasNext()) {
          final ODocument d = constraintConfigurationIterator.next();
          try {
            final int constraintVersion = d.field(OConstraintInternal.CONSTRAINT_VERSION) == null ? 1
                : (Integer) d.field(OConstraintInternal.CONSTRAINT_VERSION);

            final OConstraintMetadata newConstraintMetadata = OConstraintAbstract.loadMetadataInternal(d,
                d.field(OConstraintInternal.CONFIG_TYPE), d.field(OConstraintInternal.ALGORITHM),
                d.field(OConstraintInternal.VALUE_CONTAINER_ALGORITHM));

            constraint = OConstraints.createConstraint(getStorage(), newConstraintMetadata.getName(),
                newConstraintMetadata.getType(), newConstraintMetadata.getAlgorithm(),
                newConstraintMetadata.getValueContainerAlgorithm(), d.field(OConstraintInternal.METADATA), constraintVersion);

            final String normalizedName = newConstraintMetadata.getName();

            OConstraint<?> oldConstraint = oldConstraints.remove(normalizedName);
            if (oldConstraint != null) {
              OConstraintMetadata oldConstraintMetadata = oldConstraint.getInternal()
                  .loadMetadata(oldConstraint.getConfiguration());

              if (!(oldConstraintMetadata.equals(newConstraintMetadata)
                  || newConstraintMetadata.getConstraintDefinition() == null)) {
                oldConstraint.delete();
              }

              if (constraint.loadFromConfiguration(d)) {
                addConstraintInternal(constraint);
              } else {
                constraintConfigurationIterator.remove();
                configUpdated = true;
              }
            } else {
              if (constraint.loadFromConfiguration(d)) {
                addConstraintInternal(constraint);
              } else {
                constraintConfigurationIterator.remove();
                configUpdated = true;
              }
            }
          } catch (RuntimeException e) {
            constraintConfigurationIterator.remove();
            configUpdated = true;
            OLogManager.instance().error(this, "Error on loading constraint by configuration: %s", e, d);
          }
        }

        for (OConstraint<?> oldConstraint : oldConstraints.values())
          try {
            OLogManager.instance().warn(this, "Constraint '%s' was not found after reload and will be removed",
                oldConstraint.getName());

            oldConstraint.delete();
          } catch (Exception e) {
            OLogManager.instance().error(this, "Error on deletion of constraint '%s'", e, oldConstraint.getName());
          }

        if (configUpdated) {
          document.field(CONFIG_CONSTRAINTS, constraintDocuments);
          save();
        }
      }
    } finally {
      internalReleaseExclusiveLock();
    }
  }

  @Override
  public void removeClassPropertyConstraint(OConstraint<?> constr) {
    acquireExclusiveLock();
    try {
      final OConstraintDefinition constraintDefinition = constr.getDefinition();
      if (constraintDefinition == null || constraintDefinition.getClassName() == null)
        return;

      final Locale locale = getServerLocale();
      Map<OMultiKey, Set<OConstraint<?>>> map = classPropertyConstraint
          .get(constraintDefinition.getClassName().toLowerCase(locale));

      if (map == null) {
        return;
      }

      map = new HashMap<>(map);

      final int paramCount = constraintDefinition.getParamCount();

      for (int i = 1; i <= paramCount; i++) {
        final List<String> fields = normalizeFieldNames(constraintDefinition.getFields().subList(0, i));
        final OMultiKey multiKey = new OMultiKey(fields);

        Set<OConstraint<?>> constraintSet = map.get(multiKey);
        if (constraintSet == null)
          continue;

        constraintSet = new HashSet<>(constraintSet);
        constraintSet.remove(constr);

        if (constraintSet.isEmpty()) {
          map.remove(multiKey);
        } else {
          map.put(multiKey, constraintSet);
        }
      }

      if (map.isEmpty())
        classPropertyConstraint.remove(constraintDefinition.getClassName().toLowerCase(locale));
      else
        classPropertyConstraint.put(constraintDefinition.getClassName().toLowerCase(locale), copyPropertyMap(map));

    } finally {
      releaseExclusiveLock();
    }
  }

  public ODocument toNetworkStream() {
    ODocument document = new ODocument();
    internalAcquireExclusiveLock();
    try {
      document.setInternalStatus(ORecordElement.STATUS.UNMARSHALLING);

      try {
        final OTrackedSet<ODocument> constraints = new OTrackedSet<>(document);

        for (final OConstraint<?> c : this.constraints.values()) {
          constraints.add(((OConstraintInternal<?>) c).updateConfiguration().copy());
        }
        document.field(CONFIG_CONSTRAINTS, constraints, OType.EMBEDDEDSET);

      } finally {
        document.setInternalStatus(ORecordElement.STATUS.LOADED);
      }
      document.setDirty();

      return document;
    } finally {
      internalReleaseExclusiveLock();
    }
  }

  @Override
  protected OStorage getStorage() {
    return storage;
  }

  @SuppressWarnings("unchecked")
  @Override
  public OConstraint<?> preProcessBeforeReturn(ODatabaseDocumentInternal database, final OConstraint<?> constraint) {
    if(constraint instanceof OConstraintOneValue){
      return new OConstraintTxAwareOneValue(database, (OConstraint<OIdentifiable>) constraint);
    }
    
    System.out.println("\nMETODO preProcessBeforeReturn EM OConstraintManagerShared APENAS RETORNOU O PARAMETRO RECEBIDO");
    return constraint;
  }
}
