package com.orientechnologies.orient.core.constraint;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.orientechnologies.orient.core.db.ODatabaseDocumentInternal;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.index.OAlwaysGreaterKey;
import com.orientechnologies.orient.core.index.OAlwaysLessKey;
import com.orientechnologies.orient.core.record.ORecord;
import com.orientechnologies.orient.core.record.impl.ODocumentEntry;
import com.orientechnologies.orient.core.tx.OTransactionConstraintChanges;
import com.orientechnologies.orient.core.tx.OTransactionConstraintChanges.OPERATION;
import com.orientechnologies.orient.core.tx.OTransactionConstraintChangesPerKey;
import com.orientechnologies.orient.core.tx.OTransactionConstraintChangesPerKey.OTransactionConstraintEntry;

/**
 * 
 * @author fabio
 *
 * @param <T>
 */
public abstract class OConstraintTxAware<T> extends OConstraintAbstractDelegate<T> {
  //XXX : Not implemented in comparison to @OIndexTxAware - Fabio
  //getFirstKey()
  //gelastKey()
  //enhanceCompositeKey()
  //enhanceToCompositeKeyBetweenAsc()
  //enhanceFromCompositeKeyBetweenAsc()
  //enhanceToCompositeKeyBetweenDesc()
  //enhanceFromCompositeKeyBetweenDesc()
  //getCollatingValue()

  // from package index
  private static final OAlwaysLessKey    ALWAYS_LESS_KEY    = new OAlwaysLessKey();
  private static final OAlwaysGreaterKey ALWAYS_GREATER_KEY = new OAlwaysGreaterKey();

  protected ODatabaseDocumentInternal database;

  
  public OConstraintTxAware(final ODatabaseDocumentInternal iDatabase, OConstraint<T> delegate) {
    super(delegate);
    this.database = iDatabase;
  }

  /**
   * Indicates search behavior in case of {@link com.orientechnologies.orient.core.index.OCompositeKey} keys that have less amount
   * of internal keys are used, whether lowest or highest partially matched key should be used. Such keys is allowed to use only in
   */
  public static enum PartialSearchMode {
    /**
     * Any partially matched key will be used as search result.
     */
    NONE, /**
     * The biggest partially matched key will be used as search result.
     */
    HIGHEST_BOUNDARY,

    /**
     * The smallest partially matched key will be used as search result.
     */
    LOWEST_BOUNDARY
  }
  
  @Override
  public long getSize() {
    long tot = delegate.getSize();

    final OTransactionConstraintChanges constraintChanges = database.getMicroOrRegularTransaction().getConstraintChanges(delegate.getName());
    if (constraintChanges != null) {
      if (constraintChanges.cleared)
        // BEGIN FROM 0
        tot = 0;

      for (final Entry<Object, OTransactionConstraintChangesPerKey> entry : constraintChanges.changesPerKey.entrySet()) {
        for (final OTransactionConstraintEntry e : entry.getValue().entries) {
          if (e.operation == OPERATION.REMOVE) {
            if (e.value == null)
              // KEY REMOVED
              tot--;
          }
        }
      }

      for (final OTransactionConstraintEntry e : constraintChanges.nullKeyChanges.entries) {
        if (e.operation == OPERATION.REMOVE) {
          if (e.value == null)
            // KEY REMOVED
            tot--;
        }
      }
    }

    return tot;
  }
  
  @Override
  public OConstraint<T> put(Object iKey, OIdentifiable iValue, Map<String, List<OConstraintDto>> fields) {
    checkForKeyType(iKey);
    final ORID rid = iValue.getIdentity();

    if (!rid.isValid())
      if (iValue instanceof ORecord)
        // EARLY SAVE IT
        ((ORecord) iValue).save();
      else
        throw new IllegalArgumentException("Cannot store non persistent RID as constraint value for key '" + iKey + "'");

    iKey = getCollatingValue(iKey);

    database.getMicroOrRegularTransaction().addConstraintEntry(delegate, super.getName(), OPERATION.PUT, iKey, iValue, fields);
    return this;
  }
  
  protected Object getCollatingValue(final Object key) {
    final OConstraintDefinition definition = getDefinition();
    if (key != null && definition != null)
      return definition.getCollate().transform(key);
    return key;
  }
  
  @Override
  public boolean remove(Object key) {
    key = getCollatingValue(key);
    database.getMicroOrRegularTransaction().addConstraintEntry(delegate, super.getName(), OPERATION.REMOVE, key, null, null);
    return true;
  }
  
  @Override
  public boolean remove(Object iKey, OIdentifiable iRID) {
    iKey = getCollatingValue(iKey);
    database.getMicroOrRegularTransaction().addConstraintEntry(delegate, super.getName(), OPERATION.REMOVE, iKey, iRID, null);
    return true;
  }
  
  @Override
  public OConstraint<T> clear() {
    database.getMicroOrRegularTransaction().addConstraintEntry(delegate, super.getName(), OPERATION.CLEAR, null, null, null);
    return this;
  }
  
}
