package com.orientechnologies.orient.core.constraint;

import java.util.List;
import java.util.Map;

import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ODocumentEntry;
import com.orientechnologies.orient.core.tx.OTransactionConstraintChanges;

/**
 * 
 * @author fabio
 *
 * @param <T>
 */
public interface OConstraintInternal<T> extends OConstraint<T> {

  String CONFIG_KEYTYPE              = "keyType";
  String CONFIG_AUTOMATIC            = "automatic";
  String CONFIG_TYPE                 = "type";
  String ALGORITHM                   = "algorithm";
  String VALUE_CONTAINER_ALGORITHM   = "valueContainerAlgorithm";
  String CONFIG_NAME                 = "name";
  String CONSTRAINT_DEFINITION       = "constraintDefinition";
  String CONSTRAINT_DEFINITION_CLASS = "constraintDefinitionClass";
  String CONSTRAINT_VERSION          = "constraintVersion";
  String METADATA                    = "metadata";

  Object getCollatingValue(final Object key);

  /**
   * Saves the constraint configuration to disk.
   *
   * @return The configuration as ODocument instance
   *
   * @see #getConfiguration()
   */
  ODocument updateConfiguration();

  OConstraintMetadata loadMetadata(ODocument iConfig);

  /**
   * Loads the constraint giving the configuration.
   *
   * @param iConfig
   *          ODocument instance containing the configuration
   */
  boolean loadFromConfiguration(ODocument iConfig);
  
  /**
   * <p>
   * Returns the constraint name for a key. The name is always the current constraint name, but in cases where the constraint supports key-based
   * sharding.
   *
   * @param key the constraint key.
   *
   * @return The constraint name involved
   */
  String getConstraintNameByKey(Object key);

  /**
   * <p>
   * Acquires exclusive lock in the active atomic operation running on the current thread for this constraint.
   *
   * <p>
   * If this constraint supports a more narrow locking, for example key-based sharding, it may use the provided {@code key} to infer a
   * more narrow lock scope, but that is not a requirement.
   *
   * @param key the constraint key to lock.
   *
   * @return {@code true} if this constraint was locked entirely, {@code false} if this constraint locking is sensitive to the provided {@code
   * key} and only some subset of this constraint was locked.
   */
  boolean acquireAtomicExclusiveLock(Object key);
  
  void preCommit(OConstraintAbstract.ConstraintTxSnapshot snapshots);

  void addTxOperation(OConstraintAbstract.ConstraintTxSnapshot snapshots, final OTransactionConstraintChanges changes, Map<String, List<OConstraintDto>> fields);

  void commit(OConstraintAbstract.ConstraintTxSnapshot snapshots);

  void postCommit(OConstraintAbstract.ConstraintTxSnapshot snapshots);
  
  void setType(OType type);
}
