package com.orientechnologies.orient.core.constraint;

import java.util.Set;

import com.orientechnologies.common.listener.OProgressListener;
import com.orientechnologies.common.serialization.types.OBinarySerializer;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.exception.OInvalidConstraintEngineIdException;
import com.orientechnologies.orient.core.exception.OInvalidIndexEngineIdException;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.serialization.serializer.stream.OStreamSerializerRID;
import com.orientechnologies.orient.core.storage.OStorage;

/**
 * 
 * @author fabio
 *
 */
public abstract class OConstraintOneValue extends OConstraintAbstract<OIdentifiable> {

  public OConstraintOneValue(String name, String type, String algorithm, String valueContainerAlgorithm, ODocument metadata,
      int version, OStorage storage) {
    super(name, type, algorithm, valueContainerAlgorithm, metadata, version, storage);
  }

  @Override
  public OIdentifiable get(Object iKey) {
    iKey = getCollatingValue(iKey);

    acquireSharedLock();
    try {
      while (true)
        try {
          return (OIdentifiable) storage.getConstraintValue(constraintId, iKey);
        } catch (OInvalidConstraintEngineIdException ignore) {
          doReloadConstraintEngine();
        }
    } finally {
      releaseSharedLock();
    }
  }

  public long count(Object iKey) {
    iKey = getCollatingValue(iKey);

    acquireSharedLock();
    try {
      while (true)
        try {
          return storage.constraintContainsKey(constraintId, iKey) ? 1 : 0;
        } catch (OInvalidConstraintEngineIdException ignore) {
          doReloadConstraintEngine();
        }
    } finally {
      releaseSharedLock();
    }
  }

  @Override
  public OConstraintOneValue create(final String name, final OConstraintDefinition constraintDefinition,
      final String clusterConstraintName, final Set<String> clusterToConstraint, boolean rebuild,
      final OProgressListener progressListener) {
    return (OConstraintOneValue) super.create(constraintDefinition, clusterConstraintName, clusterToConstraint, rebuild,
        progressListener, determineValueSerializer());
  }

  // TODO: maybe
  // public OConstraintCursor iterateEntries()
  // public OConstraintCursor iterateEntriesBetween()
  // public OConstraintCursor iterateEntriesMajor()
  // public OConstraintCursor iterateEntriesMinor()

  @Override
  public long getSize() {
    acquireSharedLock();
    try {
      while (true) {
        try {
          return storage.getConstraintSize(constraintId, null);
        } catch (OInvalidConstraintEngineIdException ignore) {
          doReloadConstraintEngine();
        }
      }
    } finally {
      releaseSharedLock();
    }
  }

  public long getKeySize() {
    acquireSharedLock();
    try {
      while (true) {
        try {
          return storage.getConstraintSize(constraintId, null);
        } catch (OInvalidConstraintEngineIdException ignore) {
          doReloadConstraintEngine();
        }
      }
    } finally {
      releaseSharedLock();
    }
  }
  
  // TODO: maybe
  // cursor()
  // descCursor()

  @Override
  public boolean isUnique() {
    return true;
  }

  @Override
  protected OBinarySerializer determineValueSerializer() {
    return OStreamSerializerRID.INSTANCE;
  }

}
