package com.orientechnologies.orient.core.exception;

/**
 * 
 * @author fabio
 *
 */
public class OInvalidConstraintEngineIdException extends Exception {

  public OInvalidConstraintEngineIdException(String message) {
    super(message);
  }
}
