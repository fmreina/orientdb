package com.orientechnologies.orient.core.storage.ridbag.sbtree;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import com.orientechnologies.common.exception.OException;
import com.orientechnologies.orient.core.config.OGlobalConfiguration;
import com.orientechnologies.orient.core.db.ODatabaseDocumentInternal;
import com.orientechnologies.orient.core.db.ODatabaseRecordThreadLocal;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.index.OIndexEngineException;
import com.orientechnologies.orient.core.storage.cache.OReadCache;
import com.orientechnologies.orient.core.storage.cache.OWriteCache;
import com.orientechnologies.orient.core.storage.impl.local.OAbstractPaginatedStorage;
import com.orientechnologies.orient.core.storage.impl.local.paginated.atomicoperations.OAtomicOperation;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintRIDContainer implements Set<OIdentifiable> {

  public static final String CONSTRAINT_FILE_EXTENSION = ".ctr";

  private final long         fileId;
  private Set<OIdentifiable> underlying;
  private boolean            isEmbedded;
  private int                topThreshold              = OGlobalConfiguration.CONSTRAINT_EMBEDDED_TO_SBTREEBONSAI_THRESHOLD
      .getValueAsInteger();
  private int                bottomThreshold           = OGlobalConfiguration.CONSTRAINT_SBTREEBONSAI_TO_EMBEDDED_THRESHOLD
      .getValueAsInteger();
  private final boolean      durableNonTxMode;

  public OConstraintRIDContainer(String name, boolean durableNonTxMode, AtomicLong bonsayFileId) {
    long gotFileId = bonsayFileId.get();
    if (gotFileId == 0) {
      gotFileId = resolveFileIdByName(name + CONSTRAINT_FILE_EXTENSION);
      bonsayFileId.set(gotFileId);
    }
    this.fileId = gotFileId;

    underlying = new HashSet<>();
    isEmbedded = true;
    this.durableNonTxMode = durableNonTxMode;
  }

  public OConstraintRIDContainer(long fileId, Set<OIdentifiable> underlying, boolean durableNonTxMode) {
    this.fileId = fileId;
    this.underlying = underlying;
    isEmbedded = !(underlying instanceof OConstraintRIDContainerSBTree);
    this.durableNonTxMode = durableNonTxMode;
  }

  public void setTopThreshold(int topThreshold) {
    this.topThreshold = topThreshold;
  }

  public void setBottomThreshold(int bottomThreshold) {
    this.bottomThreshold = bottomThreshold;
  }

  private long resolveFileIdByName(String fileName) {
    final OAbstractPaginatedStorage storage = (OAbstractPaginatedStorage) ODatabaseRecordThreadLocal.instance().get().getStorage()
        .getUnderlying();
    final OAtomicOperation atomicOperation;
    try {
      atomicOperation = storage.getAtomicOperationsManager().startAtomicOperation(fileName, true);
    } catch (IOException e) {
      throw OException.wrapException(new OIndexEngineException("Error creation of sbtree with name " + fileName, fileName), e);
    }

    try {
      final OReadCache readCache = storage.getReadCache();
      final OWriteCache writeCache = storage.getWriteCache();

      if (atomicOperation == null) {
        if (writeCache.exists(fileName))
          return writeCache.fileIdByName(fileName);

        return readCache.addFile(fileName, writeCache);
      } else {
        long fileId;

        if (atomicOperation.isFileExists(fileName))
          fileId = atomicOperation.loadFile(fileName);
        else
          fileId = atomicOperation.addFile(fileName);

        storage.getAtomicOperationsManager().endAtomicOperation(false, null);
        return fileId;
      }
    } catch (IOException e) {
      try {
        storage.getAtomicOperationsManager().endAtomicOperation(true, e);
      } catch (IOException ioe) {
        throw OException.wrapException(new OIndexEngineException("Error of rollback of atomic operation", fileName), ioe);
      }

      throw OException.wrapException(new OIndexEngineException("Error creation of sbtree with name " + fileName, fileName), e);
    }
  }

  public long getFileId() {
    return fileId;
  }

  @Override
  public int size() {
    return underlying.size();
  }

  @Override
  public boolean isEmpty() {
    return underlying.isEmpty();
  }

  @Override
  public boolean contains(Object o) {
    return underlying.contains(o);
  }

  @Override
  public Iterator<OIdentifiable> iterator() {
    return underlying.iterator();
  }

  @Override
  public Object[] toArray() {
    return underlying.toArray();
  }

  @SuppressWarnings("SuspiciousToArrayCall")
  @Override
  public <T> T[] toArray(T[] a) {
    return underlying.toArray(a);
  }

  @Override
  public boolean add(OIdentifiable oIdentifiable) {
    final boolean res = underlying.add(oIdentifiable);
    checkTopThreshold();
    return res;
  }

  @Override
  public boolean remove(Object o) {
    final boolean res = underlying.remove(o);
    checkBottomThreshold();
    return res;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    return underlying.containsAll(c);
  }

  @Override
  public boolean addAll(Collection<? extends OIdentifiable> c) {
    final boolean res = underlying.addAll(c);
    checkTopThreshold();
    return res;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    return underlying.retainAll(c);
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    final boolean res = underlying.removeAll(c);
    checkBottomThreshold();
    return res;
  }

  @Override
  public void clear() {
    if (isEmbedded)
      underlying.clear();
    else {
      final OConstraintRIDContainerSBTree tree = (OConstraintRIDContainerSBTree) underlying;
      tree.delete();
      underlying = new HashSet<>();
      isEmbedded = true;
    }
  }

  public boolean isEmbedded() {
    return isEmbedded;
  }

  public boolean isDurableNonTxMode() {
    return durableNonTxMode;
  }

  public Set<OIdentifiable> getUnderlying() {
    return underlying;
  }

  private void checkTopThreshold() {
    if (isEmbedded && topThreshold < underlying.size())
      convertToSbTree();
  }

  private void checkBottomThreshold() {
    if (!isEmbedded && bottomThreshold > underlying.size())
      convertToEmbedded();
  }

  private void convertToEmbedded() {
    final OConstraintRIDContainerSBTree tree = (OConstraintRIDContainerSBTree) underlying;

    final Set<OIdentifiable> set = new HashSet<>(tree);

    tree.delete();
    underlying = set;
    isEmbedded = true;
  }

  /**
   * If set is embedded convert it not embedded representation.
   */
  public void checkNotEmbedded() {
    if (isEmbedded)
      convertToSbTree();
  }

  private void convertToSbTree() {
    final ODatabaseDocumentInternal db = ODatabaseRecordThreadLocal.instance().get();
    final OConstraintRIDContainerSBTree tree = new OConstraintRIDContainerSBTree(fileId,
        (OAbstractPaginatedStorage) db.getStorage().getUnderlying());

    tree.addAll(underlying);

    underlying = tree;
    isEmbedded = false;
  }
}
