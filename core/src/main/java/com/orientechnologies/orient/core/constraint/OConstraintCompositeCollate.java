package com.orientechnologies.orient.core.constraint;

import java.util.ArrayList;
import java.util.List;

import com.orientechnologies.orient.core.collate.OCollate;
import com.orientechnologies.orient.core.index.OCompositeKey;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintCompositeCollate implements OCollate {

  private static final long                   serialVersionUID = 6183707545533959699L;
  private final OAbstractConstraintDefinition oCompositeConstraintDefinition;

  public OConstraintCompositeCollate(final OAbstractConstraintDefinition oCompositeConstraintDefinition) {
    this.oCompositeConstraintDefinition = oCompositeConstraintDefinition;
  }

  private final List<OCollate> collates = new ArrayList<OCollate>();

  public void addCollate(OCollate collate) {
    collates.add(collate);
  }

  @Override
  public String getName() {
    throw new UnsupportedOperationException("getName");
  }

  @SuppressWarnings("unchecked")
  @Override
  public Object transform(final Object obj) {
    final List<Object> keys;
    if (obj instanceof OCompositeKey) {
      final OCompositeKey compositeKey = (OCompositeKey) obj;
      keys = compositeKey.getKeys();
    } else if (obj instanceof List) {
      keys = (List<Object>) obj;
    } else {
      throw new OConstraintException("Impossible add as key of a CompositeConstraint a value of type " + obj.getClass());
    }

    final OCompositeKey transformedKey = new OCompositeKey();

    final int size = Math.min(keys.size(), collates.size());
    for (int i = 0; i < size; i++) {
      final Object key = keys.get(i);

      final OCollate collate = collates.get(i);
      transformedKey.addKey(collate.transform(key));
    }

    for (int i = size; i < keys.size(); i++)
      transformedKey.addKey(keys.get(i));

    return transformedKey;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    final OConstraintCompositeCollate that = (OConstraintCompositeCollate) o;

    if (!collates.equals(that.collates))
      return false;

    return true;
  }

  @Override
  public int hashCode() {
    return collates.hashCode();
  }

  public List<OCollate> getCollates() {
    return collates;
  }

  @Override
  public String toString() {
    return "OCompositeCollate{" + "collates=" + collates + ", null values ignored = "
        + this.oCompositeConstraintDefinition.isNullValuesIgnored() + '}';
  }
}
