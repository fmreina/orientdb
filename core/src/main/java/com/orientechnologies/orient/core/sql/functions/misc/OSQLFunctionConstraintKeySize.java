package com.orientechnologies.orient.core.sql.functions.misc;

import com.orientechnologies.orient.core.command.OCommandContext;
import com.orientechnologies.orient.core.constraint.OConstraint;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.sql.functions.OSQLFunctionAbstract;

/**
 * returns the number of keys for a constraint
 *
 * @author fabio
 */
public class OSQLFunctionConstraintKeySize extends OSQLFunctionAbstract {
  public static final String NAME = "constraintKeySize";

  public OSQLFunctionConstraintKeySize() {
    super(NAME, 1, 1);
  }

  public Object execute(Object iThis, final OIdentifiable iCurrentRecord, Object iCurrentResult, final Object[] iParams,
      OCommandContext iContext) {
    final Object value = iParams[0];

    String constraintName = String.valueOf(value);
    OConstraint<?> constraint = iContext.getDatabase().getMetadata().getConstraintManager().getConstraint(constraintName);
    if (constraint == null) {
      return null;
    }
    return constraint.getKeySize();
  }

  public String getSyntax() {
    return "constraintKeySize(<constraintName-string>)";
  }
}
