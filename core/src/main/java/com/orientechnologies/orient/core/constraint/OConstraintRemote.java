package com.orientechnologies.orient.core.constraint;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.orientechnologies.common.listener.OProgressListener;
import com.orientechnologies.orient.core.db.ODatabaseDocumentInternal;
import com.orientechnologies.orient.core.db.ODatabaseRecordThreadLocal;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.index.OIndexException;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.ORecord;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ODocumentEntry;
import com.orientechnologies.orient.core.sql.executor.OResultSet;

/**
 * 
 * @author fabio
 *
 * @param <T>
 */
@SuppressWarnings("unchecked")
public abstract class OConstraintRemote<T> implements OConstraint<T> {

  // TODO - fabio: completar com demais comandos suportados
  protected final static String   QUERY_ENTRIES  = "select key, rid from constraint:`%s`";

  private final static String     QUERY_PUT      = "insert into constraint:`%s` (key,rid) values (?,?)";
  private final static String     QUERY_SIZE     = "select count(*) as size from constraint:`%s`";
  private final static String     QUERY_DROP     = "drop constraint %s";
  private final static String     QUERY_KEY_SIZE = "select constraintKeySize('%s') as size";
  private final static String     QUERY_REMOVE   = "delete from constraint:`%s` where key = ?";
  private final static String     QUERY_REMOVE2  = "delete from constraint:`%s` where key = ? and rid = ?";
  private final static String     QUERY_CLEAR    = "delete from constraint:`%s`";

  protected final String          databaseName;
  private final String            wrappedType;
  private final String            algorithm;
  private final ORID              rid;
  protected OConstraintDefinition constraintDefinition;
  protected String                name;
  protected ODocument             configuration;
  protected Set<String>           clustersToConstraint;

  public OConstraintRemote(final String iName, final String iWrappedType, final String algorithm, final ORID iRid,
      final OConstraintDefinition iConstraintDefinition, final ODocument iConfiguration, final Set<String> clustersToConstraint,
      String databaseName) {
    this.name = iName;
    this.wrappedType = iWrappedType;
    this.algorithm = algorithm;
    this.rid = iRid;
    this.constraintDefinition = iConstraintDefinition;
    this.configuration = iConfiguration;
    this.clustersToConstraint = new HashSet<String>(clustersToConstraint);
    this.databaseName = databaseName;
  }

  @Override
  public OConstraint<T> create(final String name, final OConstraintDefinition constraintDefinition,
      final String clusterConstraintName, final Set<String> clusterToConstraint, boolean rebuild,
      final OProgressListener progressListener) {
    this.name = name;
    return this;
  }

  @Override
  public OConstraint<T> delete() {
    getDatabase().constraintQuery(getName(), String.format(QUERY_DROP, name));
    return this;
  }

  @Override
  public String getDatabaseName() {
    return databaseName;
  }

  @Override
  public long getSize() {
    try (OResultSet result = getDatabase().constraintQuery(getName(), String.format(QUERY_SIZE, name));) {
      if (result.hasNext())
        return (Long) result.next().getProperty("size");
    }
    return 0;
  }

  @Override
  public boolean isUnique() {
    return false;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getType() {
    return wrappedType;
  }

  public long getKeySize() {
    try (OResultSet result = getDatabase().constraintQuery(getName(), String.format(QUERY_KEY_SIZE, name))) {
      if (result.hasNext())
        return (Long) result.next().getProperty("size");
    }
    return 0;
  }
 
  @Override
  public boolean isAutomatic() {
    return constraintDefinition != null && constraintDefinition.getClassName() != null;
  }

  @Override
  public ODocument getConfiguration() {
    return configuration;
  }

  @Override
  public OConstraintInternal<T> getInternal() {
    return null;
  }

  @Override
  public ODocument getMetadata() {
    return configuration.field("metadata", OType.EMBEDDED);
  }

  @Override
  public OConstraintDefinition getDefinition() {
    return constraintDefinition;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    final OConstraintRemote<?> that = (OConstraintRemote<?>) o;

    return name.equals(that.name);
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  public Set<String> getClusters() {
    return Collections.unmodifiableSet(clustersToConstraint);
  }

  @Override
  public int getConstraintId() {
    throw new UnsupportedOperationException("getConstraintId");
  }

  @Override
  public int compareTo(OConstraint<T> constraint) {
    final String name = constraint.getName();
    return this.getName().compareTo(name);
  }

  public ODatabaseDocumentInternal getDatabase() {
    return ODatabaseRecordThreadLocal.instance().get();
  }

  @Override
  public OConstraint<T> put(Object iKey, OIdentifiable iValue, Map<String, List<OConstraintDto>> fields) {
    //XXX - FABIO: recebe os fields
    if (iValue instanceof ORecord && !iValue.getIdentity().isValid())
      // SAVE IT BEFORE TO PUT
      ((ORecord) iValue).save();

    if (iValue.getIdentity().isNew())
      throw new OConstraintException(
          "Cannot insert values in manual constraint against remote protocol during a transaction. Temporary RID cannot be managed at server side");

    getDatabase().command(String.format(QUERY_PUT, name), iKey, iValue.getIdentity()).close();
    return this;
  }

  @Override
  public boolean remove(final Object key) {
    try (OResultSet result = getDatabase().command(String.format(QUERY_REMOVE, name), key)) {
      if (!result.hasNext()) {
        return false;
      }
      return ((long) result.next().getProperty("count")) > 0;
    }
  }

  @Override
  public boolean remove(final Object iKey, final OIdentifiable iRID) {
    final long deleted;
    if (iRID != null) {

      if (iRID.getIdentity().isNew())
        throw new OIndexException(
            "Cannot remove values in manual indexes against remote protocol during a transaction. Temporary RID cannot be managed at server side");

      OResultSet result = getDatabase().command(String.format(QUERY_REMOVE2, name), iKey, iRID);
      if (!result.hasNext()) {
        deleted = 0;
      } else
        deleted = result.next().getProperty("count");
      result.close();
    } else {
      OResultSet result = getDatabase().command(String.format(QUERY_REMOVE, name), iKey);
      if (!result.hasNext()) {
        deleted = 0;
      } else
        deleted = result.next().getProperty("count");
      result.close();
    }
    return deleted > 0;
  }

  public OConstraintRemote<T> clear() {
    getDatabase().command(String.format(QUERY_CLEAR, name)).close();
    return this;
  }
}
