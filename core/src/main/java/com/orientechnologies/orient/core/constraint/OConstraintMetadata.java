package com.orientechnologies.orient.core.constraint;

import java.util.Set;

/**
 * Constains the constraint metadata
 * 
 * @author fabio
 *
 */
public class OConstraintMetadata {

  private final String                name;
  private final OConstraintDefinition constraintDefinition;
  private final Set<String>           clustersToConstraint;
  private final String                type;
  private final String                algorithm;
  private final String                valueContainerAlgorithm;

  public OConstraintMetadata(String name, OConstraintDefinition constraintDefinition, Set<String> clustersToConstraint, String type,
      String algorithm, String valueContainerAlgorithm) {
    this.name = name;
    this.constraintDefinition = constraintDefinition;
    this.clustersToConstraint = clustersToConstraint;
    this.type = type;
    this.algorithm = algorithm;
    this.valueContainerAlgorithm = valueContainerAlgorithm;
  }

  public String getName() {
    return name;
  }

  public OConstraintDefinition getConstraintDefinition() {
    return constraintDefinition;
  }

  public Set<String> getClustersToConstraint() {
    return clustersToConstraint;
  }

  public String getType() {
    return type;
  }

  public String getAlgorithm() {
    return algorithm;
  }

  public String getValueContainerAlgorithm() {
    return valueContainerAlgorithm;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    final OConstraintMetadata that = (OConstraintMetadata) o;

    if (algorithm != null ? !algorithm.equals(that.algorithm) : that.algorithm != null)
      return false;
    if (!clustersToConstraint.equals(that.clustersToConstraint))
      return false;
    if (constraintDefinition != null ? !constraintDefinition.equals(that.constraintDefinition) : that.constraintDefinition != null)
      return false;
    if (!name.equals(that.name))
      return false;
    if (!type.equals(that.type))
      return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = name.hashCode();
    result = 31 * result + (constraintDefinition != null ? constraintDefinition.hashCode() : 0);
    result = 31 * result + clustersToConstraint.hashCode();
    result = 31 * result + type.hashCode();
    result = 31 * result + (algorithm != null ? algorithm.hashCode() : 0);
    return result;
  }
}