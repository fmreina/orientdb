package com.orientechnologies.orient.core.constraint;

import java.io.Serializable;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintUniqueParameters implements OConstraintParameters, Serializable {

  private static final long serialVersionUID = -8510537178146339047L;

  public OConstraintUniqueParameters() {
  }

  @Override
  public OConstraintParameters strToParam(String str) {
    return this;
  }

  @Override
  public String toString() {
    return "Unique Constraint";
  }

}
