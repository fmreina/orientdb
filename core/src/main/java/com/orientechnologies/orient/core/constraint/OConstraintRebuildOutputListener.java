package com.orientechnologies.orient.core.constraint;

import com.orientechnologies.common.listener.OProgressListener;
import com.orientechnologies.common.log.OLogManager;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintRebuildOutputListener implements OProgressListener {

  long                      startTime;
  long                      lastDump;
  long                      lastCounter = 0;
  boolean                   rebuild     = false;

  private final OConstraint constraint;

  public OConstraintRebuildOutputListener(OConstraint constraint) {
    this.constraint = constraint;
  }

  @Override
  public void onBegin(Object iTask, long iTotal, Object iRebuild) {
    startTime = System.currentTimeMillis();
    lastDump = startTime;

    rebuild = (Boolean) iRebuild;
    if (iTotal > 0)
      if (rebuild)
        OLogManager.instance().info(this, "- Rebuilding constraint %s.%s (estimated %,d items)...", constraint.getDatabaseName(),
            constraint.getName(), iTotal);
      else
        OLogManager.instance().debug(this, "- Building constraint %s.%s (estimated %,d items)...", constraint.getDatabaseName(),
            constraint.getName(), iTotal);
  }

  @Override
  public boolean onProgress(Object iTask, long iCounter, float iPercent) {
    final long now = System.currentTimeMillis();
    if (now - lastDump > 10000) {
      if (rebuild)
        OLogManager.instance().info(this, "--> %3.2f%% progress, %,d constrained so far (%,d items/sec)", iPercent, iCounter,
            ((iCounter - lastCounter) / 10));
      else
        OLogManager.instance().info(this, "--> %3.2f%% progress, %,d constrained so far (%,d items/sec)", iPercent, iCounter,
            ((iCounter - lastCounter) / 10));
      lastDump = now;
      lastCounter = iCounter;
    }
    return true;
  }

  @Override
  public void onCompletition(Object iTask, boolean iSucceed) {
    final long constraintSize = constraint.getSize(); // TODO - fabio: Verificar se faz sentido

    if (constraintSize > 0)
      if (rebuild)
        OLogManager.instance().info(this, "--> OK, constrained %,d items in %,d ms", constraintSize,
            (System.currentTimeMillis() - startTime));
      else
        OLogManager.instance().debug(this, "--> OK, constrianed %,d items in %,d ms", constraintSize,
            (System.currentTimeMillis() - startTime));
  }

}
