package com.orientechnologies.orient.core.tx;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.tx.OTransactionConstraintChanges.OPERATION;

/**
 * Collects the changes to a constraint for a certain key
 *
 * @author fabio
 */
public class OTransactionConstraintChangesPerKey {

  /* internal */ static final int SET_ADD_THRESHOLD = 8;

  public final Object                       key;
  public final List<OTransactionConstraintEntry> entries;

  public boolean clientTrackOnly;
  
  public static class OTransactionConstraintEntry {
    public OPERATION     operation;
    public OIdentifiable value;

    public OTransactionConstraintEntry(final OIdentifiable iValue, final OPERATION iOperation) {
      value = iValue;
      operation = iOperation;
    }

    @Override
    public boolean equals(Object obj) {
      // equality intentionally established by the value only, operation is ignored, see interpretAs* methods for details

      if (this == obj)
        return true;

      if (obj == null || obj.getClass() != OTransactionConstraintEntry.class)
        return false;
      final OTransactionConstraintEntry other = (OTransactionConstraintEntry) obj;

      if (this.value != null)
        return this.value.equals(other.value);

      return other.value == null;
    }

    @Override
    public int hashCode() {
      return value == null ? 0 : value.hashCode();
    }
  }

  public OTransactionConstraintChangesPerKey(final Object iKey) {
    this.key = iKey;
    entries = new ArrayList<OTransactionConstraintEntry>();
  }

  public void add(OIdentifiable iValue, final OPERATION iOperation) {
    synchronized (this) {
      entries.add(new OTransactionConstraintEntry(iValue != null ? iValue.getIdentity() : null, iOperation));
    }
  }

  /**
   * Interprets this key changes using the given {@link Interpretation interpretation}.
   *
   * @param interpretation the interpretation to use.
   *
   * @return the interpreted changes.
   */
  public Iterable<OTransactionConstraintEntry> interpret(Interpretation interpretation) {
    synchronized (this) {
      switch (interpretation) {
      case Conditional:
        return interpretAsConditional();
      case Unique:
        //TODO - FABIO : interpretation
        System.out.println("FABIO - implement the interpratation as Unique at OTransactionConstraintChangesPerKey.interpret(...)");
      default:
        throw new IllegalStateException("Unexpected interpretation '" + interpretation + "'");
      }
    }
  }

  public void clear() {
    synchronized (this) {
      entries.clear();
    }
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder(64);
    builder.append(key).append(" [");
    boolean first = true;
    for (OTransactionConstraintEntry entry : entries) {
      if (first)
        first = false;
      else
        builder.append(',');

      builder.append(entry.value).append(" (").append(entry.operation).append(")");
    }
    builder.append("]");
    return builder.toString();
  }

  private Iterable<OTransactionConstraintEntry> interpretAsConditional() {
    // 1. Handle common fast paths.

    if (entries.size() < 2)
      return new ArrayList<OTransactionConstraintEntry>(entries);

    if (entries.size() == 2) {
      final OTransactionConstraintEntry entryA = entries.get(0);
      final OTransactionConstraintEntry entryB = entries.get(1);

      if (entryA.operation == OPERATION.REMOVE && entryB.operation == OPERATION.REMOVE)
        return Collections.singletonList(entryA); // only one removal is observed anyway

      final ORID ridA = entryA.value == null ? null : entryA.value.getIdentity();
      final ORID ridB = entryB.value == null ? null : entryB.value.getIdentity();

      if (ridA != null && ridA.equals(ridB)) {
        if (entryA.operation == entryB.operation) // both operations do the same on the same RID
          return Collections.singletonList(entryA);

        if (entryB.operation == OPERATION.REMOVE) // remove operation cancels put
          return Collections.emptyList();

        return Collections.singletonList(entryB); // put wins
      }

      if (entryB.operation == OPERATION.REMOVE && entryB.value == null)
        return Collections.singletonList(entryB); // latest key removal wins

      return entryB.operation == OPERATION.PUT ?
          Collections.singletonList(entryB) /* latest put wins */ :
          Collections.singletonList(entryA) /* it's put-remove on different RIDs, put wins */;
    }

    // 2. Calculate observable changes to index.

    // XXX: We need to return only *latest observable* put, it always wins to other puts and removals. Unfortunately, there
    // is no lightweight way to find it out using standard Java data structures, thanks to Josh Bloch for not exposing the
    // LinkedHashMap's "doubly-linked list" interface to the public, but mentioning it in the clever javadoc. So we have to
    // maintain our own queue.
    final Deque<OTransactionConstraintEntry> lastObservedPuts = new ArrayDeque<OTransactionConstraintEntry>(entries.size());

    final Set<OTransactionConstraintEntry> interpretation = new HashSet<OTransactionConstraintEntry>(entries.size());
    OTransactionConstraintEntry firstExternalRemove = null;
    for (OTransactionConstraintEntry entry : entries) {
      final OIdentifiable value = entry.value;

      switch (entry.operation) {
      case PUT:
        assert value != null;

        interpretation.add(entry);
        lastObservedPuts.addLast(entry);
        break;
      case REMOVE:
        if (interpretation.remove(entry)) { // the put of this RID is no longer observable
          assert value != null;

          // Recalculate last visible put.

          if (entry.equals(lastObservedPuts.peekLast()))
            lastObservedPuts.removeLast();

          OTransactionConstraintEntry last;
          while ((last = lastObservedPuts.peekLast()) != null && !interpretation.contains(last)) // prune all unobservable puts
            lastObservedPuts.removeLast();
        } else {
          if (firstExternalRemove == null) // save only first external remove
            firstExternalRemove = entry;
          if (value == null) { // start from the scratch
            interpretation.clear();
            lastObservedPuts.clear();
          }
        }
        break;

      case CLEAR:
      default:
        assert false;
        break;
      }
    }

    // 3. Build resulting equivalent operation sequence.

    if (interpretation.isEmpty()) { // no observable changes except maybe some removal
      if (firstExternalRemove != null)
        return Collections.singletonList(firstExternalRemove);

      return Collections.emptyList();
    }

    return Collections.singletonList(lastObservedPuts.getLast()); // last visible put
  }

 private static Iterable<OTransactionConstraintEntry> swap(List<OTransactionConstraintEntry> list) {
    assert list.size() == 2;
    final List<OTransactionConstraintEntry> result = new ArrayList<OTransactionConstraintEntry>(2);
    result.add(list.get(1));
    result.add(list.get(0));
    return result;
  }

  /**
   * Defines interpretations supported by {@link #interpret(Interpretation)}.
   */
  public enum Interpretation {
    /**
     * Interpret changes like they were done for conditional constraint.
     */
    Conditional,
    
    /**
     * Interpret changes like they were done for unique constraint.
     */
    Unique
    
  }
}
