package com.orientechnologies.orient.core.constraint;

import java.util.Collection;
import java.util.Set;

import com.orientechnologies.common.listener.OProgressListener;
import com.orientechnologies.common.util.OApi;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.type.ODocumentWrapper;

/**
 * Manager of constraints.
 * 
 * @author fabio
 */
public interface OConstraintManager {

  OConstraintManager reload();

  /**
   * Returns all constraints registered in database.
   *
   * @return list of registered constraints.
   */
  Collection<? extends OConstraint<?>> getConstraints();

  /**
   * Constraint by specified name
   * 
   * @param iName
   *          name of constraint
   * 
   * @return constraint if one registered in database or null otherwise
   */
  OConstraint<?> getConstraint(final String iName);

  /**
   * Checks if constraint with specified name exists in database.
   *
   * @param iName
   *          name of constraint.
   *
   * @return true if constraint with specified name exists, false otherwise.
   */
  boolean existsConstraint(final String iName);

  /**
   * Creates a new constraint with default algorithm
   * 
   * @param iName
   *          - name of constraint
   * @param iType
   *          - constraint type
   * @param constraintDefinition
   *          - metadata that describes the constraint structure
   * @param clusterIdsToConstraint
   *          - ids of clusters that constraints should track for changes
   * @param progressListener
   *          - listener to track task progress
   * @param metadata
   *          - document with additional properties that can be used by constraint engine
   * 
   * @return a newly created constraint instance
   */
  OConstraint<?> createConstraint(final String iName, final String iType, OConstraintDefinition constraintDefinition,
      final int[] clusterIdsToConstraint, final OProgressListener progressListener, ODocument metadata);

  /**
   * Creates a new constraint
   * 
   * @param iName
   *          - name of constraint
   * @param iType
   *          - constraint type
   * @param constraintDefinition
   *          - metadata that describes the constraint structure
   * @param clusterIdsToConstraint
   *          - ids of clusters that constraints should track for changes
   * @param progressListener
   *          - listener to track task progress
   * @param metadata
   *          - document with additional properties that can be used by constraint engine
   * @param algorith
   *          - tip to a constraint factory what algorithm to use
   * 
   * @return a newly created constraint instance
   */
  OConstraint<?> createConstraint(final String iName, final String iType, OConstraintDefinition constraintDefinition,
      final int[] clusterIdsToConstraint, final OProgressListener progressListener, ODocument metadata, String algorithm);

  /**
   * Drop constraint with specified name. Do nothing if such constraint does not exists.
   *
   * @param iConstraintName
   *          the name of constraint to drop
   *
   * @return this
   */
  @OApi(maturity = OApi.MATURITY.STABLE)
  OConstraintManager dropConstraint(final String iConstraintName);

  /**
   * Removes constraint from class-property map.
   * <p>
   * IMPORTANT! Only for internal usage.
   *
   * @param idx
   *          constraint to remove.
   */
  @Deprecated
  void removeClassPropertyConstraint(OConstraint<?> constr);
  
  /**
   * Gets constraints for a specified class (excluding constraints for sub-classes).
   *
   * @param className name of class which is constrained.
   *
   * @return a set of constraints related to specified class
   */
  Set<OConstraint<?>> getClassConstraints(String className);
  
  /**
   * Gets constraints for a specified class (excluding constraints for sub-classes).
   *
   * @param className name of class which is constrained.
   * @param constraints   Collection of constraints where to add all the constraints
   */
  void getClassConstraints(String className, Collection<OConstraint<?>> constraints);
  
  /**
   * Saves constraint manager data.
   * <p>
   * IMPORTANT! Only for internal usage.
   */
  <RET extends ODocumentWrapper> RET save();
}
