package com.orientechnologies.orient.core.constraint;

import java.util.Map;
import java.util.Set;

import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.storage.OStorage;

/**
 * 
 * @author fabio
 *
 */
public interface OConstraintFactory {

  int getLastVersion();

  /**
   * @return List of supported constraints of this factory
   */
  Set<String> getTypes();

  /**
   * @return List of supported algorithms of this factory
   */
  Set<String> getAlgorithms();

  /**
   * Creates a constraint
   * 
   * @param name
   * @param storage
   * @param constraintType
   * @param algorithm
   * @param valueContainerAlgorithm
   * @param metadata
   * @param version
   * @return OConstraintInternal
   * @throws OConstraintException
   *           if creation failed
   */
  OConstraintInternal<?> createConstraint(String name, OStorage storage, String constraintType, String algorithm,
      String valueContainerAlgorithm, ODocument metadata, int version) throws OConstraintException;

  OConstraintEngine createConstraintEngine(String algorithm, String name, Boolean durableInNonTxMode, OStorage storage, int version,
      Map<String, String> engineProperties);
}
