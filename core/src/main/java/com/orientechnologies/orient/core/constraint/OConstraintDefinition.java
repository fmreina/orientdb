package com.orientechnologies.orient.core.constraint;

import java.util.List;

import com.orientechnologies.orient.core.collate.OCollate;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;

/**
 * 
 * @author fabio
 *
 */
public interface OConstraintDefinition extends OConstraintCallback {

  /**
   * @return Names of fields which given constraint is used to calculate key value. Order of fields is important.
   */
  List<String> getFields();
  
  String getConstraintParams();

  /**
   * @return Names of fields and their constraint modifiers (like "by value" for fields that hold <code>Map</code> values) which
   *         given constraint is used to calculate key value. Order of fields is important.
   */
  List<String> getFieldsToConstraint();

  /**
   * @return Name of the class which this constraint belongs to.
   */
  String getClassName();

  /**
   * {@inheritDoc}
   */
  boolean equals(Object index);

  /**
   * {@inheritDoc}
   */
  int hashCode();

  /**
   * {@inheritDoc}
   */
  String toString();

  String toCreateConstraintDDL(String constraintName, String constraintType, String engine);

  /**
   * Calculates key value by passed in parameters.
   * 
   * If it is impossible to calculate key value by given parameters <code>null</code> will be returned.
   * 
   * 
   * @param params
   *          Parameters from which constraint key will be calculated.
   * 
   * @return Key value or null if calculation is impossible.
   */
  Object createValue(Object... params);

  /**
   * Returns amount of parameters that are used to calculate key value.It does not mean that all parameters should be supplied. It
   * only means that if you provide more parameters they will be ignored and will not participate in constraint key calculation.
   * 
   * @return Amount of that are used to calculate key value. Call result should be equals to {@code getTypes().length}.
   */
  int getParamCount();

  /**
   * Return types of values from which constraint key consist. In case of constraint that is built on single document property value
   * single array that contains property type will be returned. In case of composite constraints result will contain several key
   * types.
   * 
   * @return Types of values from which constraint key consist.
   */
  OType[] getTypes();

  ODocument toStream();

  /**
   * Deserialize internal constraint state from document.
   * 
   * @param document
   *          Serialized constraint presentation.
   */
  void fromStream(ODocument document);

  boolean isAutomatic();

  OCollate getCollate();

  void setCollate(OCollate collate);

  boolean isNullValuesIgnored();

  void setNullValuesIgnored(boolean value);
}
