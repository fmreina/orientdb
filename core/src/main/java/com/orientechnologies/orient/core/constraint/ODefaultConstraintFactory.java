package com.orientechnologies.orient.core.constraint;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.storage.OStorage;
import com.orientechnologies.orient.core.storage.constraint.engine.ORemoteConstraintEngine;
import com.orientechnologies.orient.core.storage.constraint.engine.OSBTreeConstraintEngine;
import com.orientechnologies.orient.core.storage.impl.local.OAbstractPaginatedStorage;

/**
 * Default OrientDB constraint factory for constraints based on SBTree.<br>
 * Supports constraint types:
 * <ul>
 * <li>CONDITIONAL</li>
 * </ul>
 * 
 * @author fabio
 *
 */
public class ODefaultConstraintFactory implements OConstraintFactory {

  public static final String       SBTREE_ALGORITHM             = "SBTREE";

  public static final String       SBTREEBONSAI_VALUE_CONTAINER = "SBTREEBONSAISET";
  public static final String       NONE_VALUE_CONTAINER         = "NONE";

  private static final Set<String> TYPES;
  private static final Set<String> ALGORITHMS;

  static {
    final Set<String> types = new HashSet<String>();
    types.add(OClass.CONSTRAINT_TYPE.CONDITIONAL.toString());
    types.add(OClass.CONSTRAINT_TYPE.UNIQUE.toString());
    types.add(OClass.CONSTRAINT_TYPE.REQUIRED_EDGE.toString());
    types.add(OClass.CONSTRAINT_TYPE.IN_OUT_EDGE.toString());
    types.add(OClass.CONSTRAINT_TYPE.CARDINALITY.toString());
    TYPES = Collections.unmodifiableSet(types);
  }

  static {
    final Set<String> algorithms = new HashSet<String>();
    algorithms.add(SBTREE_ALGORITHM);
    ALGORITHMS = Collections.unmodifiableSet(algorithms);
  }

  public static boolean isMultiValueConstraint(final String constraintType) {
    switch (OClass.CONSTRAINT_TYPE.valueOf(constraintType)) {
    case CONDITIONAL:
    case UNIQUE:
    case REQUIRED_EDGE:
    case IN_OUT_EDGE:
    case CARDINALITY:
      return false;
    }
    return true;
  }

  /**
   * Constraint types :
   * <ul>
   * <li>CONDITIONAL</li>
   * </ul>
   */
  public Set<String> getTypes() {
    return TYPES;
  }

  public Set<String> getAlgorithms() {
    return ALGORITHMS;
  }

  @Override
  public OConstraintInternal<?> createConstraint(String name, OStorage storage, String constraintType, String algorithm,
      String valueContainerAlgorithm, ODocument metadata, int version) throws OConstraintException {
    if (valueContainerAlgorithm == null)
      valueContainerAlgorithm = NONE_VALUE_CONTAINER;

    if (version < 0)
      version = getLastVersion();

    if (SBTREE_ALGORITHM.equals(algorithm))
      return createSBTreeConstraint(name, constraintType, valueContainerAlgorithm, metadata,
          (OAbstractPaginatedStorage) storage.getUnderlying(), version);

    throw new OConstraintException("Unsupported type: " + constraintType);
  }

  private OConstraintInternal<?> createSBTreeConstraint(String name, String constraintType, String valueContainerAlgorithm,
      ODocument metadata, OAbstractPaginatedStorage storage, int version) {

    if (OClass.CONSTRAINT_TYPE.CONDITIONAL.toString().equals(constraintType)) {
      return new OConstraintConditional(name, constraintType, SBTREE_ALGORITHM, version, storage, valueContainerAlgorithm, metadata);
    } else if (OClass.CONSTRAINT_TYPE.UNIQUE.toString().equals(constraintType)) {
      return new OConstraintUnique(name, constraintType, SBTREE_ALGORITHM, version, storage, valueContainerAlgorithm, metadata);
    } else if (OClass.CONSTRAINT_TYPE.REQUIRED_EDGE.toString().equals(constraintType)) {
      return new OConstraintRequiredEdge(name, constraintType, SBTREE_ALGORITHM, version, storage, valueContainerAlgorithm, metadata);
    } else if (OClass.CONSTRAINT_TYPE.IN_OUT_EDGE.toString().equals(constraintType)) {
      return new OConstraintInOutEdge(name, constraintType, SBTREE_ALGORITHM, version, storage, valueContainerAlgorithm, metadata);
    }else if (OClass.CONSTRAINT_TYPE.CARDINALITY.toString().equals(constraintType)) {
      return new OConstraintCardinality(name, constraintType, SBTREE_ALGORITHM, version, storage, valueContainerAlgorithm, metadata);
    }
    

    throw new OConstraintException("Unsupported type: " + constraintType);
  }

  @Override
  public int getLastVersion() {
    return OSBTreeConstraintEngine.VERSION;
  }

  @Override
  public OConstraintEngine createConstraintEngine(String algorithm, String name, Boolean durableInNonTxMode, OStorage storage,
      int version, Map<String, String> engineProperties) {
    final OConstraintEngine constraintEngine;

    final String storageType = storage.getType();

    if (storageType.equals("memory") || storageType.equals("plocal"))
      constraintEngine = new OSBTreeConstraintEngine(name, (OAbstractPaginatedStorage) storage, version);
    else if (storageType.equals("distributed"))
      // DISTRIBUTED CASE: HANDLE IT AS FOR LOCAL
      constraintEngine = new OSBTreeConstraintEngine(name, (OAbstractPaginatedStorage) storage.getUnderlying(), version);
    else if (storageType.equals("remote"))
      constraintEngine = new ORemoteConstraintEngine(name);
    else
      throw new OConstraintException("Unsupported storage type: " + storageType);

    return constraintEngine;

  }
}
