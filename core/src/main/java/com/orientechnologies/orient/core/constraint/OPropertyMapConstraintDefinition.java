package com.orientechnologies.orient.core.constraint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.orientechnologies.orient.core.db.record.OMultiValueChangeEvent;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;

/**
 * 
 * @author fabio
 *
 */
public class OPropertyMapConstraintDefinition extends OAbstractConstraintDefinitionMultiValue {

  private static final long serialVersionUID = -2939919074512250250L;

  public static enum CONSTRAINT_BY {
    KEY, VALUE
  }

  private CONSTRAINT_BY constraintBy = CONSTRAINT_BY.KEY;

  public OPropertyMapConstraintDefinition() {
  }

  public OPropertyMapConstraintDefinition(final String iClassName, final String iField, final OType iType,
      final CONSTRAINT_BY constraintBy) {
    super(iClassName, iField, iType);

    if (constraintBy == null)
      throw new NullPointerException("You have to provide way by which map entries should be mapped");

    this.constraintBy = constraintBy;
  }

  @Override
  public Object getDocumentValueToConstraint(ODocument iDocument) {
    return createValue(iDocument.<Object> field(field));
  }

  @Override
  public Object createValue(List<?> params) {
    if (!(params.get(0) instanceof Map))
      return null;

    final Collection<?> mapParams = extractMapParams((Map<?, ?>) params.get(0));
    final List<Object> result = new ArrayList<Object>(mapParams.size());
    for (final Object mapParam : mapParams) {
      result.add(createSingleValue(mapParam));
    }

    return result;
  }

  @Override
  public Object createValue(Object... params) {
    if (!(params[0] instanceof Map))
      return null;

    final Collection<?> mapParams = extractMapParams((Map<?, ?>) params[0]);

    final List<Object> result = new ArrayList<>(mapParams.size());
    for (final Object mapParam : mapParams) {
      Object val = createSingleValue(mapParam);
      result.add(val);
    }

    if (getFieldsToConstraint().size() == 1 && result.size() == 1) {
      return result.get(0);
    }

    return result;
  }

  public CONSTRAINT_BY getConstraintBy() {
    return constraintBy;
  }

  @Override
  protected void serializeToStream() {
    super.serializeToStream();
    document.field("mapConstraintBy", constraintBy.toString());
  }

  @Override
  protected void serializeFromStream() {
    super.serializeFromStream();
    constraintBy = CONSTRAINT_BY.valueOf(document.<String> field("mapConstraintBy"));
  }

  private Collection<?> extractMapParams(Map<?, ?> map) {
    if (constraintBy == CONSTRAINT_BY.KEY)
      return map.keySet();

    return map.values();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;

    OPropertyMapConstraintDefinition that = (OPropertyMapConstraintDefinition) o;

    if (constraintBy != that.constraintBy)
      return false;

    return true;
  }

  @Override
  public Object createSingleValue(Object... param) {
    return OType.convert(param[0], keyType.getDefaultJavaType());
  }

  @Override
  public void processChangeEvent(OMultiValueChangeEvent<?, ?> changeEvent, Map<Object, Integer> keysToAdd,
      Map<Object, Integer> keysToRemove) {
    final boolean result;
    if (constraintBy.equals(CONSTRAINT_BY.KEY))
      result = processKeyChangeEvent(changeEvent, keysToAdd, keysToRemove);
    else
      result = processValueChangeEvent(changeEvent, keysToAdd, keysToRemove);

    if (!result)
      throw new IllegalArgumentException("Invalid change type :" + changeEvent.getChangeType());
  }

  private boolean processKeyChangeEvent(final OMultiValueChangeEvent<?, ?> changeEvent, final Map<Object, Integer> keysToAdd,
      final Map<Object, Integer> keysToRemove) {
    switch (changeEvent.getChangeType()) {
    case ADD:
      processAdd(createSingleValue(changeEvent.getKey()), keysToAdd, keysToRemove);
      return true;
    case REMOVE:
      processRemoval(createSingleValue(changeEvent.getKey()), keysToAdd, keysToRemove);
      return true;
    case UPDATE:
      return true;
    }
    return false;
  }

  private boolean processValueChangeEvent(final OMultiValueChangeEvent<?, ?> changeEvent, final Map<Object, Integer> keysToAdd,
      final Map<Object, Integer> keysToRemove) {
    switch (changeEvent.getChangeType()) {
    case ADD:
      processAdd(createSingleValue(changeEvent.getValue()), keysToAdd, keysToRemove);
      return true;
    case REMOVE:
      processRemoval(createSingleValue(changeEvent.getOldValue()), keysToAdd, keysToRemove);
      return true;
    case UPDATE:
      processRemoval(createSingleValue(changeEvent.getOldValue()), keysToAdd, keysToRemove);
      processAdd(createSingleValue(changeEvent.getValue()), keysToAdd, keysToRemove);
      return true;
    }
    return false;
  }

  @Override
  public List<String> getFieldsToConstraint() {
    if (constraintBy == CONSTRAINT_BY.KEY)
      return Collections.singletonList(field + " by key");
    return Collections.singletonList(field + " by value");
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + constraintBy.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "OPropertyMapConstraintDefinition{" + "constraintBy=" + constraintBy + "} " + super.toString();
  }

  @Override
  public String toCreateConstraintDDL(String constraintName, String constraintType, String engine) {
    final StringBuilder ddl = new StringBuilder("create constraint `");

    ddl.append(constraintName).append("` on `");
    ddl.append(className).append("` ( `").append(field).append("`");

    if (constraintBy == CONSTRAINT_BY.KEY)
      ddl.append(" by key");
    else
      ddl.append(" by value");

    ddl.append(" ) ");
    ddl.append(constraintType);

    return ddl.toString();
  }
}
