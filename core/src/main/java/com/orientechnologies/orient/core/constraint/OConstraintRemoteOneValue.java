package com.orientechnologies.orient.core.constraint;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.executor.OResultSet;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintRemoteOneValue extends OConstraintRemote<OIdentifiable> {

  protected final static String QUERY_GET = "select rid from constraint:`%s` where key = ?";

  public OConstraintRemoteOneValue(final String iName, final String iWrappedType, final String algorithm, final ORID iRid,
      final OConstraintDefinition iConstraintDefinition, final ODocument iConfiguration, final Set<String> clustersToIndex,
      String database) {
    super(iName, iWrappedType, algorithm, iRid, iConstraintDefinition, iConfiguration, clustersToIndex, database);
  }

  @Override
  public OIdentifiable get(final Object iKey) {
    try (final OResultSet result = getDatabase().constraintQuery(getName(), String.format(QUERY_GET, name), iKey)) {
      if (result != null && result.hasNext())
        return ((OIdentifiable) result.next().getProperty("rid"));
      return null;
    }
  }

  public Iterator<Entry<Object, OIdentifiable>> iterator() {
    try (final OResultSet result = getDatabase().constraintQuery(getName(), String.format(QUERY_ENTRIES, name))) {

      final Map<Object, OIdentifiable> map = result.stream()
          .collect(Collectors.toMap((res) -> res.getProperty("key"), (res) -> res.getProperty("rid")));

      return map.entrySet().iterator();
    }
  }

  @Override
  public boolean supportsOrderedIterations() {
    return false;
  }

  @Override
  public boolean isUnique() {
    return true;
  }
}
