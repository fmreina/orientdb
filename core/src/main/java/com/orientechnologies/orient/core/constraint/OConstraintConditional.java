package com.orientechnologies.orient.core.constraint;

import java.util.List;
import java.util.Map;

import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.exception.OInvalidConstraintEngineIdException;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ODocumentEntry;
import com.orientechnologies.orient.core.sql.parser.OExpression;
import com.orientechnologies.orient.core.sql.parser.OIdentifier;
import com.orientechnologies.orient.core.sql.parser.OrientSqlConstants;
import com.orientechnologies.orient.core.sql.parser.Token;
import com.orientechnologies.orient.core.storage.OConstraintValidationException;
import com.orientechnologies.orient.core.storage.impl.local.OAbstractPaginatedStorage;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintConditional extends OConstraintOneValue {

  private final OConstraintEngine.Validator<Object, OIdentifiable> CONSTRAINT_VALIDATOR = new OConstraintEngine.Validator<Object, OIdentifiable>() {
    @Override
    public Object validate(Object key, OIdentifiable oldValue, OIdentifiable newValue,
        Map<String, List<OConstraintDto>> fields, String params) {

//      System.out.println("\nValidation of the CONDITIONAL constraint.");
//      System.out.println("Params: " + params);
//      System.out.println("Entered Values:");
//      System.out.println("oldValue: " + oldValue);
//      System.out.println("newValue: "+newValue);
//      fields.keySet().forEach(k -> {
//        String cName = k;
//        System.out.println(cName);
//        List<OConstraintDto> listDto = fields.get(k);
//        listDto.forEach(dto -> {
//          Map<String, ODocumentEntry> m = dto.getAttributes();
//          m.keySet().forEach(s -> {
//            String attribute = s;
//            System.out.print("\t");
//            System.out.print(attribute);
//            System.out.print(" = ");
//            ODocumentEntry doc = m.get(s);
//            System.out.println(doc.value);
//          });
//          System.out.println();
//        });
//      });

      fields.values().forEach(list -> {
        list.forEach(dto -> doValidate(dto.getAttributes(), params));
      });

      if (!newValue.getIdentity().isPersistent())
        newValue = newValue.getRecord();
      return newValue.getIdentity();
    }

    private void doValidate(Map<String, ODocumentEntry> fields, String params) {
      if (params != null && !params.isEmpty()) {
        OConditionalParameters constraintParams = new OConditionalParameters(params);

        OExpression constraintIfValue = constraintParams.getIfPropertyValue();
        Token ifToken = constraintParams.getIfTokenCondition();
        ODocumentEntry enteredIfValue = fields.get(constraintParams.getIfPropertyName().toString());

        if (constraintIfValue != null && enteredIfValue != null && compare(enteredIfValue, ifToken, constraintIfValue)) {
          OIdentifier constraintThenName = constraintParams.getThenPropertyName();
          OExpression constraintThenValue = constraintParams.getThenPropertyValue();
          Token thenToken = constraintParams.getThenTokenCondition();
          ODocumentEntry enteredThenValue = fields.get(constraintParams.getThenPropertyName().toString());

          if (constraintThenValue != null && enteredThenValue != null
              && !compare(enteredThenValue, thenToken, constraintThenValue)) {
            throw new OConstraintValidationException("A constraint was violated. It must be as follow: " + constraintThenName + " "
                + thenToken + " " + constraintThenValue, getName());
          }
        } else {
          OIdentifier constraintElseName = constraintParams.getElsePropertyName();
          OExpression constraintElseValue = constraintParams.getElsePropertyValue();
          Token elseToken = constraintParams.getElseTokenCondition();
          ODocumentEntry enteredElseValue = fields.get(constraintParams.getThenPropertyName().toString());

          if (constraintElseValue != null && enteredElseValue != null
              && !compare(enteredElseValue, elseToken, constraintElseValue)) {
            throw new OConstraintValidationException("A constraint was violated. It must be as follow: " + constraintElseName + " "
                + elseToken + " " + constraintElseValue, getName());
          }
        }
      }
    }

    private boolean compare(ODocumentEntry enteredValue, Token token, OExpression constraintValue) {
      // FIXME: it only works for integer values
      Integer a = (Integer) enteredValue.value;
      Integer b = Integer.valueOf(constraintValue.toString());
      switch (token.kind) {
      case OrientSqlConstants.EQ:
        return a == b;
      case OrientSqlConstants.NE:
        return a != b;
      case OrientSqlConstants.GT:
        return a > b;
      case OrientSqlConstants.LT:
        return a < b;
      case OrientSqlConstants.GE:
        return a >= b;
      case OrientSqlConstants.LE:
        return a <= b;
      default:
        return false;
      }
    }
  };

  public OConstraintConditional(String name, final String type, String algorithm, int version, OAbstractPaginatedStorage storage,
      String valueContainerAlgorithm, ODocument metadata) {
    super(name, type, algorithm, valueContainerAlgorithm, metadata, version, storage);
  }

  @Override
  public OConstraint<OIdentifiable> put(Object key, final OIdentifiable value, Map<String, List<OConstraintDto>> fields) {
    key = getCollatingValue(key);

    acquireSharedLock();
    try {
      while (true) {
        try {
          storage.validatedPutConstraintValue(constraintId, key, value, CONSTRAINT_VALIDATOR, fields,
              getConstraintDefinition().getConstraintParams());
          return this;
        } catch (OInvalidConstraintEngineIdException ignore) {
          doReloadConstraintEngine();
        }
      }

    } finally {
      releaseSharedLock();
    }
  }

  @Override
  public boolean supportsOrderedIterations() {
    return false;
  }

}
