package com.orientechnologies.orient.core.constraint;

import java.util.Map;

import com.orientechnologies.orient.core.db.ODatabaseDocumentInternal;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.tx.OTransactionConstraintChanges;
import com.orientechnologies.orient.core.tx.OTransactionConstraintChanges.OPERATION;
import com.orientechnologies.orient.core.tx.OTransactionConstraintChangesPerKey;
import com.orientechnologies.orient.core.tx.OTransactionConstraintChangesPerKey.OTransactionConstraintEntry;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintTxAwareOneValue extends OConstraintTxAware<OIdentifiable>{

  //XXX: Not implemented in comparison to @OConstraintTxAwareOneValue - Fabio
  //public boolean contains() @OConstraintAbstractDelegate @OConstraint
  //public OConstraintCursor iterateEntriesBetween() @OConstraintAbstractDelegate @OConstraint
  // public OConstraintCursor iterateEntriesMajor() @OConstraintAbstractDelegate @OConstraint
  //public OConstraintCursor iterateEntriesMinor() @OConstraintAbstractDelegate @OConstraint
  //public OConstraintCursor iterateEntries() @OConstraintAbstractDelegate @OConstraint
  
  public OConstraintTxAwareOneValue(final ODatabaseDocumentInternal iDatabase, final OConstraint<OIdentifiable> delegate) {
    super(iDatabase, delegate);
  }

  @Override
  public OIdentifiable get(Object key) {
    final OTransactionConstraintChanges constraintChanges = database.getMicroOrRegularTransaction()
        .getConstraintChangesInternal(delegate.getName());
    if (constraintChanges == null)
      return super.get(key);

    key = getCollatingValue(key);

    OIdentifiable result;
    if (!constraintChanges.cleared)
      // BEGIN FROM THE UNDERLYING RESULT SET
      result = super.get(key);
    else
      // BEGIN FROM EMPTY RESULT SET
      result = null;

    // FILTER RESULT SET WITH TRANSACTIONAL CHANGES
    final Map.Entry<Object, OIdentifiable> entry = calculateTxConstraintEntry(key, result, constraintChanges);
    if (entry == null)
      return null;

    return entry.getValue();
  }
  
  private Map.Entry<Object, OIdentifiable> calculateTxConstraintEntry(final Object key, final OIdentifiable backendValue,
      final OTransactionConstraintChanges constraintChanges) {
    OIdentifiable result = backendValue;
    final OTransactionConstraintChangesPerKey changesPerKey = constraintChanges.getChangesPerKey(key);
    if (changesPerKey.entries.isEmpty()) {
      if (backendValue == null) {
        return null;
      } else {
        return createMapEntry(key, backendValue);
      }
    }

    for (OTransactionConstraintEntry entry : changesPerKey.entries) {
      if (entry.operation == OPERATION.REMOVE)
        result = null;
      else if (entry.operation == OPERATION.PUT)
        result = entry.value;
    }

    if (result == null)
      return null;

    final OIdentifiable resultValue = result;
    return createMapEntry(key, resultValue);
  }
  
  private Map.Entry<Object, OIdentifiable> createMapEntry(final Object key, final OIdentifiable resultValue) {
    return new MapEntry(key, resultValue);
  }
  
  private static class MapEntry implements Map.Entry<Object, OIdentifiable> {
    private final Object        key;
    private final OIdentifiable resultValue;

    public MapEntry(Object key, OIdentifiable resultValue) {
      this.key = key;
      this.resultValue = resultValue;
    }

    @Override
    public Object getKey() {
      return key;
    }

    @Override
    public OIdentifiable getValue() {
      return resultValue;
    }

    @Override
    public OIdentifiable setValue(OIdentifiable value) {
      throw new UnsupportedOperationException("setValue");
    }
  }
  
//  private class PureTxBetweenConstraintForwardCursor extends OConstraintAbstractCursor {
//    private final OTransactionConstraintChanges constraintChanges;
//    private       Object                   firstKey;
//    private       Object                   lastKey;
//
//    private Object nextKey;
//
//    public PureTxBetweenConstraintForwardCursor(Object fromKey, boolean fromInclusive, Object toKey, boolean toInclusive,
//        OTransactionConstraintChanges constraintChanges) {
//      this.constraintChanges = constraintChanges;
//
//      fromKey = enhanceFromCompositeKeyBetweenAsc(fromKey, fromInclusive);
//      toKey = enhanceToCompositeKeyBetweenAsc(toKey, toInclusive);
//
//      final Object[] keys = constraintChanges.firstAndLastKeys(fromKey, fromInclusive, toKey, toInclusive);
//      if (keys.length == 0) {
//        nextKey = null;
//      } else {
//        firstKey = keys[0];
//        lastKey = keys[1];
//
//        nextKey = firstKey;
//      }
//    }
//
//    @Override
//    public Map.Entry<Object, OIdentifiable> nextEntry() {
//      if (nextKey == null)
//        return null;
//
//      Map.Entry<Object, OIdentifiable> result;
//
//      do {
//        result = calculateTxConstraintEntry(nextKey, null, constraintChanges);
//        nextKey = constraintChanges.getHigherKey(nextKey);
//
//        if (nextKey != null && ODefaultComparator.INSTANCE.compare(nextKey, lastKey) > 0)
//          nextKey = null;
//
//      } while (result == null && nextKey != null);
//
//      return result;
//    }
// }
//  
//  private class PureTxBetweenConstraintBackwardCursor extends OConstraintAbstractCursor {
//    private final OTransactionConstraintChanges constraintChanges;
//    private       Object                   firstKey;
//    private       Object                   lastKey;
//
//    private Object nextKey;
//
//    public PureTxBetweenConstraintBackwardCursor(Object fromKey, boolean fromInclusive, Object toKey, boolean toInclusive,
//        OTransactionConstraintChanges constraintChanges) {
//      this.constraintChanges = constraintChanges;
//
//      fromKey = enhanceFromCompositeKeyBetweenDesc(fromKey, fromInclusive);
//      toKey = enhanceToCompositeKeyBetweenDesc(toKey, toInclusive);
//
//      final Object[] keys = constraintChanges.firstAndLastKeys(fromKey, fromInclusive, toKey, toInclusive);
//      if (keys.length == 0) {
//        nextKey = null;
//      } else {
//        firstKey = keys[0];
//        lastKey = keys[1];
//
//        nextKey = lastKey;
//      }
//    }
//
//    @Override
//    public Map.Entry<Object, OIdentifiable> nextEntry() {
//      if (nextKey == null)
//        return null;
//
//      Map.Entry<Object, OIdentifiable> result;
//      do {
//        result = calculateTxConstraintEntry(nextKey, null, constraintChanges);
//        nextKey = constraintChanges.getLowerKey(nextKey);
//
//        if (nextKey != null && ODefaultComparator.INSTANCE.compare(nextKey, firstKey) < 0)
//          nextKey = null;
//      } while (result == null && nextKey != null);
//
//      return result;
//    }
//  }
//
//  private class OConstraintTxCursor extends OConstraintAbstractCursor {
//
//    private final OConstraintCursor             backedCursor;
//    private final boolean                  ascOrder;
//    private final OTransactionConstraintChanges constraintChanges;
//    private       OConstraintCursor             txBetweenConstraintCursor;
//
//    private Map.Entry<Object, OIdentifiable> nextTxEntry;
//    private Map.Entry<Object, OIdentifiable> nextBackedEntry;
//
//    private boolean firstTime;
//
//    public OConstraintTxCursor(OConstraintCursor txCursor, OConstraintCursor backedCursor, boolean ascOrder,
//        OTransactionConstraintChanges constraintChanges) {
//      this.backedCursor = backedCursor;
//      this.ascOrder = ascOrder;
//      this.constraintChanges = constraintChanges;
//      txBetweenConstraintCursor = txCursor;
//      firstTime = true;
//    }
//
//    @Override
//    public Map.Entry<Object, OIdentifiable> nextEntry() {
//      if (firstTime) {
//        nextTxEntry = txBetweenConstraintCursor.nextEntry();
//        nextBackedEntry = backedCursor.nextEntry();
//        firstTime = false;
//      }
//
//      Map.Entry<Object, OIdentifiable> result = null;
//
//      while (result == null && (nextTxEntry != null || nextBackedEntry != null)) {
//        if (nextTxEntry == null && nextBackedEntry != null) {
//          result = nextBackedEntry(getPrefetchSize());
//        } else if (nextBackedEntry == null && nextTxEntry != null) {
//          result = nextTxEntry(getPrefetchSize());
//        } else if (nextTxEntry != null && nextBackedEntry != null) {
//          if (ascOrder) {
//            if (ODefaultComparator.INSTANCE.compare(nextBackedEntry.getKey(), nextTxEntry.getKey()) <= 0) {
//              result = nextBackedEntry(getPrefetchSize());
//            } else {
//              result = nextTxEntry(getPrefetchSize());
//            }
//          } else {
//            if (ODefaultComparator.INSTANCE.compare(nextBackedEntry.getKey(), nextTxEntry.getKey()) >= 0) {
//              result = nextBackedEntry(getPrefetchSize());
//            } else {
//              result = nextTxEntry(getPrefetchSize());
//            }
//          }
//        }
//      }
//
//      return result;
//    }
//
//    private Map.Entry<Object, OIdentifiable> nextTxEntry(int prefetchSize) {
//      Map.Entry<Object, OIdentifiable> result = nextTxEntry;
//      nextTxEntry = txBetweenConstraintCursor.nextEntry();
//      return result;
//    }
//
//    private Map.Entry<Object, OIdentifiable> nextBackedEntry(int prefetchSize) {
//      Map.Entry<Object, OIdentifiable> result;
//      result = calculateTxConstraintEntry(nextBackedEntry.getKey(), nextBackedEntry.getValue(), constraintChanges);
//      nextBackedEntry = backedCursor.nextEntry();
//      return result;
//    }
//  }
}
