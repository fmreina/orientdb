package com.orientechnologies.orient.core.constraint;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.orientechnologies.orient.core.collate.ODefaultCollate;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.db.record.ORecordElement;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandExecutorSQLCreateConstraint;

/**
 * 
 * @author fabio
 *
 */
public class OPropertyConstraintDefinition extends OAbstractConstraintDefinition {

  private static final long serialVersionUID = 1296054490950825393L;
  protected String          className;
  protected String          field;
  protected OType           keyType;
  protected String params;

  public OPropertyConstraintDefinition(final String iClassName, String iField, OType iType, OConstraintParameters params) {
    super();
    this.className = iClassName;
    this.field = iField;
    this.keyType = iType;
    this.params = params.toString();
  }

  public OPropertyConstraintDefinition() {
  }

  @Override
  public List<String> getFields() {
    return Collections.singletonList(field);
  }

  @Override
  public String getConstraintParams() {
    return params;
  }
  
  @Override
  public List<String> getFieldsToConstraint() {
    if (collate == null || collate.getName().equals(ODefaultCollate.NAME))
      return Collections.singletonList(field);
    return Collections.singletonList(field + " collate " + collate.getName());
  }

  @Override
  public String getClassName() {
    return className;
  }

  @Override
  public ODocument toStream() {
    document.setInternalStatus(ORecordElement.STATUS.UNMARSHALLING);
    try {
      serializeToStream();
    } finally {
      document.setInternalStatus(ORecordElement.STATUS.LOADED);
    }
    return document;
  }

  @Override
  protected void serializeToStream() {
    super.serializeToStream();
    document.field("className", className);
    document.field("field", field);
    document.field("keyType", keyType.toString());
    document.field("collate", collate.getName());
    document.field("nullValuesIgnored", isNullValuesIgnored());
    document.field("params", params.toString());
  }

  @Override
  protected void fromStream() {
    serializeFromStream();
  }

  protected void serializeFromStream() {
    super.serializeFromStream();

    className = document.field("className");
    field = document.field("field");

    final String keyTypeStr = document.field("keyType");
    keyType = OType.valueOf(keyTypeStr);

    setCollate((String) document.field("collate"));
    setNullValuesIgnored(!Boolean.FALSE.equals(document.<Boolean> field("nullValuesIgnored")));
    
    params = document.field("params");
  }

  @Override
  public String toCreateConstraintDDL(final String constraintName, final String constraintType, final String engine) {
    return createConstraintDDLWithFieldType(constraintName, constraintType, engine).toString();
  }

  public StringBuilder createConstraintDDLWithFieldType(String constraintName, String constraintType, String engine) {
    final StringBuilder ddl = createConstraintDDLWithoutFieldType(constraintName, constraintType, engine);
    ddl.append(' ').append(keyType.name());
    return ddl;
  }

  protected StringBuilder createConstraintDDLWithoutFieldType(final String constraintName, final String constraintType,
      final String engine) {
    final StringBuilder ddl = new StringBuilder("create constraint `");
    ddl.append(constraintName).append("` on `");
    ddl.append(className).append("` (`").append(field).append("`");

    if (collate.getName().equals(ODefaultCollate.NAME))
      ddl.append(" collate ").append(collate.getName());

    ddl.append(" ) ");
    ddl.append(constraintType);

    if (engine != null)
      ddl.append(' ').append(OCommandExecutorSQLCreateConstraint.KEYWORD_ENGINE + " " + engine);

    return ddl;
  }

  @Override
  public int getParamCount() {
    return 1;
  }

  @Override
  public Object getDocumentValueToConstraint(ODocument iDocument) {
    if (OType.LINK.equals(keyType)) {
      final OIdentifiable identifiable = iDocument.field(field);
      if (identifiable != null)
        return createValue(identifiable.getIdentity());
      else
        return null;
    }
    return createValue(iDocument.<Object> field(field));
  }

  public Object createValue(final List<?> params) {
    return OType.convert(params.get(0), keyType.getDefaultJavaType());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object createValue(Object... params) {
    return OType.convert(params[0], keyType.getDefaultJavaType());
  }

  @Override
  public OType[] getTypes() {
    return new OType[] { keyType };
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    if (!super.equals(o))
      return false;

    final OPropertyConstraintDefinition that = (OPropertyConstraintDefinition) o;

    if (!className.equals(that.className))
      return false;
    if (!field.equals(that.field))
      return false;
    if (keyType != that.keyType)
      return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + className.hashCode();
    result = 31 * result + field.hashCode();
    result = 31 * result + keyType.hashCode();
//    result = 31 * result + params.hashCode(); //TODO - FABIO:
    return result;
  }

  @Override
  public String toString() {
    return "OPropertyConstraintDefinition{" + "className='" + className + '\'' + ", field='" + field + '\'' + ", keyType=" + keyType
        + ", collate=" + collate + ", null values ignored = " + isNullValuesIgnored() + '}';
  }

  @Override
  public boolean isAutomatic() {
    return true;
  }

}
