/*
 *
 *  *  Copyright 2010-2016 OrientDB LTD (http://orientdb.com)
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *
 *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *  *
 *  * For more information: http://orientdb.com
 *
 */

package com.orientechnologies.orient.core.storage;

import com.orientechnologies.common.exception.OHighLevelException;
import com.orientechnologies.orient.core.exception.OCoreException;
import com.orientechnologies.orient.core.id.ORID;

/**
 * Copy from @ORecordDuplicatedException adapted to duplicated unique constraint (@OConstraintUnique)
 * 
 * @author fabio
 *
 */
public class ORecordDuplicatedConstraintException extends OCoreException implements OHighLevelException {
  private final ORID   rid;
  private final String constraintName;
  private final Object key;

  public ORecordDuplicatedConstraintException(final ORecordDuplicatedConstraintException exception) {
    super(exception);
    this.constraintName = exception.constraintName;
    this.rid = exception.rid;
    this.key = exception.key;
  }

  public ORecordDuplicatedConstraintException(final String message, final String constraintName, final ORID iRid, Object key) {
    super(message);
    this.constraintName = constraintName;
    this.rid = iRid;
    this.key = key;
  }

  public ORID getRid() {
    return rid;
  }

  public String getConstraintName() {
    return constraintName;
  }

  public Object getKey() {
    return key;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null || !obj.getClass().equals(getClass()))
      return false;

    if (!constraintName.equals(((ORecordDuplicatedConstraintException) obj).constraintName))
      return false;

    return rid.equals(((ORecordDuplicatedConstraintException) obj).rid);
  }

  @Override
  public int hashCode() {
    return rid.hashCode();
  }

  @Override
  public String toString() {
    return super.toString() + " CONSTRAINT=" + constraintName + " RID=" + rid;
  }
}
