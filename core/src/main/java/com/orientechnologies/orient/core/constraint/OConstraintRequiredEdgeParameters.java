package com.orientechnologies.orient.core.constraint;

import java.io.Serializable;

import com.orientechnologies.orient.core.sql.parser.OIdentifier;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintRequiredEdgeParameters implements OConstraintParameters, Serializable {

  private static final long serialVersionUID = -8510537178146339047L;

  public OIdentifier        originClass;
  public OIdentifier        edgeType;
  public OIdentifier        targetClass;

  public OConstraintRequiredEdgeParameters() {
  }

  public OConstraintRequiredEdgeParameters(OIdentifier originClass, OIdentifier edgeType, OIdentifier targetClass) {
    super();
    this.originClass = originClass;
    this.edgeType = edgeType;
    this.targetClass = targetClass;
  }

  public OConstraintRequiredEdgeParameters(String params) {
    strToParam(params);
  }

  @Override
  public OConstraintParameters strToParam(String str) {
    // FIXME
    String[] props = str.split(" ");
    // TODO - FABIO: fazer a conversão dos valores de string para os artibutos desse objeto para retornar um objeto desse tipo
    this.originClass = new OIdentifier(props[1]);
    
    this.edgeType = new OIdentifier(props[3]);
    
    this.targetClass = new OIdentifier(props[5]);

    return this;
  }

  @Override
  public String toString() {
    return "[ " + originClass + " -- " + edgeType + " -> " + targetClass + " ]";
  }

  public OIdentifier getOriginClass() {
    return originClass;
  }

  public OIdentifier getEdgeType() {
    return edgeType;
  }

  public OIdentifier getTargetClass() {
    return targetClass;
  }

}
