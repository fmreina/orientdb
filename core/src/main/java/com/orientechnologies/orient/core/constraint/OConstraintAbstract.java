package com.orientechnologies.orient.core.constraint;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import com.orientechnologies.common.concur.lock.OReadersWriterSpinLock;
import com.orientechnologies.common.exception.OException;
import com.orientechnologies.common.listener.OProgressListener;
import com.orientechnologies.common.log.OLogManager;
import com.orientechnologies.common.serialization.types.OBinarySerializer;
import com.orientechnologies.orient.core.db.ODatabaseDocumentInternal;
import com.orientechnologies.orient.core.db.ODatabaseRecordThreadLocal;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.exception.OCommandExecutionException;
import com.orientechnologies.orient.core.exception.OConfigurationException;
import com.orientechnologies.orient.core.exception.OInvalidConstraintEngineIdException;
import com.orientechnologies.orient.core.exception.OTooBigConstraintKeyException;
import com.orientechnologies.orient.core.intent.OIntentMassiveInsert;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.ORecord;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ODocumentEntry;
import com.orientechnologies.orient.core.record.impl.ODocumentInternal;
import com.orientechnologies.orient.core.storage.OStorage;
import com.orientechnologies.orient.core.storage.cache.OReadCache;
import com.orientechnologies.orient.core.storage.cache.OWriteCache;
import com.orientechnologies.orient.core.storage.impl.local.OAbstractPaginatedStorage;
import com.orientechnologies.orient.core.storage.impl.local.paginated.atomicoperations.OAtomicOperation;
import com.orientechnologies.orient.core.storage.ridbag.sbtree.OConstraintRIDContainer;
import com.orientechnologies.orient.core.tx.OTransactionConstraintChanges;
import com.orientechnologies.orient.core.tx.OTransactionConstraintChangesPerKey;

/**
 * 
 * @author fabio
 *
 * @param <T>
 */
public abstract class OConstraintAbstract<T> implements OConstraintInternal<T> {

  protected static final String              CONFIG_MAP_RID       = "mapRid";
  protected static final String              CONFIG_CLUSTERS      = "clusters";
  protected final String                     type;
  protected final ODocument                  metadata;
  protected final OAbstractPaginatedStorage  storage;
  private final String                       databaseName;
  private final String                       name;
  private final OReadersWriterSpinLock       rwLock               = new OReadersWriterSpinLock();
  private final AtomicLong                   rebuildVersion       = new AtomicLong();
  private final int                          version;
  protected volatile ConstraintConfiguration configuration;
  protected String                           valueContainerAlgorithm;
  protected int                              constraintId         = -1;
  protected Set<String>                      clustersToConstraint = new HashSet<String>();
  private String                             algorithm;
  private volatile OConstraintDefinition     constraintDefinition;
  private volatile boolean                   rebuilding           = false;
  private Map<String, String>                engineProperties     = new HashMap<String, String>();

  public OConstraintAbstract(String name, final String type, final String algorithm, final String valueContainerAlgorithm,
      final ODocument metadata, final int version, final OStorage storage) {
    acquireExclusiveLock();
    try {
      this.databaseName = storage.getName();
      this.version = version;
      this.name = name;
      this.type = type;
      this.algorithm = algorithm;
      this.metadata = metadata;
      this.valueContainerAlgorithm = valueContainerAlgorithm;
      this.storage = (OAbstractPaginatedStorage) storage.getUnderlying();
    } finally {
      releaseExclusiveLock();
    }
  }

  public static OConstraintMetadata loadMetadataInternal(final ODocument config, final String type, final String algorithm,
      final String valueContainerAlgorithm) {

    final String constraintName = config.field(OConstraintInternal.CONFIG_NAME);
    final ODocument constraintDefinitionDoc = config.field(OConstraintInternal.CONSTRAINT_DEFINITION);
    OConstraintDefinition loadedConstraintDefinition = null;
    if (constraintDefinitionDoc != null) {
      try {
        final String constraintDefClassName = config.field(OConstraintInternal.CONSTRAINT_DEFINITION_CLASS);
        final Class<?> constraintDefClass = Class.forName(constraintDefClassName);
        loadedConstraintDefinition = (OConstraintDefinition) constraintDefClass.getDeclaredConstructor().newInstance();
        loadedConstraintDefinition.fromStream(constraintDefinitionDoc);
      } catch (final ClassNotFoundException e) {
        throw OException.wrapException(new OConstraintException("Error during deserialization of constraint definition"), e);
      } catch (final NoSuchMethodException e) {
        throw OException.wrapException(new OConstraintException("Error during deserialization of constraint definition"), e);
      } catch (final InvocationTargetException e) {
        throw OException.wrapException(new OConstraintException("Error during deserialization of constraint definition"), e);
      } catch (final InstantiationException e) {
        throw OException.wrapException(new OConstraintException("Error during deserialization of constraint definition"), e);
      } catch (final IllegalAccessException e) {
        throw OException.wrapException(new OConstraintException("Error during deserialization of constraint definition"), e);
      }
    } else {
      final boolean isAutomatic = config.field(OConstraintInternal.CONFIG_AUTOMATIC);
      OConstraintFactory factory = OConstraints.getFactory(type, algorithm);
      if (Boolean.TRUE.equals(isAutomatic)) {
        final int pos = constraintName.lastIndexOf('.');
        if (pos < 0)
          throw new OConstraintException("Cannot convert from old constraint model to new one. "
              + "Invalid constraint name. Dot (.) separator should be present");

        final String className = constraintName.substring(0, pos);
        final String propertyName = constraintName.substring(pos + 1);

        final String keyTypeStr = config.field(OConstraintInternal.CONFIG_KEYTYPE);
        if (keyTypeStr == null)
          throw new OConstraintException("Cannot convert from old constraint to new one." + "Constraint key type is absent");

        final OType keyType = OType.valueOf(keyTypeStr.toUpperCase(Locale.ENGLISH));

        final OConstraintParameters params = config.field("params");
        
        loadedConstraintDefinition = new OPropertyConstraintDefinition(className, propertyName, keyType, params);

        config.removeField(OConstraintInternal.CONFIG_AUTOMATIC);
        config.removeField(OConstraintInternal.CONFIG_KEYTYPE);
      } else if (config.field(OConstraintInternal.CONFIG_KEYTYPE) != null) {
        final String keyTypeStr = config.field(OConstraintInternal.CONFIG_KEYTYPE);
        final OType keyType = OType.valueOf(keyTypeStr.toUpperCase(Locale.ENGLISH));

        loadedConstraintDefinition = new OSimpleKeyConstraintDefinition(factory.getLastVersion(), keyType);

        config.removeField(OConstraintInternal.CONFIG_KEYTYPE);
      }
    }

    final Set<String> clusters = new HashSet<>((Collection<String>) config.field(CONFIG_CLUSTERS, OType.EMBEDDEDSET));
    return new OConstraintMetadata(constraintName, loadedConstraintDefinition, clusters, type, algorithm, valueContainerAlgorithm);
  }
  
  public OConstraintDefinition getConstraintDefinition(){
    return constraintDefinition;
  }

  /**
   * Creates the constraint.
   *
   * @param clusterConstraintName
   *          Cluster name where to place the TreeMap
   * @param clustersToConstraint
   * @param rebuild
   * @param progressListener
   * @param valueSerializer
   */
  public OConstraint<?> create(final OConstraintDefinition constraintDefinition, final String clusterConstraintName,
      final Set<String> clustersToConstraint, boolean rebuild, final OProgressListener progressListener,
      final OBinarySerializer valueSerializer) {

    acquireExclusiveLock();
    try {
      configuration = constraintConfigurationInstace(new ODocument().setTrackingChanges(false));

      this.constraintDefinition = constraintDefinition;

      if (clustersToConstraint != null)
        this.clustersToConstraint = new HashSet<>(clustersToConstraint);
      else
        this.clustersToConstraint = new HashSet<>();

      // do not remove this, it is needed to remove constraint garbage if such one exists
      try {
        removeValuesContainer();
      } catch (Exception e) {
        OLogManager.instance().error(this, "Error during deletion of constraint '%s'", e, name);
      }

      final Boolean durableInNonTxMode = isDurableInNonTxMode();

      constraintId = storage.addConstraintEngine(name, algorithm, type, constraintDefinition, valueSerializer, isAutomatic(),
          durableInNonTxMode, version, getEngineProperties(), clustersToConstraint, metadata);

      assert constraintId >= 0;

      onConstraintEngineChange(constraintId);

      if (rebuild)
        fillConstraint(progressListener, false);

      updateConfiguration();

    } catch (Exception e) {
      OLogManager.instance().error(this, "Exception during constraint '%s' creation", e, name);

      while (true)
        try {
          if (constraintId >= 0)
            storage.deleteConstraintEngine(constraintId);
          break;
        } catch (OInvalidConstraintEngineIdException ignore) {
          doReloadConstraintEngine();
        } catch (Exception ex) {
          OLogManager.instance().error(this, "Exception during constraint '%s' deletion", ex, name);
        }

      if (e instanceof OConstraintException)
        throw (OConstraintException) e;

      throw OException.wrapException(new OConstraintException("Cannot create the constraint '" + name + "'"), e);

    } finally {
      releaseExclusiveLock();
    }

    return this;
  }

  @Override
  public boolean loadFromConfiguration(ODocument config) {
    acquireExclusiveLock();
    try {
      configuration = constraintConfigurationInstance(config);
      clustersToConstraint.clear();

      final OConstraintMetadata constraintMetadata = loadMetadata(config);
      constraintDefinition = constraintMetadata.getConstraintDefinition();
      clustersToConstraint.addAll(constraintMetadata.getClustersToConstraint());
      algorithm = constraintMetadata.getAlgorithm();
      valueContainerAlgorithm = constraintMetadata.getValueContainerAlgorithm();

      try {
        constraintId = storage.loadConstraintEngine(name);

        if (constraintId == -1) {
          constraintId = storage.loadExternalConstraintEngine(name, algorithm, type, constraintDefinition,
              determineValueSerializer(), isAutomatic(), isDurableInNonTxMode(), version, getEngineProperties());
        }

        if (constraintId == -1)
          return false;

        onConstraintEngineChange(constraintId);

      } catch (Exception e) {
        OLogManager.instance().error(this, "Error during load of constraint '%s'", e, name != null ? name : "null");

        if (isAutomatic()) {
          // AUTOMATIC REBUILD IT
          OLogManager.instance().warn(this, "Cannot load constraint '%s' rebuilt it from scratch", getName());
          try {
            rebuild();
          } catch (Exception t) {
            OLogManager.instance().error(this,
                "Cannot rebuild constraint '%s' because '" + t + "'. The constraint will be removed in configuration", e,
                getName());
            // REMOVE IT
            return false;
          }
        }
      }

      return true;
    } finally {
      releaseExclusiveLock();
    }
  }

  /**
   * {@inheritDoc}
   */
  public long rebuild() {
    return rebuild(new OConstraintRebuildOutputListener(this));
  }

  /**
   * {@inheritDoc}
   */
  public long rebuild(final OProgressListener iProgressListener) {
    long documentConstrained = 0;

    final boolean intentInstalled = getDatabase().declareIntent(new OIntentMassiveInsert());

    acquireExclusiveLock();
    try {
      // DO NOT REORDER 2 assignments bellow
      // see #getRebuildVersion()
      rebuilding = true;
      rebuildVersion.incrementAndGet();

      try {
        if (constraintId >= 0)
          storage.deleteConstraintEngine(constraintId);
      } catch (Exception e) {
        OLogManager.instance().error(this, "Error during constraint '%s' delete", e, name);
      }

      removeValuesContainer();

      constraintId = storage.addConstraintEngine(name, algorithm, type, constraintDefinition, determineValueSerializer(),
          isAutomatic(), isDurableInNonTxMode(), version, getEngineProperties(), clustersToConstraint, metadata);

      onConstraintEngineChange(constraintId);
    } catch (Exception e) {
      try {
        if (constraintId >= 0)
          storage.clearConstraint(constraintId);
      } catch (Exception e2) {
        OLogManager.instance().error(this, "Error during constraint rebuild", e2);
        // IGNORE EXCEPTION: IF THE REBUILD WAS LAUNCHED IN CASE OF RID INVALID CLEAR ALWAYS GOES IN ERROR
      }

      rebuilding = false;
      throw OException
          .wrapException(new OConstraintException("Error on rebuilding the constraint for clusters: " + clustersToConstraint), e);
    } finally {
      releaseExclusiveLock();
    }

    acquireSharedLock();
    try {
      documentConstrained = fillConstraint(iProgressListener, true);
    } catch (final Exception e) {
      OLogManager.instance().error(this, "Error during constraint rebuild", e);
      try {
        if (constraintId >= 0)
          storage.clearConstraint(constraintId);
      } catch (Exception e2) {
        OLogManager.instance().error(this, "Error during constraint rebuild", e2);
        // IGNORE EXCEPTION: IF THE REBUILD WAS LAUNCHED IN CASE OF RID INVALID CLEAR ALWAYS GOES IN ERROR
      }

      throw OException
          .wrapException(new OConstraintException("Error on rebuilding the constraint for clusters: " + clustersToConstraint), e);
    } finally {
      rebuilding = false;

      if (intentInstalled)
        getDatabase().declareIntent(null);

      releaseSharedLock();
    }

    return documentConstrained;
  }

  // TODO - Fabio implementar em OConstraintOneValue e OConstraintMultiValues (criar classes)
  protected abstract OBinarySerializer determineValueSerializer();

  protected ConstraintConfiguration constraintConfigurationInstance(final ODocument document) {
    return new ConstraintConfiguration(document);
  }

  private long fillConstraint(final OProgressListener iProgressListener, final boolean rebuild) {
    long documentConstrained = 0;
    try {
      long documentNum = 0;
      long documentTotal = 0;

      for (final String cluster : clustersToConstraint)
        documentTotal += storage.count(storage.getClusterIdByName(cluster));

      if (iProgressListener != null)
        iProgressListener.onBegin(this, documentTotal, rebuild);

      // CONSTRAINT ALL CLUSTERS
      for (final String clusterName : clustersToConstraint) {
        final long[] metrics = constraintCluster(clusterName, iProgressListener, documentNum, documentConstrained, documentTotal);
        documentNum = metrics[0];
        documentConstrained = metrics[1];
      }

      if (iProgressListener != null)
        iProgressListener.onCompletition(this, true);
    } catch (final RuntimeException e) {
      if (iProgressListener != null)
        iProgressListener.onCompletition(this, false);
      throw e;
    }
    return documentConstrained;
  }

  protected long[] constraintCluster(final String clusterName, final OProgressListener iProgressListener, long documentNum,
      long documentConstrained, long documentTotal) {
    try {
      for (final ORecord record : getDatabase().browseCluster(clusterName)) {
        if (Thread.interrupted())
          throw new OCommandExecutionException("The constraint rebuild has been interrupted");

        if (record instanceof ODocument) {
          final ODocument doc = (ODocument) record;

          if (constraintDefinition == null)
            throw new OConfigurationException(
                "Constraint '" + name + "' cannot be rebuilt because has no a valid definition (" + constraintDefinition + ")");

          final Object fieldValue = constraintDefinition.getDocumentValueToConstraint(doc);

          if (fieldValue != null || !constraintDefinition.isNullValuesIgnored()) {
            try {
              populateConstraint(doc, fieldValue);
            } catch (OTooBigConstraintKeyException e) {
              OLogManager.instance().error(this,
                  "Exception during constraint rebuild. Exception was caused by following key/ value pair - key %s, value %s."
                      + " Rebuild will continue from this point",
                  e, fieldValue, doc.getIdentity());
            } catch (OConstraintException e) {
              OLogManager.instance().error(this,
                  "Exception during constraint rebuild. Exception was caused by following key/ value pair - key %s, value %s."
                      + " Rebuild will continue from this point",
                  e, fieldValue, doc.getIdentity());
            }

            ++documentConstrained;
          }
        }
        documentNum++;

        if (iProgressListener != null)
          iProgressListener.onProgress(this, documentNum, (float) (documentNum * 100.0 / documentTotal));
      }
    } catch (NoSuchElementException ignore) {
      // END OF CLUSTER REACHED, IGNORE IT
    }

    return new long[] { documentNum, documentConstrained };
  }

  protected void populateConstraint(ODocument doc, Object fieldValue) {
    Map<String, List<OConstraintDto>> fieldsMap = new HashMap<>();
    List<OConstraintDto> list = new ArrayList<>();
    list.add(new OConstraintDto(doc.getIdentity(), doc.getClassName(), doc.getFieldsMap()));
    fieldsMap.put(doc.getClassName(),list);
    
    if (fieldValue instanceof Collection) {
      for (final Object fieldValueItem : (Collection<?>) fieldValue) {
        put(fieldValueItem, doc, fieldsMap);
      }
    } else
      put(fieldValue, doc, fieldsMap);
  }

  public Object getCollatingValue(final Object key) {
    if (key != null && getDefinition() != null)
      return getDefinition().getCollate().transform(key);
    return key;
  }

  protected ODatabaseDocumentInternal getDatabase() {
    return ODatabaseRecordThreadLocal.instance().get();
  }

  protected void onConstraintEngineChange(final int constraintId) {
    while (true)
      try {
        storage.callConstraintEngine(false, false, constraintId, engine -> {
          engine.init(getName(), getType(), getDefinition(), isAutomatic(), getMetadata());
          return null;
        });
        break;
      } catch (OInvalidConstraintEngineIdException ignore) {
        doReloadConstraintEngine();
      }
  }

  protected void doReloadConstraintEngine() {
    constraintId = storage.loadConstraintEngine(name);

    if (constraintId < 0) {
      throw new IllegalStateException("Constraint " + name + " can not be loaded");
    }
  }

  @Override
  public boolean isAutomatic() {
    acquireSharedLock();
    try {
      return constraintDefinition != null && constraintDefinition.getClassName() != null;
    } finally {
      releaseSharedLock();
    }
  }

  protected Map<String, String> getEngineProperties() {
    return engineProperties;
  }

  @Override
  public OConstraintMetadata loadMetadata(ODocument config) {
    return loadMetadataInternal(config, type, algorithm, valueContainerAlgorithm);
  }

  protected ConstraintConfiguration constraintConfigurationInstace(final ODocument document) {
    return new ConstraintConfiguration(document);
  }

  private void removeValuesContainer() {
    if (valueContainerAlgorithm.equals(ODefaultConstraintFactory.SBTREEBONSAI_VALUE_CONTAINER)) {

      final OAtomicOperation atomicOperation = storage.getAtomicOperationsManager().getCurrentOperation();

      final OReadCache readCache = storage.getReadCache();
      final OWriteCache writeCache = storage.getWriteCache();

      if (atomicOperation == null) {
        try {
          final String fileName = getName() + OConstraintRIDContainer.CONSTRAINT_FILE_EXTENSION;
          if (writeCache.exists(fileName)) {
            final long fileId = writeCache.loadFile(fileName);
            readCache.deleteFile(fileId, writeCache);
          }
        } catch (IOException e) {
          OLogManager.instance().error(this, "Cannot delete file for value containers", e);
        }
      } else {
        try {
          final String fileName = getName() + OConstraintRIDContainer.CONSTRAINT_FILE_EXTENSION;
          if (atomicOperation.isFileExists(fileName)) {
            final long fileId = atomicOperation.loadFile(fileName);
            atomicOperation.deleteFile(fileId);
          }
        } catch (IOException e) {
          OLogManager.instance().error(this, "Cannot delete file for value containers", e);
        }
      }
    }
  }

  private Boolean isDurableInNonTxMode() {
    Boolean durableInNonTxMode;

    Object durable = null;

    if (metadata != null) {
      durable = metadata.field("durableInNonTxMode");
    }

    if (durable instanceof Boolean)
      durableInNonTxMode = (Boolean) durable;
    else
      durableInNonTxMode = null;
    return durableInNonTxMode;
  }

  @Override
  public String getDatabaseName() {
    return databaseName;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getType() {
    return type;
  }

  @Override
  public ODocument getMetadata() {
    return metadata;
  }

  @Override
  public int getConstraintId() {
    return constraintId;
  }

  @Override
  public ODocument updateConfiguration() {
    configuration.updateConfiguration(type, name, version, constraintDefinition, clustersToConstraint, algorithm,
        valueContainerAlgorithm);
    if (metadata != null)
      configuration.document.field(OConstraintInternal.METADATA, metadata, OType.EMBEDDED);
    return configuration.getDocument();
  }

  @Override
  public ODocument getConfiguration() {
    return configuration.getDocument();
  }

  @Override
  public OConstraintInternal<T> delete() {
    acquireExclusiveLock();

    try {
      while (true)
        try {
          storage.deleteConstraintEngine(constraintId);
          break;
        } catch (OInvalidConstraintEngineIdException ignore) {
          doReloadConstraintEngine();
        }

      // REMOVE THE CONSTRAINT ALSO FROM CLASS MAP
      if (getDatabase().getMetadata() != null)
        getDatabase().getMetadata().getConstraintManager().removeClassPropertyConstraint(this);

      removeValuesContainer();
      return this;
    } finally {
      releaseExclusiveLock();
    }
  }

  @Override
  public OConstraintInternal<T> getInternal() {
    return this;
  }

  public Set<String> getClusters() {
    acquireSharedLock();
    try {
      return Collections.unmodifiableSet(clustersToConstraint);
    } finally {
      releaseSharedLock();
    }
  }

  @Override
  public OConstraintDefinition getDefinition() {
    return constraintDefinition;
  }

  @Override
  public boolean equals(final Object o) {
    acquireSharedLock();
    try {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;

      final OConstraintAbstract<?> that = (OConstraintAbstract<?>) o;

      if (!name.equals(that.name))
        return false;

      return true;
    } finally {
      releaseSharedLock();
    }
  }

  @Override
  public int hashCode() {
    acquireSharedLock();
    try {
      return name.hashCode();
    } finally {
      releaseSharedLock();
    }
  }

  @Override
  public int compareTo(OConstraint<T> constraint) {
    acquireSharedLock();
    try {
      final String name = constraint.getName();
      return this.name.compareTo(name);
    } finally {
      releaseSharedLock();
    }
  }

  protected void releaseExclusiveLock() {
    rwLock.releaseWriteLock();
  }

  protected void acquireExclusiveLock() {
    rwLock.acquireWriteLock();
  }

  protected void releaseSharedLock() {
    rwLock.releaseReadLock();
  }

  protected void acquireSharedLock() {
    rwLock.acquireReadLock();
  }

  @Override
  public String getConstraintNameByKey(final Object key) {
    OConstraintEngine engine;
    while (true)
      try {
        engine = storage.getConstraintEngine(constraintId);
        break;
      } catch (OInvalidConstraintEngineIdException ignore) {
        doReloadConstraintEngine();
      }
    return engine.getConstraintNameByKey(key);
  }

  @Override
  public boolean acquireAtomicExclusiveLock(Object key) {
    OConstraintEngine engine;
    while (true)
      try {
        engine = storage.getConstraintEngine(constraintId);
        break;
      } catch (OInvalidConstraintEngineIdException ignore) {
        doReloadConstraintEngine();
      }

    return engine.acquireAtomicExclusiveLock(key);
  }
  
  public static final class ConstraintTxSnapshot {
    public Map<Object, Object> constraintSnapshot = new HashMap<Object, Object>();
    public boolean             clear         = false;
  }
  
  @Override
  public void preCommit(ConstraintTxSnapshot snapshots) {
  }

  @Override
  public void postCommit(ConstraintTxSnapshot snapshots) {
  }
  
  @Override
  public void addTxOperation(ConstraintTxSnapshot snapshots, OTransactionConstraintChanges changes, Map<String, List<OConstraintDto>> fields) {
    acquireSharedLock();
    try {
      if (changes.cleared)
        clearSnapshot(snapshots);
      final Map<Object, Object> snapshot = snapshots.constraintSnapshot;
      for (final OTransactionConstraintChangesPerKey entry : changes.changesPerKey.values()) {
        applyConstraintTxEntry(snapshot, entry, fields);
      }
      applyConstraintTxEntry(snapshot, changes.nullKeyChanges, fields);

    } finally {
      releaseSharedLock();
    }
  }
  
  protected void clearSnapshot(ConstraintTxSnapshot constraintTxSnapshot) {
    // storage will delay real operations till the end of tx
    clear();
  }
  
  /**
   * Interprets transaction constraint changes for a certain key. Override it to customize constraint behaviour on interpreting constraint changes.
   * This may be viewed as an optimization, but in some cases this is a requirement. For example, if you put multiple values under
   * the same key during the transaction for single-valued/unique constraint, but remove all of them except one before commit, there is
   * no point in throwing {@link com.orientechnologies.orient.core.storage.ORecordDuplicatedException} while applying constraint changes.
   *
   * @param changes the changes to interpret.
   *
   * @return the interpreted constraint key changes.
   */
  protected Iterable<OTransactionConstraintChangesPerKey.OTransactionConstraintEntry> interpretTxKeyChanges(
      OTransactionConstraintChangesPerKey changes) {
    return changes.entries;
  }
  
  private void applyConstraintTxEntry(Map<Object, Object> snapshot, OTransactionConstraintChangesPerKey entry, Map<String, List<OConstraintDto>> fields) {
    for (OTransactionConstraintChangesPerKey.OTransactionConstraintEntry op : interpretTxKeyChanges(entry)) {
      switch (op.operation) {
      case PUT:
        putInSnapshot(entry.key, op.value, snapshot, fields);
        break;
      case REMOVE:
        if (op.value != null)
          removeFromSnapshot(entry.key, op.value, snapshot);
        else
          removeFromSnapshot(entry.key, snapshot);
        break;
      case CLEAR:
        // SHOULD NEVER BE THE CASE HANDLE BY cleared FLAG
        break;
      }
    }
  }
  
  protected void putInSnapshot(Object key, OIdentifiable value, Map<Object, Object> snapshot,  Map<String, List<OConstraintDto>> fields) {
    // storage will delay real operations till the end of tx
    put(key, value, fields);
  }
  
  protected void removeFromSnapshot(Object key, OIdentifiable value, Map<Object, Object> snapshot) {
    // storage will delay real operations till the end of tx
    remove(key, value);
  }
  
  protected void removeFromSnapshot(Object key, Map<Object, Object> snapshot) {
    // storage will delay real operations till the end of tx
    remove(key);
  }
  
  public boolean remove(Object key, final OIdentifiable value) {
    return remove(key);
  }
  
  public boolean remove(Object key) {
    key = getCollatingValue(key);

    acquireSharedLock();
    try {
      while (true)
        try {
          return storage.removeKeyFromConstraint(constraintId, key);
        } catch (OInvalidConstraintEngineIdException ignore) {
          doReloadConstraintEngine();
        }
    } finally {
      releaseSharedLock();
    }
  }
  
  @Override
  public void commit(ConstraintTxSnapshot snapshots) {
    acquireSharedLock();
    try {
      if (snapshots.clear)
        clear();

      commitSnapshot(snapshots.constraintSnapshot);
    } finally {
      releaseSharedLock();
    } 
  }
  
  protected void commitSnapshot(Map<Object, Object> snapshot) {
    // do nothing by default
    // storage will delay real operations till the end of tx
  }
  
  public OConstraint<T> clear() {
    acquireSharedLock();
    try {
      while (true)
        try {
          storage.clearConstraint(constraintId);
          break;
        } catch (OInvalidConstraintEngineIdException ignore) {
          doReloadConstraintEngine();
        }
      return this;
    } finally {
      releaseSharedLock();
    }
  }

  protected static class ConstraintConfiguration {
    protected final ODocument document;

    public ConstraintConfiguration(ODocument document) {
      this.document = document;
    }

    public ODocument getDocument() {
      return document;
    }

    public synchronized ODocument updateConfiguration(String type, String name, int version,
        OConstraintDefinition constraintDefinition, Set<String> clustersToConstraint, String algorithm,
        String valueContainerAlgorithm) {

      document.field(OConstraintInternal.CONFIG_TYPE, type);
      document.field(OConstraintInternal.CONFIG_NAME, name);
      document.field(OConstraintInternal.CONSTRAINT_VERSION, version);

      if (constraintDefinition != null) {

        final ODocument constraintDefDocument = constraintDefinition.toStream();

        if (!constraintDefDocument.hasOwners())
          ODocumentInternal.addOwner(constraintDefDocument, document);

        document.field(OConstraintInternal.CONSTRAINT_DEFINITION, constraintDefDocument, OType.EMBEDDED);
        document.field(OConstraintInternal.CONSTRAINT_DEFINITION_CLASS, constraintDefinition.getClass().getName());
      } else {
        document.removeField(OConstraintInternal.CONSTRAINT_DEFINITION);
        document.removeField(OConstraintInternal.CONSTRAINT_DEFINITION_CLASS);
      }

      document.field(CONFIG_CLUSTERS, clustersToConstraint, OType.EMBEDDEDSET);
      document.field(ALGORITHM, algorithm);
      document.field(OConstraintInternal.VALUE_CONTAINER_ALGORITHM, valueContainerAlgorithm);

      return document;
    }

  }
  
  @Override
  public void setType(OType type) {
    constraintDefinition = new OSimpleKeyConstraintDefinition(version, type);
    updateConfiguration();
  }
}
