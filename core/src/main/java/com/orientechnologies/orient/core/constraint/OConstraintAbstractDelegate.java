package com.orientechnologies.orient.core.constraint;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.orientechnologies.common.listener.OProgressListener;
import com.orientechnologies.orient.core.db.ODatabaseRecordThreadLocal;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ODocumentEntry;

/**
 * Generic abstract wrapper. It delegates all the operations to the wrapped OConstraint instance.
 * 
 * @author fabio
 *
 * @param <T>
 */
public class OConstraintAbstractDelegate<T> implements OConstraint<T> {
  
  protected OConstraint<T> delegate;
  
  public OConstraintAbstractDelegate(final OConstraint<T> delegate) {
    this.delegate = delegate;
  }

  @Override
  public int compareTo(OConstraint<T> o) {
    return delegate.compareTo(o);
  }

  @Override
  public OConstraint<T> create(final String name, final OConstraintDefinition constraintDefinition, final String clusterConstraintName,
      final Set<String> clusterToConstraint, boolean rebuild, final OProgressListener progressListener) {
    return delegate.create(name, constraintDefinition, clusterConstraintName, clusterToConstraint, rebuild, progressListener);
  }

  @Override
  public String getDatabaseName() {
    return delegate.getDatabaseName();
  }

  @Override
  public T get(Object iKey) {
    return delegate.get(iKey);
  }

  @Override
  public long getSize() {
    return delegate.getSize();
  }

  @Override
  public long getKeySize() {
    return delegate.getKeySize();
  }

  @Override
  public OConstraint<T> delete() {
    return delegate.delete();
  }

  @Override
  public String getName() {
    return delegate.getName();
  }

  @Override
  public String getType() {
    return delegate.getType();
  }

  @Override
  public ODocument getMetadata() {
    return delegate.getMetadata();
  }

  @Override
  public OConstraint<T> put(Object iKey, OIdentifiable iValue, Map<String, List<OConstraintDto>> fields) {
    return delegate.put(iKey, iValue, fields);
  }

  @Override
  public boolean remove(Object key) {
    return delegate.remove(key);
  }

  @Override
  public boolean remove(Object iKey, OIdentifiable iRID) {
    return delegate.remove(iKey, iRID);
  }

  @Override
  public OConstraint<T> clear() {
    return delegate.clear();
  }

  @Override
  public boolean isAutomatic() {
    return delegate.isAutomatic();
  }

  @Override
  public ODocument getConfiguration() {
    return delegate.getConfiguration();
  }

  @SuppressWarnings("unchecked")
  @Override
  public OConstraintInternal<T> getInternal() {
    OConstraint<?>  internal = this.delegate;
    
    while (!(internal instanceof OConstraintInternal) && internal != null)
      internal = internal.getInternal();
    
    return (OConstraintInternal<T>) internal;
  }

  @Override
  public OConstraintDefinition getDefinition() {
    return delegate.getDefinition();
  }

  @Override
  public Set<String> getClusters() {
    return delegate.getClusters();
  }

  @Override
  public boolean supportsOrderedIterations() {
    return delegate.supportsOrderedIterations();
  }

  @Override
  public int getConstraintId() {
    return delegate.getConstraintId();
  }

  @Override
  public boolean isUnique() {
    return delegate.isUnique();
  }

  protected void checkForKeyType(final Object iKey) {
    if (delegate.getDefinition() == null) {
      // RECOGNIZE THE KEY TYPE AT RUN-TIME

      final OType type = OType.getTypeByClass(iKey.getClass());
      if (type == null)
        return;

      OConstraintManager constraintManager = ODatabaseRecordThreadLocal.instance().get().getMetadata().getConstraintManager();
      getInternal().setType(type);
      constraintManager.save();
    }
  }
  
  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    final OConstraintAbstractDelegate<?> that = (OConstraintAbstractDelegate<?>) o;

    if (!delegate.equals(that.delegate))
      return false;

    return true;
  }

  @Override
  public int hashCode() {
    return delegate.hashCode();
  }
  
  @Override
  public String toString() {
    return delegate.toString();
  }
}
