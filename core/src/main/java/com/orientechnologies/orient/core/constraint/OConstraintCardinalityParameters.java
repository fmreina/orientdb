package com.orientechnologies.orient.core.constraint;

import java.io.Serializable;

import com.orientechnologies.orient.core.sql.parser.OExpression;
import com.orientechnologies.orient.core.sql.parser.OIdentifier;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintCardinalityParameters implements OConstraintParameters, Serializable {

  private static final long serialVersionUID = -8510537178146339047L;

  public OIdentifier originClass;
  public OIdentifier edgeType;
  public OIdentifier targetClass;
  public OExpression lowerLimit;
  public OExpression upperLimit;

  public OConstraintCardinalityParameters() {
  }

  public OConstraintCardinalityParameters(OIdentifier originClass, OIdentifier edgeType, OExpression lowerLimit, OExpression upperLimit, OIdentifier targetClass) {
    super();
    this.originClass = originClass;
    this.edgeType = edgeType;
    this.lowerLimit = lowerLimit;
    this.upperLimit = upperLimit;
    this.targetClass = targetClass;
  }

  public OConstraintCardinalityParameters(String params) {
    strToParam(params);
  }

  @Override
  public OConstraintParameters strToParam(String str) {
    // FIXME
    // TODO - FABIO: fazer a conversão dos valores de string para os artibutos desse objeto para retornar um objeto desse tipo
    String[] props = str.split(" ");
    
    this.originClass = new OIdentifier(props[1]);
    
    this.edgeType = new OIdentifier(props[2]);
    
    this.lowerLimit = new OExpression(new OIdentifier(props[3]));
    
    this.upperLimit = new OExpression(new OIdentifier(props[5]));
    
    this.targetClass = new OIdentifier(props[6]);

    return this;
  }

  @Override
  public String toString() {
    return "[ " + originClass + " " + edgeType + " " + lowerLimit + " : " + upperLimit + " " + targetClass + " ]";
  }

  public OExpression getLowerLimit() {
    return lowerLimit;
  }

  public OExpression getUpperLimit() {
    return upperLimit;
  }
  
  public OIdentifier getOriginClass() {
    return originClass;
  }

  public OIdentifier getEdgeType() {
    return edgeType;
  }

  public OIdentifier getTargetClass() {
    return targetClass;
  }

}
