package com.orientechnologies.orient.core.constraint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.orientechnologies.common.exception.OException;
import com.orientechnologies.orient.core.db.record.OMultiValueChangeEvent;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;

public class OPropertyListConstraintDefinition extends OAbstractConstraintDefinitionMultiValue {

  private static final long serialVersionUID = -1142323079782152766L;

  public OPropertyListConstraintDefinition(final String iClassName, final String iField, final OType iType) {
    super(iClassName, iField, iType);
  }

  public OPropertyListConstraintDefinition() {
  }

  @Override
  public Object getDocumentValueToConstraint(ODocument iDocument) {
    return createValue(iDocument.<Object> field(field));
  }

  @Override
  public Object createValue(List<?> params) {
    if (!(params.get(0) instanceof Collection))
      params = (List) Collections.singletonList(params);

    final Collection<?> multiValueCollection = (Collection<?>) params.get(0);
    final List<Object> values = new ArrayList<>(multiValueCollection.size());
    for (final Object item : multiValueCollection) {
      values.add(createSingleValue(item));
    }
    return values;
  }

  @Override
  public Object createValue(final Object... params) {
    Object param = params[0];
    if (!(param instanceof Collection)) {
      try {
        return OType.convert(param, keyType.getDefaultJavaType());
      } catch (Exception e) {
        return null;
      }
    }

    final Collection<?> multiValueCollection = (Collection<?>) param;
    final List<Object> values = new ArrayList<>(multiValueCollection.size());
    for (final Object item : multiValueCollection) {
      values.add(createSingleValue(item));
    }
    return values;
  }

  @Override
  public Object createSingleValue(Object... param) {
    try {
      return OType.convert(param[0], keyType.getDefaultJavaType());
    } catch (Exception e) {
      OException ex = OException.wrapException(
          new OConstraintException("Invalid key for constraint: " + param[0] + " cannot be converted to " + keyType), e);
      throw ex;
    }
  }

  public void processChangeEvent(final OMultiValueChangeEvent<?, ?> changeEvent, final Map<Object, Integer> keysToAdd,
      final Map<Object, Integer> keysToRemove) {
    switch (changeEvent.getChangeType()) {
    case ADD: {
      processAdd(createSingleValue(changeEvent.getValue()), keysToAdd, keysToRemove);
      break;
    }
    case REMOVE: {
      processRemoval(createSingleValue(changeEvent.getOldValue()), keysToAdd, keysToRemove);
      break;
    }
    case UPDATE: {
      processRemoval(createSingleValue(changeEvent.getOldValue()), keysToAdd, keysToRemove);
      processAdd(createSingleValue(changeEvent.getValue()), keysToAdd, keysToRemove);
      break;
    }
    default:
      throw new IllegalArgumentException("Invalid change type : " + changeEvent.getChangeType());
    }
  }

  @Override
  public String toCreateConstraintDDL(String constraintName, String constraintType, String engine) {
    return createConstraintDDLWithoutFieldType(constraintName, constraintType, engine).toString();
  }

}
