package com.orientechnologies.orient.core.constraint;

import static com.orientechnologies.orient.core.metadata.schema.OType.ANY;
import static com.orientechnologies.orient.core.metadata.schema.OType.EMBEDDEDLIST;
import static com.orientechnologies.orient.core.metadata.schema.OType.EMBEDDEDMAP;
import static com.orientechnologies.orient.core.metadata.schema.OType.EMBEDDEDSET;
import static com.orientechnologies.orient.core.metadata.schema.OType.LINK;
import static com.orientechnologies.orient.core.metadata.schema.OType.LINKBAG;
import static com.orientechnologies.orient.core.metadata.schema.OType.LINKLIST;
import static com.orientechnologies.orient.core.metadata.schema.OType.LINKMAP;
import static com.orientechnologies.orient.core.metadata.schema.OType.LINKSET;

import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import com.orientechnologies.orient.core.collate.OCollate;
import com.orientechnologies.orient.core.config.OStorageConfiguration;
import com.orientechnologies.orient.core.db.ODatabaseDocumentInternal;
import com.orientechnologies.orient.core.db.ODatabaseRecordThreadLocal;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OClassImpl;
import com.orientechnologies.orient.core.metadata.schema.OProperty;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.storage.OStorage;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintDefinitionFactory {
  private static final Pattern FILED_NAME_PATTERN = Pattern.compile("\\s+");

  /**
   * Creates an instance of {@link OConstraintDefinition} for automatic constraint.
   *
   * @param oClass
   *          class which will be constrained
   * @param fieldNames
   *          list of properties which will be constrained. Format should be '<property> [by key|value]', use 'by key' or 'by value'
   *          to describe how to constraint maps. By default maps constrained by key
   * @param types
   *          types of constrained properties
   * @param collates
   * @param constraintKind
   * @param algorithm
   * @return constraint definition instance
   */
  public static OConstraintDefinition createConstraintDefinition(final OClass oClass, final List<String> fieldNames,
      final List<OType> types, List<OCollate> collates, String constraintKind, String algorithm, OConstraintParameters params) {
    checkTypes(oClass, fieldNames, types);

    if (fieldNames.size() == 0)
      return createSingleFieldConstraintDefinition(oClass, "", ANY,
          collates == null ? null : collates.get(0), constraintKind, algorithm, params);
    else if (fieldNames.size() == 1)
      return createSingleFieldConstraintDefinition(oClass, fieldNames.get(0), types.get(0),
          collates == null ? null : collates.get(0), constraintKind, algorithm, params);
    else
      return createMultipleFieldConstraintDefinition(oClass, fieldNames, types, collates, constraintKind, algorithm, params);
  }

  /**
   * Extract field name from '<property> [by key|value]' field format.
   *
   * @param fieldDefinition
   *          definition of field
   * @return extracted property name
   */
  public static String extractFieldName(final String fieldDefinition) {
    String[] fieldNameParts = FILED_NAME_PATTERN.split(fieldDefinition);
    if (fieldNameParts.length == 1)
      return fieldDefinition;
    if (fieldNameParts.length == 3 && "by".equalsIgnoreCase(fieldNameParts[1]))
      return fieldNameParts[0];

    throw new IllegalArgumentException(
        "Illegal field name format, should be '<property> [by key|value]' but was '" + fieldDefinition + '\'');
  }

  private static void checkTypes(OClass oClass, List<String> fieldNames, List<OType> types) {
    if (fieldNames.size() != types.size())
      throw new IllegalArgumentException("Count of field names doesn't match count of field types. It was " + fieldNames.size()
          + " fields, but " + types.size() + " types.");

    for (int i = 0, fieldNamesSize = fieldNames.size(); i < fieldNamesSize; i++) {
      String fieldName = fieldNames.get(i);
      OType type = types.get(i);

      final OProperty property = oClass.getProperty(fieldName);
      if (property != null && !type.equals(property.getType())) {
        throw new IllegalArgumentException("Property type list not match with real property types");
      }
    }
  }

  private static OConstraintDefinition createMultipleFieldConstraintDefinition(final OClass oClass,
      final List<String> fieldsToConstraint, final List<OType> types, List<OCollate> collates, String constraintKind,
      String algorithm, OConstraintParameters params) {
    final OConstraintFactory factory = OConstraints.getFactory(constraintKind, algorithm);
    final String className = oClass.getName();
    final OCompositeConstraintDefinition compositeConstraint = new OCompositeConstraintDefinition(className);

    for (int i = 0, fieldsToConstraintSize = fieldsToConstraint.size(); i < fieldsToConstraintSize; i++) {
      OCollate collate = null;
      if (collates != null)
        collate = collates.get(i);

      compositeConstraint.addConstraint(createSingleFieldConstraintDefinition(oClass, fieldsToConstraint.get(i), types.get(i),
          collate, constraintKind, algorithm, params));
    }

    return compositeConstraint;

  }

  private static OConstraintDefinition createSingleFieldConstraintDefinition(OClass oClass, final String field, final OType type,
      OCollate collate, String constraintKind, String algorithm, OConstraintParameters params) {

    final String fieldName = OClassImpl.decodeClassName(adjustFieldName(oClass, extractFieldName(field)));
    final OConstraintDefinition constraintDefinition;

    final OProperty propertyToConstraint = oClass.getProperty(fieldName);
    final OType constraintType;
    if (EMBEDDEDMAP.equals(type) || LINKMAP.equals(type)) {
      final OPropertyMapConstraintDefinition.CONSTRAINT_BY constraintBy = extractMapConstraintSpecifier(field);

      if (constraintBy.equals(OPropertyMapConstraintDefinition.CONSTRAINT_BY.KEY))
        constraintType = OType.STRING;
      else {
        if (LINKMAP.equals(type))
          constraintType = OType.LINK;
        else {
          constraintType = propertyToConstraint.getLinkedType();
          if (constraintType == null)
            throw new OConstraintException("Linked type was not provided."
                + " You should provide linked type for embedded collections that are going to be constrained.");
        }

      }

      constraintDefinition = new OPropertyMapConstraintDefinition(oClass.getName(), fieldName, constraintType, constraintBy);
    } else if (EMBEDDEDLIST.equals(type) || EMBEDDEDSET.equals(type) || LINKLIST.equals(type)
        || LINKSET.equals(type)) {
      if (LINKSET.equals(type))
        constraintType = LINK;
      else if (LINKLIST.equals(type)) {
        constraintType = LINK;
      } else {
        constraintType = propertyToConstraint.getLinkedType();
        if (constraintType == null)
          throw new OConstraintException("Linked type was not provided."
              + " You should provide linked type for embedded collections that are going to be constrained.");
      }

      constraintDefinition = new OPropertyListConstraintDefinition(oClass.getName(), fieldName, constraintType);
    } else if (LINKBAG.equals(type)) {
      constraintDefinition = new OPropertyRidBagConstraintDefinition(oClass.getName(), fieldName);
    } else
      constraintDefinition = new OPropertyConstraintDefinition(oClass.getName(), fieldName, type, params);

    if (collate == null && propertyToConstraint != null)
      collate = propertyToConstraint.getCollate();

    if (collate != null)
      constraintDefinition.setCollate(collate);

    return constraintDefinition;
  }

  private static OPropertyMapConstraintDefinition.CONSTRAINT_BY extractMapConstraintSpecifier(final String fieldName) {

    String[] fieldNameParts = FILED_NAME_PATTERN.split(fieldName);
    if (fieldNameParts.length == 1)
      return OPropertyMapConstraintDefinition.CONSTRAINT_BY.KEY;

    if (fieldNameParts.length == 3) {
      Locale locale = getServerLocale();

      if ("by".equals(fieldNameParts[1].toLowerCase(locale)))
        try {
          return OPropertyMapConstraintDefinition.CONSTRAINT_BY.valueOf(fieldNameParts[2].toUpperCase(locale));
        } catch (IllegalArgumentException iae) {
          throw new IllegalArgumentException(
              "Illegal field name format, should be '<property> [by key|value]' but was '" + fieldName + '\'', iae);
        }
    }

    throw new IllegalArgumentException(
        "Illegal field name format, should be '<property> [by key|value]' but was '" + fieldName + '\'');
  }

  private static Locale getServerLocale() {
    ODatabaseDocumentInternal db = ODatabaseRecordThreadLocal.instance().get();
    OStorage storage = db.getStorage();
    OStorageConfiguration configuration = storage.getConfiguration();
    return configuration.getLocaleInstance();
  }

  private static String adjustFieldName(final OClass clazz, final String fieldName) {
    final OProperty property = clazz.getProperty(fieldName);

    if (property != null)
      return property.getName();
    else
      return fieldName;
  }
}
