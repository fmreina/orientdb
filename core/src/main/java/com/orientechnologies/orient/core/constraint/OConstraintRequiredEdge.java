package com.orientechnologies.orient.core.constraint;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.ietf.jgss.Oid;

import com.orientechnologies.orient.core.constraint.OConstraintDto.OOperation;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.db.record.ridbag.ORidBag;
import com.orientechnologies.orient.core.exception.OInvalidConstraintEngineIdException;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ODocumentEntry;
import com.orientechnologies.orient.core.storage.OConstraintValidationException;
import com.orientechnologies.orient.core.storage.impl.local.OAbstractPaginatedStorage;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintRequiredEdge extends OConstraintOneValue {

  private final OConstraintEngine.Validator<Object, OIdentifiable> CONSTRAINT_VALIDATOR = new OConstraintEngine.Validator<Object, OIdentifiable>() {
    @Override
    public Object validate(Object key, OIdentifiable oldValue, OIdentifiable newValue, Map<String, List<OConstraintDto>> fields,
        String params) {

      // System.out.println("\nValidation of the REQUIRED_EDGE constraint.");
      // System.out.println("Params: " + params);
      // System.out.println("Entered Values:");
      // System.out.println("oldValue: " + oldValue);
      // System.out.println("newValue: " + newValue);
      // fields.keySet().forEach(k -> {
      // String cName = k;
      // System.out.println(cName);
      // List<OConstraintDto> listDto = fields.get(k);
      // listDto.forEach(dto -> {
      // Map<String, ODocumentEntry> m = dto.getAttributes();
      // m.keySet().forEach(s -> {
      // String attribute = s;
      // System.out.print("\t");
      // System.out.print(attribute);
      // System.out.print(" = ");
      // ODocumentEntry doc = m.get(s);
      // System.out.println(doc.value);
      // });
      // System.out.println();
      // });
      // });

      OConstraintRequiredEdgeParameters reqParams = new OConstraintRequiredEdgeParameters(params);

      String outParam = reqParams.getOriginClass().getValue();
      String edgeParam = reqParams.getEdgeType().getValue();
      String inParam = reqParams.getTargetClass().getValue();

      // gets the list of fields related to the edge declared on the constraint(edgeParam)
      List<OConstraintDto> edgeEnteredList = fields.get(edgeParam.toUpperCase());

      // if (edgeEnteredList == null)
      // throw new OConstraintValidationException(
      // "Could not retrieve the edge type or it does not match the type " + edgeParam + " defined in the constraint.\nTransaction
      // is rolled back!",
      // getName());

      if (edgeEnteredList != null)
        edgeEnteredList.forEach(
            edgeEntered -> doValidate(edgeEntered.getOperation(), edgeEntered.getAttributes(), outParam, edgeParam, inParam));

      if (!newValue.getIdentity().isPersistent())
        newValue = newValue.getRecord();
      return newValue.getIdentity();
    }

    private void doValidate(OOperation operation, Map<String, ODocumentEntry> edgeEntered, String outParam, String edgeParam,
        String inParam) {

      // if (edgeEntered == null)
      // throw new OConstraintValidationException(
      // "Could not retrieve the edge type or it does not match the type " + edgeParam + " defined in the constraint",
      // getName());

      ODocumentEntry outEdge = edgeEntered.get("out");
      ODocumentEntry inEdge = edgeEntered.get("in");

      if (outEdge == null || inEdge == null)
        throw new OConstraintValidationException("Could not retrieve the some of the boundery nodes", getName());

      OIdentifiable o = (OIdentifiable) outEdge.value;
      OIdentifiable i = (OIdentifiable) inEdge.value;
      String outNodeType = ((ODocument) o.getRecord()).getClassName();
      String inNodeType = ((ODocument) i.getRecord()).getClassName();

      if (OConstraintDto.OOperation.DELETED.equals(operation)) {
        if (outNodeType.equalsIgnoreCase(outParam) && inNodeType.equalsIgnoreCase(inParam)) {
          Optional<OVertex> op = ((ODocument) o.getRecord()).asVertex();
          OVertex vertex;
          if(op.isPresent()){
            vertex = op.get();
            ORidBag edges = vertex.getProperty("out_"+edgeParam);
            if(edges.size() < 1){
              throw new OConstraintValidationException(String.format(
                  "Not allowed to delete edge %s.", edgeParam), getName());
            }
          }
        }
      } else {
        if (!outNodeType.toUpperCase().equals(outParam.toUpperCase()))
          throw new OConstraintValidationException(String.format(
              "The out node type entered for the edge %s does not match the type defined in the REQUIRED_EDGE constraint. It was expected %s but it got %s.",
              edgeParam.toUpperCase(), outParam.toUpperCase(), outNodeType.toUpperCase()), getName());

        if (!inNodeType.toUpperCase().equals(inParam.toUpperCase()))
          throw new OConstraintValidationException(String.format(
              "The out node type entered for the edge %s does not match the type defined in the REQUIRED_EDGE constraint. It was expected %s but it got %s.",
              edgeParam.toUpperCase(), inParam.toUpperCase(), inNodeType.toUpperCase()), getName());
      }
    }
  };

  public OConstraintRequiredEdge(String name, final String type, String algorithm, int version, OAbstractPaginatedStorage storage,
      String valueContainerAlgorithm, ODocument metadata) {
    super(name, type, algorithm, valueContainerAlgorithm, metadata, version, storage);
  }

  @Override
  public OConstraint<OIdentifiable> put(Object key, final OIdentifiable value, Map<String, List<OConstraintDto>> fields) {
    key = getCollatingValue(key);

    acquireSharedLock();
    try {
      while (true) {
        try {
          storage.validatedPutConstraintValue(constraintId, key, value, CONSTRAINT_VALIDATOR, fields,
              getConstraintDefinition().getConstraintParams());
          return this;
        } catch (OInvalidConstraintEngineIdException ignore) {
          doReloadConstraintEngine();
        }
      }

    } finally {
      releaseSharedLock();
    }
  }

  @Override
  public boolean supportsOrderedIterations() {
    return false;
  }

}
