/* Generated By:JJTree: Do not edit this line. OCreateConstraintStatement.java Version 6.0 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=true,TRACK_TOKENS=true,NODE_PREFIX=O,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package com.orientechnologies.orient.core.sql.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import com.orientechnologies.orient.core.collate.OCollate;
import com.orientechnologies.orient.core.command.OCommandContext;
import com.orientechnologies.orient.core.constraint.OConditionalParameters;
import com.orientechnologies.orient.core.constraint.OConstraint;
import com.orientechnologies.orient.core.constraint.OConstraintDefinition;
import com.orientechnologies.orient.core.constraint.OConstraintDefinitionFactory;
import com.orientechnologies.orient.core.constraint.OConstraintException;
import com.orientechnologies.orient.core.constraint.OConstraintFactory;
import com.orientechnologies.orient.core.constraint.OConstraintInOutEdgeParameters;
import com.orientechnologies.orient.core.constraint.OConstraintParameters;
import com.orientechnologies.orient.core.constraint.OConstraintRequiredEdgeParameters;
import com.orientechnologies.orient.core.constraint.OConstraintUniqueParameters;
import com.orientechnologies.orient.core.constraint.OConstraints;
import com.orientechnologies.orient.core.constraint.OSimpleKeyConstraintDefinition;
import com.orientechnologies.orient.core.db.ODatabase;
import com.orientechnologies.orient.core.exception.OCommandExecutionException;
import com.orientechnologies.orient.core.exception.ODatabaseException;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OClassImpl;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OSQLEngine;
import com.orientechnologies.orient.core.sql.executor.OInternalResultSet;
import com.orientechnologies.orient.core.sql.executor.OResultInternal;
import com.orientechnologies.orient.core.sql.executor.OResultSet;

public class OCreateConstraintStatement extends ODDLStatement {

  protected OConstraintName   name;
  protected OIdentifier       className;
  protected List<Property>    propertyList = new ArrayList<Property>();
  protected OIdentifier       type;
  protected OIdentifier       engine;
  protected List<OIdentifier> keyTypes     = new ArrayList<OIdentifier>();
  protected OJson             metadata;
  protected boolean           ifNotExists  = false;
  
  OConstraintConditionalBody conditionalBody;
  OConstraintReqEdgeBody requiredEdgeBody;
  OConstraintInOutEdgeBody inOutEdgeBody;
  OConstraintCardinalityBody cardinalityBody;


  public OCreateConstraintStatement(int id) {
    super(id);
  }

  public OCreateConstraintStatement(OrientSql p, int id) {
    super(p, id);
  }

  @Override
  public OResultSet executeDDL(OCommandContext ctx) {
    Object execResult = execute(ctx);
    OInternalResultSet rs = new OInternalResultSet();
    if (execResult != null) {
      OResultInternal result = new OResultInternal();
      result.setProperty("operation", "create constraint");
      result.setProperty("name", name.getValue());
      rs.add(result);
    }
    return rs;
  }

  Object execute(OCommandContext ctx) {
    final ODatabase database = ctx.getDatabase();

    if (database.getMetadata().getConstraintManager().existsConstraint(name.getValue())) {
      if (ifNotExists) {
        return null;
      } else {
        throw new OCommandExecutionException("Constraint " + name + " already exists");
      }
    }

    final OConstraint<?> constraint;
    List<OCollate> collatesList = calculateCollates(ctx);
    String engine = this.engine == null ? null : this.engine.getStringValue().toUpperCase(Locale.ENGLISH);
    ODocument metadataDoc = calculateMetadata(ctx);

    if (propertyList == null || propertyList.size() == 0) {

      // TODO - FABIO: melhorar implementação do if
      if(type.getStringValue().equalsIgnoreCase(OClass.CONSTRAINT_TYPE.REQUIRED_EDGE.name()) ||
          type.getStringValue().equalsIgnoreCase(OClass.CONSTRAINT_TYPE.IN_OUT_EDGE.name()) ||
          type.getStringValue().equalsIgnoreCase(OClass.CONSTRAINT_TYPE.CARDINALITY.name()) ){
        String[] fields = calculateProperties(ctx);
        OClass oClass = getConstraintClass(ctx);
        constraint = getoConstraint(oClass, fields, engine, database, collatesList, metadataDoc);
      } else {
        OConstraintFactory factory = OConstraints.getFactory(type.getStringValue(), null);
  
        OType[] keyTypes = calculateKeyTypes(ctx);
  
        if (keyTypes != null && keyTypes.length > 0) {
          constraint = database.getMetadata().getConstraintManager().createConstraint(name.getValue(), type.getStringValue(),
              new OSimpleKeyConstraintDefinition(keyTypes, collatesList, factory.getLastVersion()), null, null, metadataDoc, engine);
        } else if (keyTypes != null && keyTypes.length == 0 && "LUCENE_CROSS_CLASS".equalsIgnoreCase(engine)) {
          // handle special case of cross class Lucene constraint: awful but works
          OConstraintDefinition keyDef = new OSimpleKeyConstraintDefinition(new OType[] { OType.STRING }, collatesList,
              factory.getLastVersion());
          constraint = database.getMetadata().getConstraintManager().createConstraint(name.getValue(), type.getStringValue(), keyDef,
              null, null, metadataDoc, engine);
  
        } else if (className == null && keyTypes == null || keyTypes.length == 0) {
          // legacy: create constraint without specifying property names
          String[] split = name.getValue().split("\\.");
          if (split.length != 2) {
            throw new ODatabaseException(
                "Impossible to create an constraint without specify class and property name nor key types: " + toString());
          }
          OClass oClass = database.getClass(split[0]);
          if (oClass == null) {
            throw new ODatabaseException("Impossible to create an constraint, class not found: " + split[0]);
          }
          if (oClass.getProperty(split[1]) == null) {
            throw new ODatabaseException("Impossible to create an constraint, property not found: " + name.getValue());
          }
          String[] fields = new String[] { split[1] };
          constraint = getoConstraint(oClass, fields, engine, database, collatesList, metadataDoc);
  
        } else {
          throw new ODatabaseException(
              "Impossible to create an constraint without specify the key type or the associated property: " + toString());
        }
      }
    } else {
      String[] fields = calculateProperties(ctx);
      OClass oClass = getConstraintClass(ctx);
      constraint = getoConstraint(oClass, fields, engine, database, collatesList, metadataDoc);
    }

    if (constraint != null)
      return constraint.getSize();

    return null;
  }

  private OConstraint<?> getoConstraint(OClass oClass, String[] fields, String engine, ODatabase database,
      List<OCollate> collatesList, ODocument metadataDoc) {
    OConstraint<?> constraint;
    if ((keyTypes == null || keyTypes.size() == 0) && collatesList == null) {

      constraint = oClass.createConstraint(name.getValue(), type.getStringValue(), null, metadataDoc, engine, getConstraintParams(), fields);
    } else {
      final List<OType> fieldTypeList;
      if (keyTypes == null || keyTypes.size() == 0 && fields.length > 0) {
        for (final String fieldName : fields) {
          if (!fieldName.equals("@rid") && !oClass.existsProperty(fieldName))
            throw new OConstraintException("Constraint with name : '" + name.getValue() + "' cannot be created on class : '"
                + oClass.getName() + "' because field: '" + fieldName + "' is absent in class definition.");
        }
        fieldTypeList = ((OClassImpl) oClass).extractFieldTypes(fields);
      } else
        fieldTypeList = keyTypes.stream().map(x -> OType.valueOf(x.getStringValue())).collect(Collectors.toList());

      final OConstraintDefinition constraintDef = OConstraintDefinitionFactory.createConstraintDefinition(oClass,
          Arrays.asList(fields), fieldTypeList, collatesList, type.getStringValue(), null, getConstraintParams());

      constraint = database.getMetadata().getConstraintManager().createConstraint(name.getValue(), type.getStringValue(),
          constraintDef, oClass.getPolymorphicClusterIds(), null, metadataDoc, engine);
    }
    return constraint;
  }

  protected OConstraintParameters getConstraintParams() {
    if(type.getStringValue().equalsIgnoreCase(OClass.CONSTRAINT_TYPE.CONDITIONAL.name()))
      return conditionalBody.getConstraintParams();
    else if(type.getStringValue().equalsIgnoreCase(OClass.CONSTRAINT_TYPE.UNIQUE.name()))
      return new OConstraintUniqueParameters() ;
    else if(type.getStringValue().equalsIgnoreCase(OClass.CONSTRAINT_TYPE.REQUIRED_EDGE.name())){
      requiredEdgeBody.setOriginClass(className);
      return requiredEdgeBody.getConstraintParams();
    }
    else if(type.getStringValue().equalsIgnoreCase(OClass.CONSTRAINT_TYPE.IN_OUT_EDGE.name()))
      return inOutEdgeBody.getConstraintParams() ;
    else if(type.getStringValue().equalsIgnoreCase(OClass.CONSTRAINT_TYPE.CARDINALITY.name())){
      cardinalityBody.setOriginClass(className);
      return cardinalityBody.getConstraintParams() ;
    }
    return null;
  }

  /***
   * returns the list of property names to be constrainted
   *
   * @param ctx
   * @return
   */
  private String[] calculateProperties(OCommandContext ctx) {
    if (propertyList == null) {
      return null;
    }
    return propertyList.stream().map(x -> x.getCompleteKey()).collect(Collectors.toList()).toArray(new String[] {});
  }

  /**
   * calculates the constrainted class based on the class name
   *
   * @param ctx
   *
   * @return
   */
  private OClass getConstraintClass(OCommandContext ctx) {
    if (className == null) {
      return null;
    }
    OClass result = ctx.getDatabase().getMetadata().getSchema().getClass(className.getStringValue());
    if (result == null) {
      throw new OCommandExecutionException("Cannot find class " + className);
    }
    return result;
  }

  /**
   * returns constraint metadata as an ODocuemnt (as expected by Constraint API)
   *
   * @param ctx
   *
   * @return
   */
  private ODocument calculateMetadata(OCommandContext ctx) {
    if (metadata == null) {
      return null;
    }
    return metadata.toDocument(null, ctx);
  }

  private OType[] calculateKeyTypes(OCommandContext ctx) {
    if (keyTypes == null) {
      return new OType[0];
    }
    return keyTypes.stream().map(x -> OType.valueOf(x.getStringValue())).collect(Collectors.toList()).toArray(new OType[] {});
  }

  private List<OCollate> calculateCollates(OCommandContext ctx) {
    List<OCollate> result = new ArrayList<>();
    boolean found = false;
    for (Property prop : this.propertyList) {
      String collate = prop.collate == null ? null : prop.collate.getStringValue();
      if (collate != null) {
        final OCollate col = OSQLEngine.getCollate(collate);
        result.add(col);
        found = true;
      } else {
        result.add(null);
      }
    }
    if (!found) {
      return null;
    }
    return result;
  }

  @Override
  public void toString(Map<Object, Object> params, StringBuilder builder) {
    builder.append("CREATE  CONSTRAINT ");
    name.toString(params, builder);
    if (className != null) {
      builder.append(" ON ");
      className.toString(params, builder);
      builder.append(" (");
      boolean first = true;
      for (Property prop : propertyList) {
        if (!first) {
          builder.append(", ");
        }
        if (prop.name != null) {
          prop.name.toString(params, builder);
        } else {
          prop.recordAttribute.toString(params, builder);
        }
        if (prop.byKey) {
          builder.append(" BY KEY");
        } else if (prop.byValue) {
          builder.append(" BY VALUE");
        }
        if (prop.collate != null) {
          builder.append(" COLLATE ");
          prop.collate.toString(params, builder);
        }
        first = false;
      }
      builder.append(")");
    }
    builder.append(" ");
    type.toString(params, builder);
    if (engine != null) {
      builder.append(" ENGINE ");
      engine.toString(params, builder);
    }
    if (keyTypes != null && keyTypes.size() > 0) {
      boolean first = true;
      builder.append(" ");
      for (OIdentifier keyType : keyTypes) {
        if (!first) {
          builder.append(",");
        }
        keyType.toString(params, builder);
        first = false;
      }
    }
    if (metadata != null) {
      builder.append(" METADATA ");
      metadata.toString(params, builder);
    }
    getConstraintParams().toString();
  }

  @Override
  public OCreateConstraintStatement copy() {
    OCreateConstraintStatement result = new OCreateConstraintStatement(-1);
    result.name = name == null ? null : name.copy();
    result.className = className == null ? null : className.copy();
    result.propertyList = propertyList == null ? null : propertyList.stream().map(x -> x.copy()).collect(Collectors.toList());
    result.type = type == null ? null : type.copy();
    result.engine = engine == null ? null : engine.copy();
    result.keyTypes = keyTypes == null ? null : keyTypes.stream().map(x -> x.copy()).collect(Collectors.toList());
    result.metadata = metadata == null ? null : metadata.copy();
    return result;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    OCreateConstraintStatement that = (OCreateConstraintStatement) o;

    if (name != null ? !name.equals(that.name) : that.name != null)
      return false;
    if (className != null ? !className.equals(that.className) : that.className != null)
      return false;
    if (propertyList != null ? !propertyList.equals(that.propertyList) : that.propertyList != null)
      return false;
    if (type != null ? !type.equals(that.type) : that.type != null)
      return false;
    if (engine != null ? !engine.equals(that.engine) : that.engine != null)
      return false;
    if (keyTypes != null ? !keyTypes.equals(that.keyTypes) : that.keyTypes != null)
      return false;
    if (metadata != null ? !metadata.equals(that.metadata) : that.metadata != null)
      return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = name != null ? name.hashCode() : 0;
    result = 31 * result + (className != null ? className.hashCode() : 0);
    result = 31 * result + (propertyList != null ? propertyList.hashCode() : 0);
    result = 31 * result + (type != null ? type.hashCode() : 0);
    result = 31 * result + (engine != null ? engine.hashCode() : 0);
    result = 31 * result + (keyTypes != null ? keyTypes.hashCode() : 0);
    result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
    return result;
  }

  public static class Property {
    protected OIdentifier      name;
    protected ORecordAttribute recordAttribute;
    protected boolean          byKey   = false;
    protected boolean          byValue = false;
    protected OIdentifier      collate;

    public Property copy() {
      Property result = new Property();
      result.name = name == null ? null : name.copy();
      result.recordAttribute = recordAttribute == null ? null : recordAttribute.copy();
      result.byKey = byKey;
      result.byValue = byValue;
      result.collate = collate == null ? null : collate.copy();
      return result;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;

      Property property = (Property) o;

      if (byKey != property.byKey)
        return false;
      if (byValue != property.byValue)
        return false;
      if (name != null ? !name.equals(property.name) : property.name != null)
        return false;
      if (recordAttribute != null ? !recordAttribute.equals(property.recordAttribute) : property.recordAttribute != null)
        return false;
      if (collate != null ? !collate.equals(property.collate) : property.collate != null)
        return false;

      return true;
    }

    @Override
    public int hashCode() {
      int result = name != null ? name.hashCode() : 0;
      result = 31 * result + (recordAttribute != null ? recordAttribute.hashCode() : 0);
      result = 31 * result + (byKey ? 1 : 0);
      result = 31 * result + (byValue ? 1 : 0);
      result = 31 * result + (collate != null ? collate.hashCode() : 0);
      return result;
    }

    /**
     * returns the complete key to constraint, eg. property name or "property by key/value"
     *
     * @return
     */
    public String getCompleteKey() {
      StringBuilder result = new StringBuilder();
      if (name != null)
        result.append(name.getStringValue());
      else if (recordAttribute != null)
        result.append(recordAttribute.getName());

      if (byKey) {
        result.append(" by key");
      }
      if (byValue) {
        result.append(" by value");
      }
      return result.toString();
    }
  }

}
/* JavaCC - OriginalChecksum=385ae25e16a88550846137685771a559 (do not edit this line) */
