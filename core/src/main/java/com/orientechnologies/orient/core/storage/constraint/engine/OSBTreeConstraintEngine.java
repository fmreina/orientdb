package com.orientechnologies.orient.core.storage.constraint.engine;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.orientechnologies.common.serialization.types.OBinarySerializer;
import com.orientechnologies.orient.core.constraint.OConstraintDefinition;
import com.orientechnologies.orient.core.constraint.OConstraintDto;
import com.orientechnologies.orient.core.constraint.OConstraintEngine;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.encryption.OEncryption;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ODocumentEntry;
import com.orientechnologies.orient.core.storage.impl.local.OAbstractPaginatedStorage;
import com.orientechnologies.orient.core.storage.index.sbtree.local.OSBTree;

/**
 * 
 * @author fabio
 *
 */
public class OSBTreeConstraintEngine implements OConstraintEngine {

  public static final int               VERSION                    = 1;

  public static final String            DATA_FILE_EXTENSION        = ".sbt";
  public static final String            NULL_BUCKET_FILE_EXTENSION = ".nbt";

  private final OSBTree<Object, Object> sbTree;
  private int                           version;
  private final String                  name;

  public OSBTreeConstraintEngine(String name, OAbstractPaginatedStorage storage, int version) {
    this.name = name;
    this.version = version;
    this.sbTree = new OSBTree<>(name, DATA_FILE_EXTENSION, NULL_BUCKET_FILE_EXTENSION, storage);
  }

  @Override
  public void init(String constraintName, String constraintType, OConstraintDefinition constraintDefinition, boolean isAutomatic,
      ODocument metadata) {
  }

  @Override
  public void create(OBinarySerializer valueSerializer, boolean isAutomatic, OType[] keyTypes, boolean nullPointerSupport,
      OBinarySerializer keySerializer, int keySize, Set<String> clustersToConstraint, Map<String, String> engineProperties,
      ODocument metadata, OEncryption encryption) {
    sbTree.create(keySerializer, valueSerializer, keyTypes, keySize, nullPointerSupport, encryption);
  }

  @Override
  public void delete() {
    sbTree.delete();
  }

  @Override
  public void load(String constraintName, OBinarySerializer valueSerializer, boolean isAutomatic, OBinarySerializer keySerializer,
      OType[] keyTypes, boolean nullPointerSupport, int keySize, Map<String, String> engineProperties, OEncryption encryption) {
    sbTree.load(constraintName, keySerializer, valueSerializer, keyTypes, keySize, nullPointerSupport, encryption);
  }

  public String getName() {
    return name;
  }

  @Override
  public int getVersion() {
    return version;
  }

  @Override
  public boolean contains(Object key) {
    // FIXME - FABIO : esta usando o sbTree.get do index
    return sbTree.get(key) != null;
  }

  @Override
  public boolean remove(Object key) {
    return sbTree.remove(key) != null;
  }
  
  @Override
  public void clear() {
    sbTree.clear();
  }

  @Override
  public Object get(Object key) {
    // FIXME - FABIO : esta usando o sbTree.get do index
    return sbTree.get(key);
  }

  @Override
  public void put(Object key, Object value) {
    // FIXME - FABIO : esta usando o sbTree.put do index
    sbTree.put(key, value);
  }

  @Override
  public boolean validatedPut(Object key, OIdentifiable value, Validator<Object, OIdentifiable> validator, Map<String, List<OConstraintDto>> fields, String params) {
    return sbTree.validatedPutConstraint(key, value, (Validator) validator, fields, params);
  }

  @Override
  public long size(final ValuesTransformer transformer) {
    // FIXME - FABIO : esta usando o sbTree.size do index
    if (transformer == null)
      return sbTree.size();
    else {
      int counter = 0;

      if (sbTree.isNullPointerSupport()) {
        // FIXME - FABIO : esta usando o sbTree.get do index
        final Object nullValue = sbTree.get(null);
        if (nullValue != null) {
          counter += transformer.transformFromValue(nullValue).size();
        }
      }

      // FIXME - FABIO : esta usando o sbTree.firstkey e sbtree.lastkey do index
      final Object firstKey = sbTree.firstKey();
      final Object lastKey = sbTree.lastKey();

      if (firstKey != null && lastKey != null) {
        // FIXME - FABIO : esta usando o sbTree.iterateEntries... do index
        final OSBTree.OSBTreeCursor<Object, Object> cursor = sbTree.iterateEntriesBetween(firstKey, true, lastKey, true, true);
        Map.Entry<Object, Object> entry = cursor.next(-1);
        while (entry != null) {
          counter += transformer.transformFromValue(entry.getValue()).size();
          entry = cursor.next(-1);
        }

        return counter;
      }

      return counter;
    }
  }

  @Override
  public String getConstraintNameByKey(Object key) {
    return name;
  }

  @Override
  public boolean acquireAtomicExclusiveLock(Object key) {
    sbTree.acquireAtomicExclusiveLock();
    return true;
  }
}
