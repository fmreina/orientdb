package com.orientechnologies.orient.core.sql;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.orientechnologies.common.exception.OException;
import com.orientechnologies.common.util.OPatternConst;
import com.orientechnologies.orient.core.collate.OCollate;
import com.orientechnologies.orient.core.command.OCommandDistributedReplicateRequest;
import com.orientechnologies.orient.core.command.OCommandRequest;
import com.orientechnologies.orient.core.command.OCommandRequestText;
import com.orientechnologies.orient.core.constraint.OConstraint;
import com.orientechnologies.orient.core.constraint.OConstraintDefinition;
import com.orientechnologies.orient.core.constraint.OConstraintDefinitionFactory;
import com.orientechnologies.orient.core.constraint.OConstraintException;
import com.orientechnologies.orient.core.constraint.OConstraintFactory;
import com.orientechnologies.orient.core.constraint.OConstraints;
import com.orientechnologies.orient.core.constraint.OPropertyMapConstraintDefinition;
import com.orientechnologies.orient.core.constraint.ORuntimeKeyConstraintDefinition;
import com.orientechnologies.orient.core.constraint.OSimpleKeyConstraintDefinition;
import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.exception.OCommandExecutionException;
import com.orientechnologies.orient.core.exception.ODatabaseException;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OClassImpl;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;

/**
 * 
 * @author fabio
 *
 */
@SuppressWarnings("unchecked")
public class OCommandExecutorSQLCreateConstraint extends OCommandExecutorSQLAbstract
    implements OCommandDistributedReplicateRequest {

  public static final String     KEYWORD_CREATE     = "CREATE";
  public static final String     KEYWORD_CONSTRAINT = "CONSTRAINT";
  public static final String     KEYWORD_ON         = "ON";
  public static final String     KEYWORD_METADATA   = "METADATA";
  public static final String     KEYWORD_ENGINE     = "ENGINE";

  private String                 constraintName;
  private OClass                 oClass;
  private String[]               fields;
  private OClass.CONSTRAINT_TYPE constraintType;
  private OType[]                keyTypes;
  private byte                   serializerKeyId;
  private String                 engine;
  private ODocument              metadataDoc        = null;
  private String[]               collates;

  @Override
  public OCommandExecutorSQLCreateConstraint parse(OCommandRequest iRequest) {
    final OCommandRequestText textRequest = (OCommandRequestText) iRequest;
    String queryText = textRequest.getText();
    String originalQuery = queryText;
    try {
      queryText = preParse(queryText, iRequest);
      textRequest.setText(queryText);

      init((OCommandRequestText) iRequest);

      final StringBuilder word = new StringBuilder();

      int oldPos = 0;
      int pos = nextWord(parserText, parserTextUpperCase, oldPos, word, true);
      if (pos == -1 || !word.toString().equals(KEYWORD_CREATE))
        throw new OCommandSQLParsingException("Keyword " + KEYWORD_CREATE + " not found. Use " + getSyntax(), parserText, oldPos);

      oldPos = pos;
      pos = nextWord(parserText, parserTextUpperCase, oldPos, word, true);
      if (pos == -1 || !word.toString().equals(KEYWORD_CONSTRAINT))
        throw new OCommandSQLParsingException("Keyword " + KEYWORD_CONSTRAINT + " not found. Use " + getSyntax(), parserText,
            oldPos);

      oldPos = pos;
      pos = nextWord(parserText, parserTextUpperCase, oldPos, word, false);
      if (pos == -1)
        throw new OCommandSQLParsingException("Expected constraint name. Use " + getSyntax(), parserText, oldPos);

      constraintName = decodeClassName(word.toString());

      oldPos = pos;
      pos = nextWord(parserText, parserTextUpperCase, oldPos, word, true);
      if (pos == -1)
        throw new OCommandSQLParsingException("Constraint type requested. Use " + getSyntax(), parserText, oldPos + 1);

      if (word.toString().equals(KEYWORD_ON)) {
        oldPos = pos;
        pos = nextWord(parserText, parserTextUpperCase, oldPos, word, true);
        if (pos == -1)
          throw new OCommandSQLParsingException("Expected class name. Use " + getSyntax(), parserText, oldPos);

        oldPos = pos;
        oClass = findClass(decodeClassName(word.toString()));

        if (oClass == null)
          throw new OCommandExecutionException("Class " + word + " not found");

        pos = parserTextUpperCase.indexOf(")");
        if (pos == -1) {
          throw new OCommandSQLParsingException("No right bracket found. Use " + getSyntax(), parserText, oldPos);
        }

        final String props = parserText.substring(oldPos, pos).trim().substring(1);

        List<String> propList = new ArrayList<String>();
        Collections.addAll(propList, OPatternConst.PATTERN_COMMA_SEPARATED.split(props.trim()));

        fields = new String[propList.size()];
        propList.toArray(fields);

        for (int i = 0; i < fields.length; i++) {
          final String fieldName = fields[i];

          final int collatePos = fieldName.toUpperCase(Locale.ENGLISH).indexOf(" COLLATE ");

          if (collatePos > 0) {
            if (collates == null)
              collates = new String[fields.length];

            collates[i] = fieldName.substring(collatePos + " COLLATE ".length()).toLowerCase(Locale.ENGLISH).trim();
            fields[i] = fieldName.substring(0, collatePos);
          } else {
            if (collates != null)
              collates[i] = null;
          }
          fields[i] = decodeClassName(fields[i]);
        }

        for (String propToConstraint : fields) {
          checkMapConstraintSpecifier(propToConstraint, parserText, oldPos);

          propList.add(propToConstraint);
        }

        oldPos = pos + 1;
        pos = nextWord(parserText, parserTextUpperCase, oldPos, word, true);
        if (pos == -1)
          throw new OCommandSQLParsingException("Constraint type requested. Use " + getSyntax(), parserText, oldPos + 1);
      } else {
        if (constraintName.indexOf('.') > 0) {
          final String[] parts = constraintName.split("\\.");

          oClass = findClass(parts[0]);
          if (oClass == null)
            throw new OCommandExecutionException("Class " + parts[0] + " not found");

          fields = new String[] { parts[1] };
        }
      }

      constraintType = OClass.CONSTRAINT_TYPE.valueOf(word.toString());

      if (constraintType == null)
        throw new OCommandSQLParsingException("Constraint type is null", parserText, oldPos);

      oldPos = pos;
      pos = nextWord(parserText, parserTextUpperCase, oldPos, word, true);

      if (word.toString().equals(KEYWORD_ENGINE)) {
        oldPos = pos;
        pos = nextWord(parserText, parserTextUpperCase, oldPos, word, false);
        oldPos = pos;
        engine = word.toString().toUpperCase(Locale.ENGLISH);
      } else
        parserGoBack();

      final int configPos = parserTextUpperCase.indexOf(KEYWORD_METADATA, oldPos);

      if (configPos > -1) {
        final String configString = parserText.substring(configPos + KEYWORD_METADATA.length()).trim();
        metadataDoc = new ODocument().fromJSON(configString);
      }

      pos = nextWord(parserText, parserTextUpperCase, oldPos, word, true);
      if (pos != -1 && !word.toString().equalsIgnoreCase("NULL") && !word.toString().equalsIgnoreCase(KEYWORD_METADATA)) {
        final String typesString;
        if (configPos > -1)
          typesString = parserTextUpperCase.substring(oldPos, configPos).trim();
        else
          typesString = parserTextUpperCase.substring(oldPos).trim();

        if (word.toString().equalsIgnoreCase("RUNTIME")) {
          oldPos = pos;
          pos = nextWord(parserText, parserTextUpperCase, oldPos, word, true);

          serializerKeyId = Byte.parseByte(word.toString());
        } else {
          ArrayList<OType> keyTypeList = new ArrayList<OType>();
          for (String typeName : OPatternConst.PATTERN_COMMA_SEPARATED.split(typesString)) {
            keyTypeList.add(OType.valueOf(typeName));
          }

          keyTypes = new OType[keyTypeList.size()];
          keyTypeList.toArray(keyTypes);

          if (fields != null && fields.length != 0 && fields.length != keyTypes.length) {
            throw new OCommandSQLParsingException("Count of fields does not match with count of property types. " + "Fields: "
                + Arrays.toString(fields) + "; Types: " + Arrays.toString(keyTypes), parserText, oldPos);
          }
        }
      }
    } finally {
      textRequest.setText(originalQuery);
    }

    return this;
  }

  /**
   * Execute the CREATE CONSTRAINT.
   */
  @SuppressWarnings("rawtypes")
  @Override
  public Object execute(Map<Object, Object> iArgs) {
    if (constraintName == null)
      throw new OCommandExecutionException("Cannot execute the command because it has not been parsed yet");

    final ODatabaseDocument database = getDatabase();
    final OConstraint<?> constr;
    List<OCollate> collatesList = null;

    if (collates != null) {
      collatesList = new ArrayList<OCollate>();

      for (String collate : collates) {
        if (collate != null) {
          final OCollate col = OSQLEngine.getCollate(collate);
          collatesList.add(col);
        } else
          collatesList.add(null);
      }
    }

    if (fields == null || fields.length == 0) {
      OConstraintFactory factory = OConstraints.getFactory(constraintType.toString(), null);

      if (keyTypes != null)
        constr = database.getMetadata().getConstraintManager().createConstraint(constraintName, constraintType.toString(),
            new OSimpleKeyConstraintDefinition(keyTypes, collatesList, factory.getLastVersion()), null, null, metadataDoc, engine);
      else if (serializerKeyId != 0) {
        constr = database.getMetadata().getConstraintManager().createConstraint(constraintName, constraintType.toString(),
            new ORuntimeKeyConstraintDefinition(serializerKeyId, factory.getLastVersion()), null, null, metadataDoc, engine);
      } else {
        throw new ODatabaseException("Impossible to create a constraint without specify the key type or the associated property");
      }
    } else {
      if ((keyTypes == null || keyTypes.length == 0) && collates == null) {
        System.out.println("\nOConstraintParameters params are null em OCommandExecutorSQLCreateConstraint"); //FIXME - FABIO: params == null
        constr = oClass.createConstraint(constraintName, constraintType.toString(), null, metadataDoc, engine, null, fields);
      } else {
        final List<OType> fieldTypeList;
        if (keyTypes == null) {
          for (final String fieldName : fields) {
            if (!fieldName.equals("@rid") && !oClass.existsProperty(fieldName))
              throw new OConstraintException("Constraint with name : '" + constraintName + "' cannot be created on class : '"
                  + oClass.getName() + "' because field: '" + fieldName + "' is absent in class definition.");
          }
          fieldTypeList = ((OClassImpl) oClass).extractFieldTypes(fields);
        } else
          fieldTypeList = Arrays.asList(keyTypes);

        System.out.println("\nOConstraintParameters params are null em OCommandExecutorSQLCreateConstraint"); //FIXME - FABIO: OConstraintParameters params == null
        final OConstraintDefinition constrDef = OConstraintDefinitionFactory.createConstraintDefinition(oClass,
            Arrays.asList(fields), fieldTypeList, collatesList, constraintType.toString(), null, null);

        constr = database.getMetadata().getConstraintManager().createConstraint(constraintName, constraintType.name(), constrDef,
            oClass.getPolymorphicClusterIds(), null, metadataDoc, engine);
      }
    }

    if (constr != null)
      return constr.getSize();

    return null;
  }

  @Override
  public QUORUM_TYPE getQuorumType() {
    return QUORUM_TYPE.ALL;
  }

  @Override
  public String getSyntax() {
    // TODO - Fabio: checar sintaxe.
    return "CREATE CONSTRAINT <name> [ON <class-name> (prop-names [COLLATE <collate>])] <type> [<key-type>] [ENGINE <engine>] [METADATA {JSON Constraint Metadata Document}]";
  }

  private OClass findClass(String part) {
    return getDatabase().getMetadata().getSchema().getClass(part);
  }

  private void checkMapConstraintSpecifier(final String fieldName, final String text, final int pos) {
    final String[] fieldNameParts = OPatternConst.PATTERN_SPACES.split(fieldName);
    if (fieldNameParts.length == 1)
      return;

    if (fieldNameParts.length == 3) {
      if ("by".equals(fieldNameParts[1].toLowerCase(Locale.ENGLISH))) {
        try {
          OPropertyMapConstraintDefinition.CONSTRAINT_BY.valueOf(fieldNameParts[2].toUpperCase(Locale.ENGLISH));
        } catch (IllegalArgumentException iae) {
          throw OException.wrapException(new OCommandSQLParsingException(
              "Illegal field name format, should be '<property> [by key|value]' but was '" + fieldName + "'", text, pos), iae);
        }
        return;
      }
      throw new OCommandSQLParsingException(
          "Illegal field name format, should be '<property> [by key|value]' but was '" + fieldName + "'", text, pos);
    }

    throw new OCommandSQLParsingException(
        "Illegal field name format, should be '<property> [by key|value]' but was '" + fieldName + "'", text, pos);
  }

  @Override
  public String getUndoCommand() {
    return "drop constraint " + constraintName;
  }
}
