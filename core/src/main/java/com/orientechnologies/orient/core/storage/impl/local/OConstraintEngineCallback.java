package com.orientechnologies.orient.core.storage.impl.local;

import com.orientechnologies.orient.core.constraint.OConstraintEngine;

/**
 * 
 * @author fabio
 *
 */
public interface OConstraintEngineCallback<T> {
  T callEngine(OConstraintEngine engine);
}
