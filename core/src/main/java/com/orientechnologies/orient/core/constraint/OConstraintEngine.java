package com.orientechnologies.orient.core.constraint;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.orientechnologies.common.serialization.types.OBinarySerializer;
import com.orientechnologies.orient.core.constraint.OConstraintEngine.Validator;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.encryption.OEncryption;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ODocumentEntry;

/**
 * 
 * @author fabio
 *
 */
public interface OConstraintEngine {

  void init(String constraintName, String constraintType, OConstraintDefinition constraintDefinition, boolean isAutomatic,
      ODocument metadata);

  void create(OBinarySerializer valueSerializer, boolean isAutomatic, OType[] keyTypes, boolean nullPointerSupport,
      OBinarySerializer keySerializer, int keySize, Set<String> clustersToConstraint, Map<String, String> engineProperties,
      ODocument metadata, OEncryption encryption);

  long size(ValuesTransformer transformer);

  String getName();

  int getVersion();

  void delete();

  void load(String constraintName, OBinarySerializer valueSerializer, boolean isAutomatic, OBinarySerializer keySerializer,
      OType[] keyTypes, boolean nullPointerSupport, int keySize, Map<String, String> engineProperties, OEncryption encryption);

  boolean contains(Object key);
  
  boolean remove(Object key);

  void clear();

  Object get(Object key);

  void put(Object key, Object value);

  /**
   * Puts the given value under the given key into this constraint engine. Validates the operation using the provided validator.
   *
   * @param key
   *          the key to put the value under.
   * @param value
   *          the value to put.
   * @param validator
   *          the operation validator.
   *
   * @return {@code true} if the validator allowed the put, {@code false} otherwise.
   *
   * @see Validator#validate(Object, Object, Object)
   */
  boolean validatedPut(Object key, OIdentifiable value, Validator<Object, OIdentifiable> validator, Map<String, List<OConstraintDto>> fields, String params);
  interface ValuesTransformer {
    Collection<OIdentifiable> transformFromValue(Object value);
  }

  /**
   * Put operation validator.
   *
   * @param <K>
   *          the key type.
   * @param <V>
   *          the value type.
   */
  interface Validator<K, V> {

    /**
     * Indicates that a put request should be silently ignored by the store.
     *
     * @see #validate(Object, Object, Object)
     */
    Object IGNORE = new Object();

    /**
     * Validates the put operation for the given key, the old value and the new value. May throw an exception to abort the current
     * put operation with an error.
     *
     * @param key
     *          the put operation key.
     * @param oldValue
     *          the old value or {@code null} if no value is currently stored.
     * @param newValue
     *          the new value passed to {@link #validatedPut(Object, OIdentifiable, Validator)}.
     * @param fields
     *
     * @return the new value to store, may differ from the passed one, or the special {@link #IGNORE} value to silently ignore the
     *         put operation request being processed.
     */
    Object validate(K key, V oldValue, V newValue, Map<String, List<OConstraintDto>> fields, String params);
  }

  String getConstraintNameByKey(Object key);
  
  /**
   * <p>Acquires exclusive lock in the active atomic operation running on the current thread for this constraint engine.
   * <p>
   * <p>If this constraint engine supports a more narrow locking, for example key-based sharding, it may use the provided {@code key} to
   * infer a more narrow lock scope, but that is not a requirement.
   *
   * @param key the constraint key to lock.
   *
   * @return {@code true} if this constraint was locked entirely, {@code false} if this constraint locking is sensitive to the provided {@code
   * key} and only some subset of this constraint was locked.
   */
  boolean acquireAtomicExclusiveLock(Object key);
}
