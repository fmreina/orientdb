package com.orientechnologies.orient.core.tx;

/**
 * Represents information for each constraint operation for each record in DB.
 * 
 * @author fabio
 */
public final class OTransactionRecordConstraintOperation {
  public String                             constraint;
  public Object                             key;
  public OTransactionConstraintChanges.OPERATION operation;

  public OTransactionRecordConstraintOperation(String constraint, Object key, OTransactionConstraintChanges.OPERATION operation) {
    this.constraint = constraint;
    this.key = key;
    this.operation = operation;
  }
}
