package com.orientechnologies.orient.core.constraint;

import static com.orientechnologies.orient.core.constraint.OConstraintManagerAbstract.getDatabase;

import java.util.Collection;
import java.util.Locale;
import java.util.Set;

import com.orientechnologies.common.listener.OProgressListener;
import com.orientechnologies.orient.core.db.ODatabaseDocumentInternal;
import com.orientechnologies.orient.core.db.record.OProxedResource;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.record.ORecordInternal;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandExecutorSQLCreateConstraint;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.orientechnologies.orient.core.storage.OAutoshardedStorage;
import com.orientechnologies.orient.core.type.ODocumentWrapper;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintManagerProxy extends OProxedResource<OConstraintManagerAbstract> implements OConstraintManager {

  public OConstraintManagerProxy(OConstraintManagerAbstract iDelegate, ODatabaseDocumentInternal iDatabase) {
    super(iDelegate, iDatabase);
  }

  @Override
  public OConstraintManager reload() {
    delegate.load(database);
    return this;
  }

  @Override
  public Collection<? extends OConstraint<?>> getConstraints() {
    return delegate.getConstraints(database);
  }

  @Override
  public OConstraint<?> getConstraint(String iName) {
    return delegate.getConstraint(iName);
  }

  @Override
  public boolean existsConstraint(String iName) {
    return delegate.existsConstraint(iName);
  }

  @Override
  public OConstraint<?> createConstraint(String iName, String iType, OConstraintDefinition constraintDefinition,
      int[] clusterIdsToConstraint, OProgressListener progressListener, ODocument metadata) {
    if (isDistributedCommand()) {
      return distributedCreateConstraint(iName, iType, constraintDefinition, clusterIdsToConstraint, progressListener, metadata,
          null);
    }
    return delegate.createConstraint(iName, iType, constraintDefinition, clusterIdsToConstraint, progressListener, metadata);
  }

  @Override
  public OConstraint<?> createConstraint(String iName, String iType, OConstraintDefinition constraintDefinition,
      int[] clusterIdsToConstraint, OProgressListener progressListener, ODocument metadata, String algorithm) {
    if (isDistributedCommand()) {
      return distributedCreateConstraint(iName, iType, constraintDefinition, clusterIdsToConstraint, progressListener, metadata,
          algorithm);
    }
    return delegate.createConstraint(iName, iType, constraintDefinition, clusterIdsToConstraint, progressListener, metadata,
        algorithm);
  }

  public OConstraint<?> distributedCreateConstraint(String iName, String iType, OConstraintDefinition iConstraintDefinition,
      int[] clusterIdsToConstraint, OProgressListener progressListener, ODocument metadata, String engine) {

    String createConstraintDDl;
    if (iConstraintDefinition != null)
      createConstraintDDl = iConstraintDefinition.toCreateConstraintDDL(iName, iType, engine);
    else
      createConstraintDDl = new OSimpleKeyConstraintDefinition().toCreateConstraintDDL(iName, iType, engine);

    if (metadata != null)
      createConstraintDDl += " " + OCommandExecutorSQLCreateConstraint.KEYWORD_METADATA + " " + metadata.toJSON();

    if (progressListener != null)
      progressListener.onBegin(this, 0, false);

    getDatabase().command(new OCommandSQL(createConstraintDDl)).execute();

    ORecordInternal.setIdentity(delegate.getDocument(),
        new ORecordId(getDatabase().getStorage().getConfiguration().getConstraintMgrRecordId()));

    if (progressListener != null)
      progressListener.onCompletition(this, true);

    reload();

    final Locale locale = delegate.getServerLocale();

    return delegate.preProcessBeforeReturn(getDatabase(), delegate.getConstraint(iName));
  }

  public OConstraintManager dropConstraint(final String iConstraintName) {
    if (isDistributedCommand()) {
      distributedDropConstraint(iConstraintName);
      return this;
    }

    return delegate.dropConstraint(iConstraintName);
  }

  public void distributedDropConstraint(final String iName) {

    String dropConstraintDDL = "DROP CONSTRAINT `" + iName + "`";

    getDatabase().command(new OCommandSQL(dropConstraintDDL)).execute();
    ORecordInternal.setIdentity(delegate.getDocument(),
        new ORecordId(getDatabase().getStorage().getConfiguration().getConstraintMgrRecordId()));

    reload();

  }

  @Override
  public void removeClassPropertyConstraint(OConstraint<?> constr) {
    delegate.removeClassPropertyConstraint(constr);
  }

  private boolean isDistributedCommand() {
    return database.getStorage().isDistributed() && !((OAutoshardedStorage) database.getStorage()).isLocalEnv();
  }
  
  @Override
  public Set<OConstraint<?>> getClassConstraints(String className) {
    return delegate.getClassConstraints(className);
  }
  
  @Override
  public void getClassConstraints(String className, Collection<OConstraint<?>> constraints) {
    delegate.getClassConstraints(className, constraints);
  }

  public void getClassRawConstraints(String name, Collection<OConstraint<?>> constraints) {
    delegate.getClassRawConstraints(name, constraints);
  }
  
  public OConstraint<?> preProcessBeforeReturn(ODatabaseDocumentInternal database, OConstraint<?> constraint) {
    return delegate.preProcessBeforeReturn(database, constraint);
  }
  
  @Override
  public <RET extends ODocumentWrapper> RET save() {
    return delegate.save();
  }
}
