package com.orientechnologies.orient.core.constraint;

import java.util.List;
import java.util.Map;

import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.exception.OInvalidConstraintEngineIdException;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ODocumentEntry;
import com.orientechnologies.orient.core.storage.ORecordDuplicatedConstraintException;
import com.orientechnologies.orient.core.storage.impl.local.OAbstractPaginatedStorage;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintUnique extends OConstraintOneValue {

  private final OConstraintEngine.Validator<Object, OIdentifiable> CONSTRAINT_VALIDATOR = new OConstraintEngine.Validator<Object, OIdentifiable>() {
    @Override
    public Object validate(Object key, OIdentifiable oldValue, OIdentifiable newValue,
        Map<String, List<OConstraintDto>> fields, String params) {

//      System.out.println("\nValidation of the UNIQUE constraint.");
//      System.out.println("Params: " + params);
//      System.out.println("Entered Values:");
//      System.out.println("oldValue: " + oldValue);
//      System.out.println("newValue: "+newValue);
//      fields.keySet().forEach(k -> {
//        String cName = k;
//        System.out.println(cName);
//        List<OConstraintDto> listDto = fields.get(k);
//        listDto.forEach(dto -> {
//          Map<String, ODocumentEntry> m = dto.getAttributes();
//          m.keySet().forEach(s -> {
//            String attribute = s;
//            System.out.print("\t");
//            System.out.print(attribute);
//            System.out.print(" = ");
//            ODocumentEntry doc = m.get(s);
//            System.out.println(doc.value);
//          });
//          System.out.println();
//        });
//      });

      // copy from @OIndexUnique.java, does the same processes.
      if (oldValue != null) {
        // CHECK IF THE ID IS THE SAME OF CURRENT: THIS IS THE UPDATE CASE
        if (!oldValue.equals(newValue)) {
          final Boolean mergeSameKey = metadata != null ? (Boolean) metadata.field(OConstraint.CONSTRAINT_KEYS) : Boolean.FALSE;
          if (mergeSameKey == null || !mergeSameKey)
            throw new ORecordDuplicatedConstraintException(
                String.format("Found duplicated key '%s' for constraint '%s' previously assigned to the record %s", key, getName(),
                    oldValue.getIdentity()),
                getName(), oldValue.getIdentity(), key);
        } else
          return OConstraintEngine.Validator.IGNORE;
      }

      if (!newValue.getIdentity().isPersistent())
        newValue = newValue.getRecord();
      return newValue.getIdentity();
    }
  };

  public OConstraintUnique(String name, final String type, String algorithm, int version, OAbstractPaginatedStorage storage,
      String valueContainerAlgorithm, ODocument metadata) {
    super(name, type, algorithm, valueContainerAlgorithm, metadata, version, storage);
  }

  @Override
  public OConstraint<OIdentifiable> put(Object key, final OIdentifiable value, Map<String, List<OConstraintDto>> fields) {
    key = getCollatingValue(key);

    acquireSharedLock();
    try {
      while (true) {
        try {
          storage.validatedPutConstraintValue(constraintId, key, value, CONSTRAINT_VALIDATOR, fields,
              getConstraintDefinition().getConstraintParams());
          return this;
        } catch (OInvalidConstraintEngineIdException ignore) {
          doReloadConstraintEngine();
        }
      }

    } finally {
      releaseSharedLock();
    }
  }

  @Override
  public boolean supportsOrderedIterations() {
    return false;
  }

}
