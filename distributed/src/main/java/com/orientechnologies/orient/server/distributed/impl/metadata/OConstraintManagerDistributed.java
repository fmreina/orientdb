package com.orientechnologies.orient.server.distributed.impl.metadata;

import com.orientechnologies.orient.core.constraint.OConstraintManagerAbstract;
import com.orientechnologies.orient.core.constraint.OConstraintManagerShared;
import com.orientechnologies.orient.core.db.ODatabaseDocumentInternal;
import com.orientechnologies.orient.core.db.OScenarioThreadLocal;
import com.orientechnologies.orient.core.storage.OStorage;

/**
 * 
 * @author fabio
 *
 */
public class OConstraintManagerDistributed extends OConstraintManagerShared{

	public OConstraintManagerDistributed(OStorage storage) {
		super(storage);
	}

	@Override
	public OConstraintManagerAbstract load(ODatabaseDocumentInternal database) {
		OScenarioThreadLocal.executeAsDistributed(()->{
			super.load(database);
			return null;
		});
		return this;
	}
}
