package com.orientechnologies.orient.client.remote.message;

import static com.orientechnologies.orient.enterprise.channel.binary.OChannelBinaryProtocol.REQUEST_PUSH_CONSTRAINT_MANAGER;

import java.io.IOException;

import com.orientechnologies.orient.client.remote.ORemotePushHandler;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.serialization.serializer.record.binary.ORecordSerializerNetworkV37;
import com.orientechnologies.orient.enterprise.channel.binary.OChannelBinaryProtocol;
import com.orientechnologies.orient.enterprise.channel.binary.OChannelDataInput;
import com.orientechnologies.orient.enterprise.channel.binary.OChannelDataOutput;

/**
 * 
 * @author fabio
 *
 */
public class OPushConstraintManagerRequest implements OBinaryPushRequest<OBinaryPushResponse> {

	private ODocument constraintManager;
	
	public OPushConstraintManagerRequest() {

	}
	
	public OPushConstraintManagerRequest(ODocument constraintManager) {
		this.constraintManager = constraintManager;
	}
	
	@Override
	public void write(OChannelDataOutput channel) throws IOException {
		channel.writeBytes(ORecordSerializerNetworkV37.INSTANCE.toStream(constraintManager, false));
	}

	@Override
	public void read(OChannelDataInput network) throws IOException {
		byte[] bytes = network.readBytes();
		this.constraintManager = (ODocument) ORecordSerializerNetworkV37.INSTANCE.fromStream(bytes, null, null);
	}

	@Override
	public OBinaryPushResponse execute(ORemotePushHandler pushHandler) {
		return pushHandler.executeUpdateConstraintManager(this);
	}

	@Override
	public OBinaryPushResponse createResponse() {
		return null;
	}

	@Override
	public byte getPushCommand() {
		return REQUEST_PUSH_CONSTRAINT_MANAGER;
	}

	public ODocument getConstraintManager() {
		return constraintManager;
	}
}
